<?php

/**
 * Class to manage user via token
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2020-2024 Denis Chenu <http://www.sondages.pro>
 * @license AGPL v3
 * @since 0.36.1
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

namespace TokenUsersListAndManagePlugin;

use App;
use Yii;
use CDbcriteria;
use Template;
use CHtml;
use CClientScript;
use Permission;
use CHttpException;
use Survey;
use Token;
use TokenManaged;

class TokenUsersManager
{
    /** @var integer|null surveyId **/
    public $surveyId;

    /** @var integer|null originasurveyId **/
    public $originalSurveyId;
    /** @var string|null current user token **/
    public $token;
    /** @var integer|null current response id **/
    public $srid;

    /** @var integer|null restrictedSurvey **/
    public $restrictedSurvey;
    /** @var boolean is adminsitrator **/
    private $isAdmin = false;

    /** @var array|null aSurveyInfo used for rendering**/
    private $aSurveyInfo;
    /* @var array used for rendering*/
    private $aTokenUsersListInfo = array();
    /* @var array of language string used for rendering*/
    public $language = array();

    /* @var boolean $allowAjax for direct request : allow to show as ajax */
    private $allowAjax = true;

    /* @var integer[] childsSurveyIdDone used to not update toen tabke mutiple time.*/
    private $childsSurveyIdDone = [];

    public function __construct($surveyId)
    {
        $this->surveyId = $this->originalSurveyId = $surveyId;
        $this->setFinalSurveyId();
    }

    /**
     * Action to list participant of current survey
     */
    public function actionListParticipant()
    {
        $this->setViewSettings();
        $this->checkManagerToken();
        $this->showParticipantsInSurvey();
    }

    /**
     * Action to list participant of current survey
     */
    public function actionNewUser()
    {
        $this->setViewSettings();
        $this->checkRight('create');
        $this->createNewuser();
    }

    /**
     * Action to send an email to a participant
     */
    public function actionEmail()
    {
        $this->setViewSettings();
        $this->checkRight('message');
        $this->sendMessageToUser();
    }

    /**
     * Action to edit a participant of current survey
     */
    public function actionEdit()
    {
        $this->setViewSettings();
        $this->checkManagerToken();
        $this->editUser();
    }

    /**
     * Action to list participant of current survey
     */
    public function actionDisable()
    {
        $this->setViewSettings();
        $this->checkManagerToken();
        $this->disableAccess();
    }

    /**
     * Action to list participant of current survey
     */
    public function actionDelete()
    {
        $this->setViewSettings();
        $this->checkManagerToken();
        $this->deleteAccount();
    }

    /**
     * Action to give right to user (assign)
     */
    public function actionAssignUser()
    {
        $this->setViewSettings();
        $this->checkRight('assign');
        $this->assignUserTo();
    }

    /**
     * Action to give right to user (assign)
     */
    public function actionSendMessage()
    {
        $this->setViewSettings();
        $this->checkRight('message');
        $this->sendMessage();
    }

    /**
     * Show the token user list according to current params
     * @param integer $sureyid
     * @param string $token
     * @return void
     */
    private function showParticipantsInSurvey()
    {
        $surveyId = $this->surveyId;
        $token = $this->token;
        $this->allowAjax = false;
        $this->aTokenUsersListInfo['usersGridList'] = $this->getUsersGridList($surveyId, $token);
        $this->aTokenUsersListInfo['toolUrls'] = array(
            'survey' => null,
            'myaccount' => null,
            'createuser' => null,
        );
        if ($token) {
            $returnSurveyId = App()->getRequest()->getParam('returnsurveyid', $this->originalSurveyId);
            $params =  array(
                "sid" => $returnSurveyId,
                'token' => $token,
            );
            if (App()->getRequest()->getParam('originalSrid')) {
                $params['srid'] = App()->getRequest()->getParam('originalSrid');
            }
            $this->aTokenUsersListInfo['toolUrls']['survey'] = App()->createUrl("survey/index", $params);
            if ($this->checkRight('myaccount', false)) {
                $this->aTokenUsersListInfo['toolUrls']['myaccount'] = App()->createUrl(
                    "plugins/direct",
                    array(
                        "plugin" => 'TokenUsersListAndManage',
                        "surveyid" => $surveyId,
                        "function" => "myaccount",
                    )
                );
            }
        }
        $allowedCreateUser = $this->checkRight('create', false);
        if ($allowedCreateUser) {
            $this->aTokenUsersListInfo['toolUrls']['createuser'] = App()->createUrl(
                "plugins/direct",
                array(
                    "plugin" => 'TokenUsersListAndManage',
                    "surveyid" => $this->originalSurveyId,
                    "function" => "newuser"
                )
            );
        }
        $this->aSurveyInfo['include_content'] = 'TULAM_listusers';
        $this->language = array_merge($this->language, array(
            "Edit user" => self::translate("Edit user"),
            "Disable access" => self::translate("Disable access"),
            "Create a user and send an email with survey link." => self::translate("Create a user and send an email with survey link."),
            "Create a user" => self::translate("Create a user"),
            "Update my account" => self::translate("Update my account"),
            "Return to survey" => self::translate("Return to survey"),
            "Send a message to %s." => self::translate("Send a message to %s."),
        ));

        $this->rendertwig();
        App()->end();
    }

    /**
     * get the HTML for response list
     * @param integer $sureyid
     * @param string $token
     * @return string
     */
    private function getUsersGridList()
    {
        $surveyId = $this->surveyId;
        $model = \TokenManaged::model($surveyId);

        $model->setScenario('search');
        $filters = Yii::app()->request->getParam('TokenManaged');
        $model->setAttributes($filters, false);
        if (!empty($filters['active'])) {
            $model->active = $filters['active'];
        }
        $model->currentToken = $this->token;
        $model->originalSid = $this->originalSurveyId;
        $model->pluginName = 'TokenUsersListAndManage';
        if (!$this->checkRight('message', false)) {
            $model->availableButtons['email'] = false;
            $model->availableButtons['invalidemail'] = false;
        }
        $columns = $model->getGridColumns();

        $aRenderData['model'] = $model;
        $aRenderData['filters'] = $filters;
        $aRenderData['columns'] = $columns;
        return App()->getController()->renderPartial(
            "TokenUsersListAndManagePlugin.views.listUsers",
            $aRenderData,
            true
        );
    }

    /**
     * Update my account
     */
    public function actionMyAccount()
    {
        $this->setViewSettings();
        $surveyId = $this->surveyId;
        $token = $this->token;
        $aSurveyInfo = $this->aSurveyInfo;
        $oOriginalManagerToken = $oManagerToken = TokenManaged::model($surveyId)->findByToken($token);
        if (empty($oManagerToken)) {
            $this->throwAdaptedException(400, $this->translate("Invalid parameters"));
        }
        /* Check access */
        $allowEditAccount = Utilities::getSetting($surveyId, 'allowEditAccount');
        $tokenAttributeGroupManager = \TokenUsersListAndManagePlugin\Utilities::getTokenAttributeGroupManager($surveyId);
        $tokenAttributeRestricted = Utilities::getSetting($surveyId, 'tokenAttributeRestricted');
        $bUserManager = !empty($oUserToken->$tokenAttributeGroupManager);
        $bUserRestricted = !empty($oUserToken->$tokenAttributeRestricted);
        $allowedEditAccount = ($allowEditAccount == 'admin' && $bUserManager)
            || ($allowEditAccount == 'user' && ($bUserManager || !$bUserRestricted))
            || $allowEditAccount == 'all';
        if (!$allowedEditAccount) {
            $this->throwAdaptedException(403, $this->translate("No access to this page"));
        }
        $htmlErrors = null;
        $aSubmitErrors = null;
        if (App()->getRequest()->isPostRequest) {
            $oManagerToken = $this->setUserAttribute($this->token);
            if (empty($oManagerToken->getErrors())) {
                $this->language['Account updated successfully.'] = self::translate('Account updated successfully.');
                $this->aSurveyInfo['include_content'] = "TULAM_message";
                $this->aTokenUsersListInfo['messagefile'] = 'TULAM_myaccount_success';
                $this->rendertwig();
            }
            $aSubmitErrors = $oManagerToken->getErrors();
            $htmlErrors = CHtml::errorSummary($oManagerToken, self::translate("Please review your account"));
        }
        $aForm = array();
        $params = array(
            "plugin" => 'TokenUsersListAndManage',
            "surveyid" => $this->originalSurveyId,
            "function" => "myaccount",
        );
        if (intval(App()->getRequest()->getQuery('qid'))) {
            $params['qid'] = intval(App()->getRequest()->getQuery('qid'));
        }
        if (intval(App()->getRequest()->getQuery('srid'))) {
            $params['srid'] = intval(App()->getRequest()->getQuery('srid'));
        }
        $aForm['action'] = App()->createUrl(
            "plugins/direct",
            $params
        );
        $aForm['htmlErrors'] = $htmlErrors;
        $aForm['aSubmitErrors'] = $aSubmitErrors;

        $this->aTokenUsersListInfo['aForm'] = $aForm;
        $this->aTokenUsersListInfo['userData'] = $this->getUserFormData($token, $token);
        $this->aTokenUsersListInfo['oManagerToken'] = $oManagerToken;
        $this->aTokenUsersListInfo['oUserToken'] = $oManagerToken;
        $this->aTokenUsersListInfo['oOriginalUserToken'] = $oOriginalManagerToken;
        $this->language = array_merge($this->language, array(
            "Edit my account" => self::translate("Edit my account"),
            "My information" => self::translate("My information"),
            "First name" => self::translate("First name"),
            "Last name" => self::translate("Last name"),
            "Email" => self::translate("Email"),
            "Confirm your email" => self::translate("Confirm your email"),
            "Only needed to update the email" => self::translate("Only needed to update the email"),
            "Update" => self::translate("Update"),
        ));
        $this->aSurveyInfo['include_content'] = 'TULAM_myaccount';
        $this->rendertwig();
        App()->end();
    }

    /**
     * send an email to a new user
     */
    private function createNewuser()
    {
        $surveyId = $this->surveyId;
        $srid = $this->srid;
        $token = $this->token;
        $oSurvey = Survey::model()->findByPk($surveyId);
        $aSurveyInfo = $this->aSurveyInfo;
        $oOriginalSurvey = Survey::model()->findByPk($this->originalSurveyId);
        $aOriginalSurveyInfo = getSurveyInfo($this->originalSurveyId, Yii::app()->getLanguage());
        $htmlErrors = null;
        $aSubmitErrors = null;
        $oOriginalUserToken = $oUserToken = $this->getEmptyUser();
        $oManagerToken = null;
        if ($token) {
            $oManagerToken = Token::model($surveyId)->findByToken($token);
        }
        $mailtype = "register";
        $noMessage = Utilities::getSetting($surveyId, 'tokenEmailUsage') == 'hidden';
        $allowNoMessage = Utilities::getSetting($this->originalSurveyId, 'allowNoMessageCreate');
        $forcedTextMessage =  Utilities::getSetting($this->originalSurveyId, 'forcedSendMessageCreate');

        if (App()->getRequest()->isPostRequest) {
            $oUserToken = $this->findExistingUser();
            $sendMessage = (!$allowNoMessage || App()->getRequest()->getPost('tulam_sendMessage') !== '') && !$noMessage;
            if ($oUserToken && $oUserToken->token) {
                $this->aTokenUsersListInfo['oUserToken'] = $oUserToken;
                $this->language['This user already exists.'] = self::translate('This user already exists.');
                $this->language['No message was sent.'] = self::translate('No message was sent.');
                $this->language['This user ask to not receive new emails.'] = self::translate('This user ask to not receive new emails.');
                $this->language['User email is invalid (%s).'] = self::translate('User email is invalid (%s).');
                $this->aSurveyInfo['include_content'] = "TULAM_message";
                $this->aTokenUsersListInfo['messagefile'] = 'TULAM_createuser_warning';
                $this->aTokenUsersListInfo['messagenoclose'] = true;
                $this->rendertwig();
            }
            $oUserToken = $this->setUserAttribute(null, $forcedTextMessage && $sendMessage);
            if ($oUserToken->token) {
                $this->setRestrictions($oUserToken->token);
            }
            $this->aTokenUsersListInfo['oUserToken'] = $oUserToken;
            if (empty($oUserToken->getErrors())) {
                $this->language['User created successfully.'] = self::translate('User created successfully.');
                $this->aSurveyInfo['include_content'] = "TULAM_message";
                $this->aTokenUsersListInfo['messagefile'] = 'TULAM_createuser_success';
                $message = $aForm['values']['message'] = App()->getRequest()->getPost("tulam_message");
                if ($sendMessage) {
                    if (!empty($oUserToken->email)) {
                        if ($this->sendMail($oUserToken, $oManagerToken, $message, $mailtype, $srid)) {
                            $this->language['Message send with success'] = self::translate('Message send with success.');
                        } else {
                            $this->aTokenUsersListInfo['mailError'] = true;
                            $this->aTokenUsersListInfo['messagenoclose'] = true;
                            $this->language['An error happen when send the message.'] = self::translate('An error happen when send the message.');
                            $this->language['The token code of the user was %s.'] = self::translate('The token code of the user was %s.');
                        }
                    } else {
                        $this->aTokenUsersListInfo['messagenoclose'] = true;
                        $this->language['No email to send the message.'] = self::translate('No email to send the message.');
                        $this->language['The token code of the user was %s.'] = self::translate('The token code of the user was %s.');
                    }
                } else {
                    $this->aTokenUsersListInfo['nomail'] = true;
                    $this->aTokenUsersListInfo['messagenoclose'] = true;
                    $this->language['The token code of the user was %s.'] = self::translate('The token code of the user was %s.');
                }
                $this->rendertwig();
            }
            $aSubmitErrors = $oUserToken->getErrors();
            $htmlErrors = CHtml::errorSummary($oUserToken, self::translate("Please review participant"));
        }
        $aForm = array(
            'noMessage' => $noMessage,
            'allowNoMessage' => $allowNoMessage,
            'forcedTextMessage' => $forcedTextMessage
        );
        $params = array(
            "plugin" => 'TokenUsersListAndManage',
            "surveyid" => $this->originalSurveyId,
            "function" => "newuser"
        );
        if (intval(App()->getRequest()->getQuery('qid'))) {
            $params['qid'] = intval(App()->getRequest()->getQuery('qid'));
        }
        if (strval(intval($srid)) == strval($srid)) {
            $params['srid'] = $srid;
        }
        $aForm['action'] = App()->createUrl(
            "plugins/direct",
            $params
        );
        $this->setToolsUrlsData();
        $aForm['mailTemplate'] = Utilities::getMailTemplate($this->originalSurveyId, $mailtype, false);
        $aForm['aMailReplace'] = Utilities::getDefaultMailReplace($this->originalSurveyId);
        $aForm['aMailSearch'] = array_keys($aForm['aMailReplace']);

        $aForm['htmlErrors'] = $htmlErrors;
        $aForm['aSubmitErrors'] = $aSubmitErrors;

        $this->aTokenUsersListInfo['aForm'] = $aForm;
        $this->aTokenUsersListInfo['userData'] = $this->getUserFormData($token);
        $this->aTokenUsersListInfo['oUserToken'] = $oUserToken;
        $this->aTokenUsersListInfo['oOriginalUserToken'] = $oOriginalUserToken;
        $this->language = array_merge($this->language, array(
            "Create new user" => self::translate("Create new user"),
            "User information" => self::translate("User information"),
            "First name" => self::translate("First name"),
            "Last name" => self::translate("Last name"),
            "Email" => self::translate("Email"),
            "Send message" => self::translate("Send message"),
            "Send" => self::translate("Send"),
            "Message template" => self::translate("Message template"),
            "Your specific instructions" => self::translate("Your specific instructions"),
            "Create" => self::translate("Create"),
            "Manage restrictions" => self::translate("Manage restrictions"),
            "No existing data in survey" => self::translate("No existing data in survey"),
            "No access" => self::translate("No access"),
        ));
        $this->aSurveyInfo['include_content'] = 'TULAM_createuser';
        $this->rendertwig();
        App()->end();
    }

    /**
     * send an email to an exiting user
     * @return void
     */
    private function sendMessageToUser()
    {
        $surveyId = $this->surveyId;
        $srid = $this->srid;
        $token = $this->token;
        $oUserToken = $this->getUserTokenParam();
        $oManagerToken = null;
        if ($token) {
            $oManagerToken = Token::model($surveyId)->findByToken($token);
        }
        $oSurvey = Survey::model()->findByPk($surveyId);
        $this->aSurveyInfo;

        $mailtype = "invite";
        $htmlErrors = null;
        $aForm = array();
        $params = array(
            "plugin" => 'TokenUsersListAndManage',
            "surveyid" => $this->originalSurveyId,
            "usertoken" => $oUserToken->token,
            "function" => "email"
        );
        if (intval(App()->getRequest()->getQuery('qid'))) {
            $params['qid'] = intval(App()->getRequest()->getQuery('qid'));
        }
        if (strval(intval($srid)) == strval($srid)) {
            $params['srid'] = $srid;
        }
        $aForm['action'] = App()->createUrl(
            "plugins/direct",
            $params
        );
        $this->setToolsUrlsData();
        $aForm['values'] = array(
            'message' => '',
        );
        $aForm['mailError'] = false;
        if (App()->getRequest()->isPostRequest) {
            $message = $aForm['values']['message'] = App()->getRequest()->getPost("tulam_message");
            $forcedTextMessage = Utilities::getSetting($this->originalSurveyId, 'forcedTextMessage');
            if ($forcedTextMessage && empty($message)) {
                /* Add an option */
                $htmlErrors = self::translate("Instruction is mandatory");
            } else {
                if ($this->sendMail($oUserToken, $oManagerToken, $message, $mailtype, $srid)) {
                    $this->language['Message sent successfully'] = self::translate('Message sent successfully');
                    $this->aSurveyInfo['include_content'] = "TULAM_message";
                    $this->aTokenUsersListInfo['messagefile'] = 'TULAM_mailuser_success';
                    $this->rendertwig();
                } else {
                    $aForm['mailError'] = true;
                    $htmlErrors = self::translate("Sorry an error happen when sending message.");
                }
            }
        }

        /* help construction */
        $aForm['mailTemplate'] = Utilities::getMailTemplate($this->originalSurveyId, $mailtype, false);
        $aForm['aMailReplace'] = Utilities::getDefaultMailReplace($this->originalSurveyId);
        $aForm['aMailSearch'] = array_keys($aForm['aMailReplace']);

        $aForm['htmlErrors'] = $htmlErrors;

        $this->aTokenUsersListInfo['aForm'] = $aForm;
        $this->aTokenUsersListInfo['oUserToken'] = $oUserToken;
        $this->language = array_merge($this->language, array(
            "Send a message to %s" => self::translate("Send a message to %s"),
            "Send message" => self::translate("Send message"),
            "Send" => self::translate("Send"),
            "Message template" => self::translate("Message template"),
            "Your specific instructions" => self::translate("Your specific instructions"),
        ));

        $this->aSurveyInfo['include_content'] = 'TULAM_mailuser';
        $this->rendertwig();
        App()->end();
    }

    /**
     * Edit particpant action : show form and edit participant
     * @return void
     */
    private function editUser()
    {
        $surveyId = $this->surveyId;
        $token = $this->token;
        $oOriginalUserToken =  $oUserToken = $this->getUserTokenParam(true);
        if (empty($oUserToken)) {
            $this->throwAdaptedException(401, self::translate("Invalid user"));
        }
        $oSurvey = Survey::model()->findByPk($surveyId);
        $aSurveyInfo = $this->aSurveyInfo;

        $htmlErrors = null;
        $aSubmitErrors = null;
        if (App()->getRequest()->isPostRequest) {
            $oUserToken = $this->setUserAttribute($oUserToken->token);
            $this->setRestrictions($oUserToken->token);
            if (empty($oUserToken->getErrors())) {
                $tokenattribute = App()->getRequest()->getPost('tokenattribute');
                $activeSubmitted = isset($tokenattribute['active']) ? $tokenattribute['active'] : null;
                $now = dateShift(date("Y-m-d H:i:s"), "Y-m-d H:i:s", App()->getConfig('timeadjust'));
                if ($activeSubmitted === '') {
                    if (empty($oUserToken->validuntil) || $oUserToken->validuntil > $now) {
                        $oUserToken->validuntil = $now;
                        $oUserToken->update('validuntil');
                    }
                } elseif ($activeSubmitted === 'Y') {
                    $oUserToken->validuntil = null;
                    $oUserToken->update('validuntil');
                    if ($oUserToken->validfrom && $oUserToken->validfrom > $now) {
                        $oUserToken->validfrom = $now;
                        $oUserToken->update('validfrom');
                    }
                }
                $this->language['Participant updated with succes'] = self::translate('Participant updated with succes');
                $this->aSurveyInfo['include_content'] = "TULAM_message";
                $this->aTokenUsersListInfo['messagefile'] = 'TULAM_edituser_success';
                $this->rendertwig();
            }
            $aSubmitErrors = $oUserToken->getErrors();
            $htmlErrors = CHtml::errorSummary($oUserToken, self::translate("Please review participant"));
        }
        $aForm = array();
        $aForm['action'] = App()->createUrl(
            "plugins/direct",
            array(
                "plugin" => 'TokenUsersListAndManage',
                "surveyid" => $this->originalSurveyId,
                "usertoken" => $oUserToken->token,
                "function" => "edit"
            )
        );
        $aForm['htmlErrors'] = $htmlErrors;
        $aForm['aSubmitErrors'] = $aSubmitErrors;
        $this->aTokenUsersListInfo['aForm'] = $aForm;
        $this->aTokenUsersListInfo['userData'] = $this->getUserFormData($token, $oUserToken->token);
        $this->aTokenUsersListInfo['oUserToken'] = $oUserToken;
        $this->aTokenUsersListInfo['oOriginalUserToken'] = $oOriginalUserToken;
        $this->language = array_merge($this->language, array(
            "Edit user %s" => self::translate("Edit user %s"),
            "User information" => self::translate("User information"),
            "First name" => self::translate("First name"),
            "Last name" => self::translate("Last name"),
            "Email" => self::translate("Email"),
            "Update" => self::translate("Update"),
            "Active" => self::translate("Active"),
            "Manage restrictions" => self::translate("Manage restrictions"),
            "No existing data in survey" => self::translate("No existing data in survey"),
            "No access" => self::translate("No access"),
        ));
        $this->updateTokenInExtrasurveys($surveyId, $oUserToken->token);
        $this->aSurveyInfo['include_content'] = 'TULAM_edituser';
        $this->rendertwig();
        App()->end();
    }

    /**
     * Disable access of a particpant and remove all information
     * @return void
     */
    private function deleteAccount()
    {
        $surveyId = $this->surveyId;
        $token = $this->token;
        $oUserToken = $this->getUserTokenParam(true);
        if (empty($oUserToken)) {
            $this->throwAdaptedException(401, self::translate("Invalid user"));
        }
        $tokenAttributeGroup = \TokenUsersListAndManagePlugin\Utilities::getTokenAttributeGroup($surveyId);
        $tokenAttributeGroupManager = \TokenUsersListAndManagePlugin\Utilities::getTokenAttributeGroupManager($surveyId);
        $oSurvey = Survey::model()->findByPk($surveyId);
        $aSurveyInfo = $this->aSurveyInfo;
        $aForm = array();
        if (App()->getRequest()->isPostRequest) {
            if (App()->getRequest()->getPost('confirm') == 'confirm') {
                $oUserToken->setDeleted($this->token);
                $this->language['All account informations deleted.'] = self::translate('All account informations deleted.');
                $this->language['User access disabled.'] = self::translate('User access disabled.');
                $this->aSurveyInfo['include_content'] = "TULAM_message";
                $this->aTokenUsersListInfo['messagefile'] = 'TULAM_deleteaccount_success';
                $this->rendertwig();
            } elseif (App()->getRequest()->getPost('cancel')) {
                $this->language['Cancelled'] = self::translate('Cancelled, account informations not updated.');
                $this->aSurveyInfo['include_content'] = "TULAM_message";
                $this->aTokenUsersListInfo['messagefile'] = 'TULAM_deleteaccount_success';
                $this->rendertwig();
            } else {
                $this->throwAdaptedException(400, $message);
            }
            App()->end();
        }
        $aForm['action'] = App()->createUrl(
            "plugins/direct",
            array(
                "plugin" => 'TokenUsersListAndManage',
                "surveyid" => $this->originalSurveyId,
                "function" => 'empty',
                "usertoken" => $oUserToken->token,
            )
        );
        $this->aTokenUsersListInfo['aForm'] = $aForm;
        $this->aTokenUsersListInfo['oUserToken'] = $oUserToken;
        $this->language = array_merge($this->language, array(
            "Delete account" => self::translate("Delete account"),
            "Do you confirm you want delete all account informations of %s?" => self::translate("Do you confirm you want delete all account informations of %s?"),
            "This action is definitive." => self::translate("This action is definitive."),
            "Only account informations are delete, no data entered by this user was deleted." => self::translate("Only account informations are delete, no data entered by this user was deleted."),
            "Confirm" => self::translate("Confirm"),
            "Cancel" => self::translate("Cancel"),
        ));

        $this->aSurveyInfo['include_content'] = 'TULAM_deleteaccount';
        $this->rendertwig();
        App()->end();
    }

    /**
     * Disable access of a particpant
     * @return void
     */
    private function disableAccess()
    {
        $surveyId = $this->surveyId;
        $token = $this->token;
        $oUserToken = $this->getUserTokenParam(true);
        if (empty($oUserToken)) {
            $this->throwAdaptedException(401, self::translate("Invalid user"));
        }
        $tokenAttributeGroup = \TokenUsersListAndManagePlugin\Utilities::getTokenAttributeGroup($surveyId);
        $tokenAttributeGroupManager = \TokenUsersListAndManagePlugin\Utilities::getTokenAttributeGroupManager($surveyId);
        $oSurvey = Survey::model()->findByPk($surveyId);
        $aSurveyInfo = $this->aSurveyInfo;
        $aForm = array();
        if (App()->getRequest()->isPostRequest) {
            if (App()->getRequest()->getPost('confirm') == 'confirm') {
                $oUserToken->setDisable();
                $this->language['User access disabled.'] = self::translate('User access disabled.');
                $this->aSurveyInfo['include_content'] = "TULAM_message";
                $this->aTokenUsersListInfo['messagefile'] = 'TULAM_disableuser_success';
                $this->rendertwig();
            } elseif (App()->getRequest()->getPost('cancel')) {
                $this->language['Cancelled'] = self::translate('Cancelled, account informations not updated.');
                $this->aSurveyInfo['include_content'] = "TULAM_message";
                $this->aTokenUsersListInfo['messagefile'] = 'TULAM_disableuser_cancel';
                $this->rendertwig();
            } else {
                $this->throwAdaptedException(400, $message);
            }
            App()->end();
        }
        $aForm['action'] = App()->createUrl(
            "plugins/direct",
            array(
                "plugin" => 'TokenUsersListAndManage',
                "surveyid" => $this->originalSurveyId,
                "function" => 'disable',
                "usertoken" => $oUserToken->token,
            )
        );
        $this->aTokenUsersListInfo['aForm'] = $aForm;
        $this->aTokenUsersListInfo['oUserToken'] = $oUserToken;
        $this->language = array_merge($this->language, array(
            "Disable access" => self::translate("Disable access"),
            "Do you confirm you want to disable access for %s?" => self::translate("Do you confirm you want to disable access for %s?"),
            "Confirm" => self::translate("Confirm"),
            "Cancel" => self::translate("Cancel"),
        ));

        $this->aSurveyInfo['include_content'] = 'TULAM_disableuser';
        $this->rendertwig();
        App()->end();
    }

    /**
     * Show a dropdow with available user for this resonse id and allow assignand send email
     * @return void
     */
    private function assignUserTo()
    {
        $surveyId = $this->surveyId;
        $srid = $this->srid;
        $token = $this->token;
        $oManagerToken = null;
        if ($token) {
            $oManagerToken = Token::model($surveyId)->findByToken($token);
        }
        $oSurvey = Survey::model()->findByPk($surveyId);
        $aSurveyInfo = $this->aSurveyInfo;
        if ($token) {
            $aGroupTokens = \TokenUsersListAndManagePlugin\Utilities::getTokensList($surveyId, $token);
            unset($aGroupTokens[$token]);
        } else {
            $aGroupTokens = Token::model($surveyId)->findAll("token <> ''");
        }

        $tokenListData = array();
        $tokenListOption = array();
        $tokenAttributeRestricted = Utilities::getSetting($surveyId, 'tokenAttributeRestricted');
        if (!Utilities::getSetting($this->originalSurveyId, 'useAttributeRestricted')) {
            $tokenAttributeRestricted = null;
        }
        if (empty($tokenAttributeRestricted)) {
            $this->throwAdaptedException(403, self::translate("No restrictions."));
        }
        $now = dateShift(date("Y-m-d H:i:s"), "Y-m-d H:i:s", Yii::app()->getConfig("timeadjust"));
        if (!empty($aGroupTokens)) {
            foreach ($aGroupTokens as $selecttoken) {
                /* check validity when search (using editable) */
                $oToken = Token::model($surveyId)->editable()->findByToken($selecttoken);
                if (empty($oToken)) {
                    continue;
                }
                if (!empty($oToken->$tokenAttributeRestricted) && !$this->getHaveAllAccess($this->originalSurveyId, $selecttoken)) {
                    $oTokenSridAccess = models\TokenSridAccess::model()->findByPk(array(
                        'sid' => $this->originalSurveyId,
                        'token' => $selecttoken,
                        'srid' => $srid,
                    ));
                    if (empty($oTokenSridAccess)) {
                        $description = implode(" ", array($oToken->firstname,$oToken->lastname));
                        if (empty($description)) {
                            $description = $oToken->token;
                        }
                        $tokenListData[$selecttoken] = $description;
                        if (empty($oToken->email) || $oToken->emailstatus != "OK") {
                            //$tokenListOption[$selecttoken] = array('disabled' => 'disabled');
                        }
                    }
                }
            }
        }
        $noMessage = Utilities::getSetting($surveyId, 'tokenEmailUsage') == 'hidden';
        $allowNoMessage =  Utilities::getSetting($this->originalSurveyId, 'allowNoMessageAssign');
        $forcedTextMessage =  Utilities::getSetting($this->originalSurveyId, 'forcedTextMessage');

        $mailtype = "remind";
        $htmlErrors = null;
        $aForm = array(
            'noMessage' => $noMessage,
            'allowNoMessage' => $allowNoMessage,
            'forcedTextMessage' => $forcedTextMessage
        );
        $params = array(
            "plugin" => 'TokenUsersListAndManage',
            "surveyid" => $this->originalSurveyId,
            "function" => 'assignuser'
        );
        if (intval(App()->getRequest()->getQuery('qid'))) {
            $params['qid'] = intval(App()->getRequest()->getQuery('qid'));
        }
        if (strval(intval($srid)) == strval($srid)) {
            $params['srid'] = $srid;
        }
        $aForm['action'] = App()->createUrl(
            "plugins/direct",
            $params
        );
        /* Get the token list for this user */
        $aForm['tokens'] = array(
            'listData' => $tokenListData,
            'listOption' => $tokenListOption,
            'selected' => ''
        );
        $this->setToolsUrlsData();
        $aForm['values'] = array(
            'message' => '',
        );
        $aForm['mailError'] = false;
        if (App()->getRequest()->isPostRequest) {
            $this->aTokenUsersListInfo['oUserToken'] = $oUserToken = $this->getUserTokenParam(true);
            if (empty($oUserToken)) {
                /* Can be an ivalid token too */
                $htmlErrors = self::translate("You must choose a token");
            } else {
                $sendMessage = (!$allowNoMessage || App()->getRequest()->getPost('tulam_sendMessage') !== '') && !$noMessage;
                $message = $aForm['values']['message'] = App()->getRequest()->getPost("tulam_message");
                if ($sendMessage && $forcedTextMessage && trim($message) == "") {
                    $htmlErrors = self::translate("Instruction is mandatory");
                } else {
                    $this->language = array_merge($this->language, array(
                        "User assigned successfully." => self::translate("User assigned successfully."),
                        "An error happen when send the message." => self::translate("An error happen when send the message."),
                        "No email to send the message." => self::translate("No email to send the message."),
                        "No email adress for this user." => self::translate("No email adress for this user."),
                        "Email status for this user is invalid (%s)." => self::translate("Email status for this user is invalid (%s)."),
                    ));
                    $aForm['tokens']['selected'] = $oUserToken->token;
                    /* First : find if this token exist in this survey */
                    $this->updateTokenInExtrasurveys($surveyId, $oUserToken->token);
                    $oTokenSridAccess = models\TokenSridAccess::model()->findByPk(array(
                        'sid' => $this->originalSurveyId,
                        'token' => $oUserToken->token,
                        'srid' => $srid,
                    ));
                    /* Strangelyu : must be , else giving right to an user with already right … */
                    if (empty($oTokenSridAccess)) {
                        $oTokenSridAccess = new models\TokenSridAccess();
                        $oTokenSridAccess->sid = $this->originalSurveyId;
                        $oTokenSridAccess->token = $oUserToken->token;
                        $oTokenSridAccess->srid = $srid;
                        $oTokenSridAccess->save();
                    }
                    if ($sendMessage) {
                        if ($oUserToken->email && $oUserToken->emailstatus == "OK") {
                            if ($this->sendMail($oUserToken, $oManagerToken, $message, $mailtype, $srid)) {
                                $this->language['Message sent successfully'] = self::translate('Message sent successfully');
                                $this->aSurveyInfo['include_content'] = "TULAM_message";
                                $this->aTokenUsersListInfo['messagefile'] = 'TULAM_mailuser_success';
                                $this->rendertwig();
                            } else {
                                $this->aTokenUsersListInfo['messagenoclose'] = true;
                                $this->aTokenUsersListInfo['mailError'] = true;
                                $htmlErrors = self::translate("Sorry an error happen when sending message.");
                            }
                        } else {
                            $this->aTokenUsersListInfo['messagenoclose'] = true;
                            $this->aTokenUsersListInfo['mailWarning'] = true;
                        }
                    }
                    $this->aTokenUsersListInfo['aForm'] = $aForm;
                    $this->aSurveyInfo['include_content'] = "TULAM_message";
                    $this->aTokenUsersListInfo['messagefile'] = 'TULAM_assignuser_success';
                    $this->rendertwig();
                }
            }
        }

        /* help construction */
        $aForm['mailTemplate'] = Utilities::getMailTemplate($this->originalSurveyId, $mailtype, false);
        $aForm['aMailReplace'] = Utilities::getDefaultMailReplace($this->originalSurveyId);
        $aForm['aMailSearch'] = array_keys($aForm['aMailReplace']);

        $aForm['htmlErrors'] = $htmlErrors;

        $this->aTokenUsersListInfo['aForm'] = $aForm;
        $this->language = array_merge($this->language, array(
            "Assign user" => self::translate("Assign user"),
            "Send message" => self::translate("Send message"),
            "User" => self::translate("User"),
            "Select user" => self::translate("Select user"),
            "Select…" => self::translate("Select…"),
            "Send message" => self::translate("Send message"),
            "Assign and send message" => self::translate("Assign and send message"),
            "Send message" => self::translate("Send message"),
            "Message template" => self::translate("Message template"),
            "Your specific instructions" => self::translate("Your specific instructions"),
        ));
        $this->aSurveyInfo['include_content'] = 'TULAM_assignuser';
        $this->rendertwig();
        App()->end();
    }

    /**
     * Show dropdown with available user and allow to send email
     * @return void
     */
    private function sendMessage()
    {
        $surveyId = $this->surveyId;
        $srid = $this->srid;
        $token = $this->token;
        $oManagerToken = null;
        if ($token) {
            $oManagerToken = Token::model($surveyId)->findByToken($token);
        }
        $oSurvey = Survey::model()->findByPk($surveyId);
        $this->aSurveyInfo = $aSurveyInfo = getSurveyInfo($surveyId, Yii::app()->getLanguage());
        if ($token) {
            $aGroupTokens = \TokenUsersListAndManagePlugin\Utilities::getTokensList($surveyId, $token);
            unset($aGroupTokens[$token]);
        } else {
            $aGroupTokens = Token::model($surveyId)->findAll("token <> ''");
        }

        $tokenListData = array();
        $aTokenUsersAttributes = $this->extractAttributesData();
        $tokenAttributeManager = Utilities::getSetting($surveyId, 'tokenAttributeManager');
        $managerTitle = self::translate("Manager");
        if (!empty($aTokenUsersAttributes[$tokenAttributeManager]['description'])) {
            $managerTitle = $aTokenUsersAttributes[$tokenAttributeManager]['description'];
        } elseif (!empty($aTokenUsersAttributes[$tokenAttributeManager]['caption'])) {
            $managerTitle = $aTokenUsersAttributes[$tokenAttributeManager]['caption'];
        }
        $userTitle = self::translate("User");
        $tokenListDataByCategories = array(
            $managerTitle => array(),
            $userTitle => array(),
        );
        $tokenListOption = array();
        $tokenAttributeRestricted = Utilities::getSetting($surveyId, 'tokenAttributeRestricted');
        if ($tokenAttributeRestricted) {
            $restrictedTitle = self::translate("restricted");
            if (!empty($aTokenUsersAttributes[$tokenAttributeRestricted]['description'])) {
                $restrictedTitle = $aTokenUsersAttributes[$tokenAttributeRestricted]['description'];
            } elseif (!empty($aTokenUsersAttributes[$tokenAttributeRestricted]['caption'])) {
                $restrictedTitle = $aTokenUsersAttributes[$tokenAttributeRestricted]['caption'];
            }
        }
        $now = dateShift(date("Y-m-d H:i:s"), "Y-m-d H:i:s", Yii::app()->getConfig("timeadjust"));
        $aAllTokens = array();
        if (!empty($aGroupTokens)) {
            $criteria = new CDBCriteria();
            $criteria->order = "lastname,firstname,tid";
            $criteria->addInCondition('token', $aGroupTokens);
            $oContactTokens = TokenManaged::model($surveyId)->findAll($criteria);
            foreach ($oContactTokens as $oContactToken) {
                /* check validity when search (using editable) */
                if (!$oContactToken->getActive()) {
                    continue;
                }
                $haveAccess = true;
                if (Utilities::getSetting($this->originalSurveyId, 'useAttributeRestricted') && $oContactToken->getIsRestricted()) {
                    $oTokenSridAccess = models\TokenSridAccess::model()->findByPk(array(
                        'sid' => $this->originalSurveyId,
                        'token' => $oContactToken->token,
                        'srid' => $srid,
                    ));
                    if (empty($oTokenSridAccess) && !$this->getHaveAllAccess($this->originalSurveyId, $oContactToken->token)) {
                        $haveAccess = false;
                    }
                }
                $aAllTokens[$oContactToken->token] = $oContactToken->getAttributes();
                $tokenListData[$oContactToken->token] = array();
                $tokenListOption[$oContactToken->token] = array();
                $description = implode(" ", array($oContactToken->firstname,$oContactToken->lastname));
                if (empty($description)) {
                    $description = $oContactToken->token;
                }
                $aAllTokens[$oContactToken->token]['description'] = $description;
                if ($haveAccess) {
                    $tokenListData[$oContactToken->token] = $description;
                }
                if ($oContactToken->getIsManager()) {
                    $aAllTokens[$oContactToken->token]['status'] = 'manager';
                    $aAllTokens[$oContactToken->token]['i18nstatus'] = $managerTitle;
                    $tokenListDataByCategories[$managerTitle][$oContactToken->token] = $description;
                } elseif ($oContactToken->getIsRestricted()) {
                    $aAllTokens[$oContactToken->token]['status'] = 'restricted';
                    $aAllTokens[$oContactToken->token]['i18nstatus'] = $restrictedTitle;
                    $tokenListDataByCategories[$restrictedTitle][$oContactToken->token] = $description;
                } else {
                    $aAllTokens[$oContactToken->token]['status'] = 'user';
                    $aAllTokens[$oContactToken->token]['i18nstatus'] = $userTitle;
                    $tokenListDataByCategories[$userTitle][$oContactToken->token] = $description;
                }
                if (empty($oContactToken->email) || $oContactToken->emailstatus != "OK" || !$haveAccess) {
                    $tokenListOption[$oContactToken->token] = array('disabled' => 'disabled');
                    if (!$haveAccess) {
                        $tokenListOption[$oContactToken->token]['title'] = self::translate("No access");
                    } elseif (empty($oContactToken->email)) {
                        $tokenListOption[$oContactToken->token]['title'] = self::translate("No email");
                    } else {
                        $tokenListOption[$oContactToken->token]['title'] = sprintf(self::translate("Invalid email (%s)"), $oContactToken->emailstatus);
                    }
                }
            }
        }
        $forcedTextMessage = Utilities::getSetting($this->originalSurveyId, 'forcedTextMessage');

        $aForm = array(
            'allowNoMessage' => false,
            'forcedTextMessage' => $forcedTextMessage
        );
        $aForm['tokens'] = array(
            'listData' => $tokenListData,
            'listDataByCategories' => $tokenListDataByCategories,
            'listOption' => $tokenListOption,
            'aAllTokens' => $aAllTokens,
            'selected' => ''
        );

        $mailtype = "remind";
        $htmlErrors = null;
        $mailtype = "remind";
        $htmlErrors = null;
        $params = array(
            "plugin" => 'TokenUsersListAndManage',
            "surveyid" => $this->originalSurveyId,
            "function" => "sendmessage"
        );
        if (intval(App()->getRequest()->getQuery('qid'))) {
            $params['qid'] = intval(App()->getRequest()->getQuery('qid'));
        }
        if (strval(intval($srid)) == strval($srid)) {
            $params['srid'] = $srid;
        }
        $aForm['action'] = App()->createUrl(
            "plugins/direct",
            $params
        );
        $this->setToolsUrlsData();
        $aForm['values'] = array(
            'message' => '',
        );
        if (App()->getRequest()->isPostRequest) {
            $message = $aForm['values']['message'] = trim(App()->getRequest()->getPost("tulam_message"));
            if ($forcedTextMessage && empty($message)) {
                $aForm['htmlErrors'] = self::translate("Instruction is mandatory");
                $aForm['mailError'] = true;
            } else {
                $oUserTokens = $this->getUserTokenParam(true, true);
                if (empty($oUserTokens)) {
                    /* Can be an ivalid token too */
                    $aForm['htmlErrors'] = self::translate("You must choose a token");
                    $aForm['mailError'] = true;
                } else {
                    $aForm['mailError'] = false;
                    $htmlErrors = [];
                    foreach($oUserTokens as $oUserToken) {
                        $aForm['selected'][] = $oUserToken->token;
                        if (empty($oUserToken->email) || $oUserToken->emailstatus != "OK") {
                            $htmlErrors[] = sprintf(self::translate("%s user have an invalid email address."), $oUserToken->firstname . " " . $oUserToken->lastname);
                        } else {
                            if (!$this->sendMail($oUserToken, $oManagerToken, $message, $mailtype, $srid)) {
                                $aForm['mailError'] = true;
                                $htmlErrors[] = sprintf(self::translate("Sorry an error happen when sending message for %s."), $oUserToken->firstname . " " . $oUserToken->lastname);
                            }
                        }
                    }
                    /* The errors */
                    if (!$aForm['mailError']) {
                        $this->language['Message sent successfully'] = self::translate('Message sent successfully');
                        $this->aSurveyInfo['include_content'] = "TULAM_message";
                        $this->aTokenUsersListInfo['messagefile'] = 'TULAM_mailuser_success';
                        $this->rendertwig();
                    }
                    if (count($oUserTokens > 1)) {
                        $aForm['htmlErrors'] = '<ul>';
                        foreach($htmlErrors as $htmlError) {
                            $aForm['htmlErrors'] .= '<li>' . $htmlError . '</li>';
                        }
                         $aForm['htmlErrors'] .= '</ul>';
                    } else {
                        $aForm['htmlErrors'] = $htmlErrors[0];
                    }
                }
            }
        }

        /* help construction */
        $aForm['mailTemplate'] = Utilities::getMailTemplate($this->originalSurveyId, $mailtype, false);
        $aForm['aMailReplace'] = Utilities::getDefaultMailReplace($this->originalSurveyId);
        $aForm['aMailSearch'] = array_keys($aForm['aMailReplace']);

        $aForm['htmlErrors'] = $htmlErrors;

        $this->aTokenUsersListInfo['aForm'] = $aForm;
        $this->language = array_merge($this->language, array(
            "Assign user" => self::translate("Assign user"),
            "Send message" => self::translate("Send message"),
            "User" => self::translate("User"),
            "Select user" => self::translate("Select user"),
            "Select…" => self::translate("Select…"),
            "Send message" => self::translate("Send message"),
            "Assign and send message" => self::translate("Assign and send message"),
            "Send message" => self::translate("Send message"),
            "Message template" => self::translate("Message template"),
            "Your specific instructions" => self::translate("Your specific instructions"),
        ));

        $this->aSurveyInfo['include_content'] = 'TULAM_sendmessage';
        $this->rendertwig();
        App()->end();
    }

    /**
     * get usertoken by param
     * @param boolean $allowEmpty
     * @param boolean $asArray
     * @throw Exception
     * @return null|\Token|\Token[]
     */
    private function getUserTokenParam($allowEmpty = false, $asArray = false)
    {
        $usertokens = App()->getRequest()->getParam('usertoken', App()->getRequest()->getParam('userToken'));
        if (empty($usertokens)) {
            if ($allowEmpty) {
                return null;
            }
            $this->throwAdaptedException(400, self::translate("Invalid usertoken parameter"));
        }
        $single = false;
        if (is_string($usertokens)) {
            $single = true;
            $usertokens = [$usertokens];
        }
        $aUserTokens = [];
        if ($this->token) {
            $aGroupTokens = \TokenUsersListAndManagePlugin\Utilities::getTokensList($this->surveyId, $this->token);
            $aFilteredTokens = array_intersect($usertokens, $aGroupTokens);
            if (count($aFilteredTokens) != count($usertokens)) {
                $this->throwAdaptedException(403, self::translate("Invalid usertoken parameter"));
            }
        }
        foreach($usertokens as $usertoken) {
            $oUserToken = \TokenManaged::model($this->surveyId)->findByToken($usertoken);
            if (!empty($oUserToken)) {
                $aUserTokens[] = $oUserToken;
            }
        }
        if($single && !$asArray) {
            return $oUserToken;
        }
        return $aUserTokens;
    }

    /**
     * Check if user is allowed to assign a srid
     * @param string $function (assign, message, create)
     * @param boolean $throwException thiorw exceptuion (or return false)
     * @throw exception if no right
     * @return void
     */
    private function checkRight($function = 'assign', $throw = true)
    {
        $surveyId = $this->surveyId;
        $token = $this->token;
        $srid = $this->srid;

        switch ($function) {
            case 'message':
                $noRightString = self::translate("No right to send email.");
                $allowRight = Utilities::getSetting($surveyId, 'allowSendEmail');
                break;
            case 'create':
                $noRightString = self::translate("No right to create access.");
                $allowRight = Utilities::getSetting($surveyId, 'allowCreateUser');
                break;
            case 'myaccount':
                $noRightString = self::translate("No right to update your account.");
                $allowRight = Utilities::getSetting($surveyId, 'allowEditAccount');
                break;
            case 'assing':
            default:
                $noRightString = self::translate("No right to give access.");
                $allowRight = Utilities::getSetting($surveyId, 'allowGiveRight');
                break;
        }
        if ($allowRight == 'none') {
            if (!$throw) {
                return false;
            }
            $this->throwAdaptedException(403, $noRightString);
        }
        if ($this->isAdmin) {
            return true;
        }
        if (empty($token)) {
            return;
        }
        $tokenAttributeGroupManager = \TokenUsersListAndManagePlugin\Utilities::getTokenAttributeGroupManager($surveyId);
        $tokenAttributeRestricted = Utilities::getSetting($surveyId, 'tokenAttributeRestricted');
        $oUserToken = TokenManaged::model($surveyId)->findByToken($token);
        $bUserManager = !empty($oUserToken->$tokenAttributeGroupManager);
        $bUserRestricted = !empty($oUserToken->$tokenAttributeRestricted);
        if ($allowRight == 'admin') {
            if ($bUserManager) {
                return true;
            }
            if (!$throw) {
                return false;
            }
            $this->throwAdaptedException(403, $noRightString);
        }
        if ($allowRight == 'user') {
            if (!$bUserRestricted) {
                return true;
            }
            if (!$throw) {
                return false;
            }
            $this->throwAdaptedException(403, $noRightString);
        }
        /* User are a restricted user */
        if ($function == 'create') {
            if (!$throw) {
                return false;
            }
            $this->throwAdaptedException(403, $noRightString);
        }
        if ($function == 'assign' && $bUserRestricted) {
            $oSridList = models\TokenSridAccess::model()->findByPk(array(
                'sid' => $this->originalSurveyId,
                'token' => $token,
                'srid' => $srid
            ));
            if (empty($oSridList)) {
                if (!$throw) {
                    return false;
                }
                $this->throwAdaptedException(403, $noRightString);
            }
        }
        return true;
    }

    /**
     * Chek if current user have set Manager right
     * @return boolean
     */
    private function haveSetRightManager()
    {
        $allowSetManager = Utilities::getSetting($this->surveyId, 'allowSetManager');
        if ($allowSetManager == 'none') {
            return false;
        }
        if ($this->isAdmin) {
            return true;
        }
        if ($allowSetManager == 'limesurvey') {
            return false;
        }
        $tokenAttributeGroupManager = \TokenUsersListAndManagePlugin\Utilities::getTokenAttributeGroupManager($this->surveyId);
        $oUserToken = TokenManaged::model($this->surveyId)->findByToken($this->token);
        return !empty($oUserToken->$tokenAttributeGroupManager);
    }

    /**
     * Get the reponse list for a token user
     * @param integer $surveyId
     * @param string $usertoken
     * @param integer|null $qid optionnal id to get the default title
     * @return null|array[] : key is id or response
     */
    private function getResponseList($surveyId, $usertoken = null)
    {
        $baseQuestionId = null;
        if (!Utilities::getSetting($surveyId, 'useAttributeRestricted')) {
            return null;
        }
        if ($surveyId != $this->originalSurveyId) {
            /* Not same survey : must check if it's a child */
            $oChildSurveys = new \RelatedSurveyManagement\ChildrenSurveys($this->originalSurveyId);
            $aChildrensSurveys = $oChildSurveys->getChildrensSurveys();
            if (!in_array($surveyId, $aChildrensSurveys)) {
                return null;
            }
            $baseQuestionId = array_search($surveyId, $aChildrensSurveys);
        }
        $useToken = $this->token;
        if (empty($useToken) && empty($usertoken)) {
            $useToken = $usertoken;
        }
        /* Find the column used for title */
        $availableColumns = \SurveyDynamic::model($surveyId)->getAttributes();
        $aCodeQuestions = array_flip(\getQuestionInformation\helpers\surveyCodeHelper::getAllQuestions($surveyId));
        $columnTitle = "id";
        $responseListTitle = Utilities::getSetting($surveyId, 'responseListTitle');
        if ($responseListTitle) {
            if (array_key_exists($responseListTitle, $availableColumns)) {
                $columnTitle = $responseListTitle;
            } elseif (array_key_exists($responseListTitle, $aCodeQuestions)) {
                $columnTitle = $aCodeQuestions[$responseListTitle];
            }
        }
        if ($columnTitle == "id") {
            if (empty($baseQuestionId)) {
                if (version_compare(\RelatedSurveyManagement\Utilities::API, "0.8", ">=")) {
                    $oParentSurvey = \RelatedSurveyManagement\ParentSurveys::getInstance($surveyId);
                } else {
                    $oParentSurvey = new \RelatedSurveyManagement\ParentSurveys($surveyId);
                }
                $aParentSurveys = $oParentSurvey->getParentSurveys();
                $baseQuestionId = key($aParentSurveys);
            }
            if ($baseQuestionId) {
                $aRelatedPluginsTitle = array_filter(array_column(\RelatedSurveyManagement\Utilities::getAvailableRelatedPlugins(), 'title'));
                $criteria = new CDbCriteria();
                $criteria->compare('qid', $baseQuestionId);
                $criteria->addInCondition('attribute', $aRelatedPluginsTitle);
                $oAttribute = \QuestionAttribute::model()->find($criteria);
                if ($oAttribute && $oAttribute->value) {
                    $responseListTitle = $oAttribute->value;
                    if (array_key_exists($responseListTitle, $availableColumns)) {
                        $columnTitle = $responseListTitle;
                    } elseif (array_key_exists($responseListTitle, $aCodeQuestions)) {
                        $columnTitle = $aCodeQuestions[$responseListTitle];
                    }
                }
            }
        }
        $reponseCriteria = new CDbCriteria();
        $quotedColumnTitle = \App()->getDb()->quoteColumnName($columnTitle);
        if ($columnTitle == "id") {
            $reponseCriteria->select = "id";
        } else {
            $reponseCriteria->select = array(
                'id',
                $quotedColumnTitle
            );
        }
        if ($columnTitle != "id") {
            $reponseCriteria->addCondition("{$quotedColumnTitle} IS NOT NULL and {$quotedColumnTitle} <> ''");
            $reponseCriteria->order = "{$quotedColumnTitle} ASC";
        }
        $reponseCriteria->addInCondition("token", \TokenUsersListAndManagePlugin\Utilities::getTokensList($this->surveyId, $useToken));
        $oResponseList = \Response::model($surveyId)->findAll($reponseCriteria);
        return \CHtml::listData($oResponseList, 'id', $columnTitle);
    }

    /**
     * Get the reponse list for a token user
     * @param integer $surveyId
     * @param string|null $usertoken
     * @return integer[] : key is id or response
     */
    private function getAccesList($surveyId, $usertoken)
    {
        if (empty($usertoken)) {
            return array();
        }
        return models\TokenSridAccess::getReponseForToken($surveyId, $usertoken);
    }

    /**
     * Get the reponse list for a token user
     * @param integer $surveyId
     * @param string|null $usertoken
     * @return boolean
     */
    private function getHaveAllAccess($surveyId, $usertoken)
    {
        return Utilities::getHaveAllAccess($surveyId, $usertoken);
    }

    /**
     * extra the attribute information for edition
     * @param $managerToken manager token
     * @param string|null token
     * @return array
     */
    private function getUserFormData($managerToken, $usertoken = null)
    {
        $surveyId = $this->surveyId;
        $aSurveyInfo = $this->aSurveyInfo;
        if (empty($surveyId) || empty($aSurveyInfo)) {
            $this->throwAdaptedException(500);
        }
        $tokenAttributeGroup = \TokenUsersListAndManagePlugin\Utilities::getTokenAttributeGroup($this->surveyId);
        $tokenAttributeGroupManager = \TokenUsersListAndManagePlugin\Utilities::getTokenAttributeGroupManager($this->surveyId);
        $tokenAttributeRestricted = Utilities::getSetting($surveyId, 'tokenAttributeRestricted');
        $aTokenUsersAttributes = $this->extractAttributesData($usertoken);
        /* Remove invalid attribute set */
        $aSurveyInfo = $this->aSurveyInfo;
        $aAllAttributes = Survey::model()->findByPk($surveyId)->getTokenAttributes();
        $aRegisterAttributes = $this->extractAttributesData($usertoken);
        if (!isset($aAllAttributes[$tokenAttributeGroup])) {
            $tokenAttributeGroup = null;
        }
        $this->aTokenUsersListInfo['tokenAttributeGroup'] = $tokenAttributeGroup;
        if (!isset($aAllAttributes[$tokenAttributeGroupManager])) {
            $tokenAttributeGroupManager = null;
        }
        $this->aTokenUsersListInfo['tokenAttributeGroupManager'] = $tokenAttributeGroupManager;
        if (!isset($aAllAttributes[$tokenAttributeRestricted])) {
            $tokenAttributeRestricted = null;
        }
        $this->aTokenUsersListInfo['tokenAttributeRestricted'] = $tokenAttributeRestricted;
        //~ unset($aRegisterAttributes[$tokenAttributeGroup]);
        //~ unset($aRegisterAttributes[$tokenAttributeGroupManager]);
        //~ unset($aRegisterAttributes[$tokenAttributeRestricted]);
        /* Add the needed attribute at end if not already here */
        if ($tokenAttributeGroup) {
            if ($managerToken && isset($aTokenUsersAttributes[$tokenAttributeGroup])) {
                $aTokenUsersAttributes[$tokenAttributeGroup]['type'] = 'group';
                $aTokenUsersAttributes[$tokenAttributeGroup]['disabled'] = true;
            }
            if (!$managerToken && $this->aTokenUsersListInfo['isAdmin']) {
                if (!isset($aTokenUsersAttributes[$tokenAttributeGroup])) {
                    $aTokenUsersAttributes[$tokenAttributeGroup] = $aAllAttributes[$tokenAttributeGroup];
                }
                $key = $tokenAttributeGroup;
                $aTokenUsersAttributes[$key]['type'] = 'group';
                $aTokenUsersAttributes[$key]['mandatory'] = 'Y';
                if (empty($aTokenUsersAttributes[$key]['description'])) {
                    $aTokenUsersAttributes[$key]['description'] = $aTokenUsersAttributes[$key]['caption'];
                    if (empty($aTokenUsersAttributes[$key]['description'])) {
                        self::translate("Group");
                    }
                }
                $aTokenUsersAttributes[$key]['notin'] = array('myaccount'); /* Can not happen ! */
                $aTokenUsersAttributes[$key]['help'] = self::translate("This is the group of this participant. Update with care.");
            }
        }
        if ($tokenAttributeGroupManager) {
            if ($this->haveSetRightManager() && !isset($aTokenUsersAttributes[$tokenAttributeGroupManager])) {
                /* Need it if have right */
                $aTokenUsersAttributes[$tokenAttributeGroupManager] = $aAllAttributes[$tokenAttributeGroupManager];
            }
            if (isset($aTokenUsersAttributes[$tokenAttributeGroupManager])) {
                $key = $tokenAttributeGroupManager;
                $aTokenUsersAttributes[$key]['type'] = 'groupmanager';
                $aTokenUsersAttributes[$key]['mandatory'] = 'N';
                if (empty($aTokenUsersAttributes[$key]['description'])) {
                    $aTokenUsersAttributes[$key]['description'] = $aTokenUsersAttributes[$key]['caption'];
                    if (empty($aTokenUsersAttributes[$key]['description'])) {
                        $aTokenUsersAttributes[$key]['description'] = self::translate("Is manager");
                    }
                }
                $aTokenUsersAttributes[$key]['disabled'] = !$this->haveSetRightManager();
                $aTokenUsersAttributes[$key]['notin'] = array('myaccount');
                if ($managerToken) {
                    $oManagerToken = Token::model($surveyId)->findByToken($managerToken);
                    $aTokenUsersAttributes[$key]['setValue'] = $oManagerToken->getAttribute($tokenAttributeGroupManager);
                }
            }
        }

        if ($tokenAttributeRestricted) {
            if (!isset($aTokenUsersAttributes[$tokenAttributeRestricted])) {
                /* Need it if have right */
                $aTokenUsersAttributes[$tokenAttributeRestricted] = $aAllAttributes[$tokenAttributeRestricted];
            }
            if (isset($aTokenUsersAttributes[$tokenAttributeRestricted])) {
                $key = $tokenAttributeRestricted;
                $aTokenUsersAttributes[$key]['type'] = 'restricted';
                $aTokenUsersAttributes[$key]['mandatory'] = 'N';
                if (empty($aTokenUsersAttributes[$key]['description'])) {
                    $aTokenUsersAttributes[$key]['description'] = $aTokenUsersAttributes[$key]['caption'];
                    if (empty($aTokenUsersAttributes[$key]['description'])) {
                        self::translate("Is restricted");
                    }
                }
                $aTokenUsersAttributes[$key]['notin'] = array('myaccount');
                $aRestrictedSurveys = array();
                $restrictedSurveyId = $this->originalSurveyId;
                $aTokenUsersAttributes[$key]["listSurveyRestrictedByParent"] = Utilities::getSetting($this->originalSurveyId, 'listSurveyRestrictedByParent');
                if (Utilities::getSetting($this->originalSurveyId, 'listSurveyRestrictedByParent')) {
                    $restrictedSurveyId = $this->surveyId;
                }
                $aTokenUsersAttributes[$key]["useAttributeRestricted"] = Utilities::getSetting($restrictedSurveyId, 'useAttributeRestricted');
                $aTokenUsersAttributes[$key]["allowAllAttributeRestricted"] = Utilities::getSetting($restrictedSurveyId, 'allowAllAttributeRestricted');
                if (!Utilities::getSetting($restrictedSurveyId, 'useAttributeRestricted')) {
                    /* find children surveys */
                    $oChildSurveys = new \RelatedSurveyManagement\ChildrenSurveys($surveyId);
                    $aChildrensSurveys = $oChildSurveys->getChildrensSurveys();
                    $aRestrictedSurveys = array();
                    foreach ($aChildrensSurveys as $childSurveyId) {
                        $oSurvey = Survey::model()->active()->with('languagesettings')->findByPk($childSurveyId);
                        if ($oSurvey && Utilities::getSetting($childSurveyId, 'useAttributeRestricted')) {
                            $responseList = $this->getResponseList($childSurveyId, $managerToken);
                            $aRestrictedSurveys[$childSurveyId] = array(
                                'title' => $oSurvey->getLocalizedTitle(),
                                'responses' => $responseList,
                                'access' => $this->getAccesList($childSurveyId, $usertoken),
                                'allaccess' => $this->getHaveAllAccess($childSurveyId, $usertoken),
                                'allowallaccess' => Utilities::getSetting($childSurveyId, 'allowAllAttributeRestricted'),
                            );
                            $this->language['No response'] = self::translate("No response");
                        }
                    }
                    $aTokenUsersAttributes[$key]["tokenAttributeRestrictedSurveys"] = $aRestrictedSurveys;
                } else {
                    $tokenAttributeRestrictedResponses = array();
                    $responseList = $this->getResponseList($restrictedSurveyId, $managerToken);
                    if (!empty($responseList)) {
                        $tokenAttributeRestrictedResponses = array(
                            'responses' => $responseList,
                            'access' => $this->getAccesList($restrictedSurveyId, $usertoken),
                            'allaccess' => $this->getHaveAllAccess($restrictedSurveyId, $usertoken),
                        );
                    }
                    $aTokenUsersAttributes[$key]["tokenAttributeRestrictedResponses"] = $tokenAttributeRestrictedResponses;
                }
            }
        }
        $this->language['Give all access'] = self::translate("Give all access");
        $this->language['User will have access to all the answers. The current ones and the future ones.'] = self::translate("User will have access to all the answers. The current ones and the future ones.");
        $userFormData = array();
        $coreAttributes = array();
        if (Utilities::getSetting($surveyId, 'tokenFirstnameUsage') != 'hidden') {
            $coreAttributes['firstname'] = array(
                'description' => gT("First name"),
                'mandatory' => Utilities::getSetting($surveyId, 'tokenFirstnameUsage') == 'mandatory' ? "Y" : "",
            );
        }
        if (Utilities::getSetting($surveyId, 'tokenLastnameUsage') != 'hidden') {
            $coreAttributes['lastname'] = array(
                'description' => gT("Last name"),
                'mandatory' => Utilities::getSetting($surveyId, 'tokenLastnameUsage') == 'mandatory' ? "Y" : "",
            );
        }
        if (Utilities::getSetting($surveyId, 'tokenLastnameUsage') != 'hidden') {
            $coreAttributes['lastname'] = array(
                'description' => gT("Last name"),
                'mandatory' => Utilities::getSetting($surveyId, 'tokenLastnameUsage') == 'mandatory' ? "Y" : "",
            );
        }
        if (Utilities::getSetting($surveyId, 'tokenEmailUsage') != 'hidden') {
            $coreAttributes['email'] = array(
                'description' => gT("Email"),
                'type' => 'email',
                'mandatory' => Utilities::getSetting($surveyId, 'tokenEmailUsage') == 'mandatory' ? "Y" : "",
            );
        }
        if (Utilities::getSetting($surveyId, 'tokenTokenShow') != 'hide') {
            $coreAttributes['token'] = array(
                'description' => gT("Token"),
                'type' => 'token',
                'notin' => array('createuser'),
                'disabled' => true,
                'showas' => Utilities::getSetting($surveyId, 'tokenTokenShow'),
                'url' => App()->getController()->createAbsoluteUrl(
                    'survey/index',
                    array(
                        'sid' => $this->originalSurveyId,
                        'token' => $usertoken,
                        'newtest' => 'Y'
                    )
                ),
            );
        }
        $activeAttribute = array(
            "active" => array(
                'description' => $this->translate("Active"),
                'type' => 'active',
                'mandatory' => 'N',
                'notin' => array('myaccount','createuser'),
                'setValue' => "Y",
                'unsetValue' => "",
            ),
        );
        $userForm = array_merge($coreAttributes, $aTokenUsersAttributes, $activeAttribute);
        $userFormData['attributes'] = $userForm;
        $userFormDataEvent = new \PluginEvent('getTULAMUserFormData');
        $userFormDataEvent->set('surveyid', $this->surveyId);
        $userFormDataEvent->set('originalSurveyId', $this->originalSurveyId);
        $userFormDataEvent->set('managerToken', $managerToken);
        $userFormDataEvent->set('usertoken', $usertoken);
        $userFormDataEvent->set('userFormData', $userFormData);
        App()->getPluginManager()->dispatchEvent($userFormDataEvent);
        $userFormData = $userFormDataEvent->get('userFormData');
        return $userFormData;
    }

    /**
     * get the tools URL according to current qid
     * @return array()
     */
    private function setToolsUrlsData()
    {
        $qid = intval(App()->getRequest()->getQuery('qid'));
        if (!$qid) {
            return;
        }
        $originalSurveyId = $this->originalSurveyId;
        $managertoken = $this->token;
        $srid = intval(app()->getRequest()->getQuery('srid'));
        $aAttributes = \QuestionAttribute::model()->getQuestionAttributes($qid);
        if (empty($aAttributes['TulamUserManagement'])) {
            return array();
        }
        if (empty($aAttributes['TulamContactUser'])) {
            return array();
        }
        $toolUrls = array(
            'createuser' => null,
            'assignuser' => null,
            'sendmessage' => null,
        );
        $toolOptions = array(
            'createuser' => array(),
            'assignuser' => array(),
            'sendmessage' => array(),
        );

        $tokenAttributeGroupManager = \TokenUsersListAndManagePlugin\Utilities::getTokenAttributeGroupManager($this->surveyId);
        $tokenAttributeRestricted = Utilities::getSetting($this->surveyId, 'tokenAttributeRestricted');
        $useAttributeRestricted = \TokenUsersListAndManagePlugin\Utilities::getSetting($originalSurveyId, 'useAttributeRestricted');
        $oUserToken = TokenManaged::model($originalSurveyId)->findByToken($managertoken);
        $bUserManager = !empty($oUserToken->$tokenAttributeGroupManager);
        $bUserRestricted = !empty($oUserToken->$tokenAttributeRestricted);
        /* create user : only right */
        $allowRight = Utilities::getSetting($originalSurveyId, 'allowCreateUser');
        $allowCreateUser = \TokenUsersListAndManagePlugin\Utilities::getSetting($originalSurveyId, 'allowCreateUser');
        $allowedCreateUser = ($allowCreateUser == 'admin' && $bUserManager)
            || ($allowCreateUser == 'user' && ($bUserManager || !$bUserRestricted))
            || $allowCreateUser == 'all';
        if ($allowedCreateUser) {
            $toolUrls['newuser'] = App()->createUrl(
                "plugins/direct",
                array(
                    "plugin" => 'TokenUsersListAndManage',
                    "surveyid" => $originalSurveyId,
                    "srid" => $srid,
                    "function" => "newuser",
                    "qid" => $qid,
                )
            );
        }
        /* State of other buttons */
        $haveMessageToken = false;
        $haveAssignToken = false;
        $aGroupTokens = \TokenUsersListAndManagePlugin\Utilities::getTokensList($originalSurveyId, $managertoken);

        foreach ($aGroupTokens as $selecttoken) {
            /* check validity when search (using editable) */
            $oTestToken = TokenManaged::model($originalSurveyId)->findByToken($selecttoken);
            if (empty($oTestToken) || empty($oTestToken->active)) {
                continue;
            }
            $mailsIsValid = !empty($oTestToken->email) && $oTestToken->emailstatus == 'OK';
            if (empty($oTestToken->$tokenAttributeRestricted) || $this->getHaveAllAccess($originalSurveyId, $selecttoken)) {
                $haveMessageToken = $haveMessageToken || $mailsIsValid;
            } else {
                $oTokenSridAccess = models\TokenSridAccess::model()->findByPk(array(
                    'sid' => $originalSurveyId,
                    'token' => $selecttoken,
                    'srid' => $srid,
                ));
                if (empty($oTokenSridAccess)) {
                    $haveAssignToken =  $haveAssignToken || $mailsIsValid;
                } else {
                    $haveMessageToken = $haveMessageToken || $mailsIsValid;
                }
            }
        }
        /* assing user : right, and attribute */
        if ($tokenAttributeRestricted && $useAttributeRestricted) {
            $allowGiveRight = \TokenUsersListAndManagePlugin\Utilities::getSetting($originalSurveyId, 'allowGiveRight');
            $allowedAssignUser = ($allowGiveRight == 'admin' && $bUserManager)
                || ($allowGiveRight == 'user' && ($bUserManager || !$bUserRestricted))
                || $allowGiveRight == 'all';
            $srid = app()->getRequest()->getQuery('srid');
            if ($allowedAssignUser) {
                $toolUrls['assignuser'] = App()->createUrl(
                    "plugins/direct",
                    array(
                        "plugin" => 'TokenUsersListAndManage',
                        "surveyid" => $originalSurveyId,
                        "srid" => $srid,
                        "function" => "assignuser",
                        "qid" => $qid,
                    )
                );
                $toolOptions['assignuser'] = array(
                    'disabled' => !$haveAssignToken
                );
            }
        }
        /* Send message : right and state */
        $allowSendEmail = \TokenUsersListAndManagePlugin\Utilities::getSetting($originalSurveyId, 'allowSendEmail');
        $allowedSendEmail = ($allowSendEmail == 'admin' && $bUserManager)
            || ($allowSendEmail == 'user' && ($bUserManager || !$bUserRestricted))
            || $allowSendEmail == 'all';
        if ($allowedSendEmail) {
            $toolUrls['sendmessage'] = App()->createUrl(
                "plugins/direct",
                array(
                    "plugin" => 'TokenUsersListAndManage',
                    "surveyid" => $originalSurveyId,
                    "srid" => $srid,
                    "function" => "sendmessage",
                    "qid" => $qid,
                )
            );
            $toolOptions['sendmessage'] = array(
                'disabled' => !$haveMessageToken
            );
        }

        $this->language = array_merge($this->language, array(
            "Update my account" => $this->translate("Update my account"),
            "Manage users" => $this->translate("Manage users"),
            "Create a user and send an email with survey link." => self::translate("Create a user and send an email with survey link."),
            "Create a user" => self::translate("Create a user"),
            "Assign user and send an email with survey link." => $this->translate("Assign user and send an email with survey link."),
            "Assign user" => $this->translate("Assign user"),
            "Send message" => $this->translate("Send message"),
            "No valid user to send message" => $this->translate("No valid user to send message"),
            "No user to assign" => $this->translate("No user to assign"),
        ));
        $this->aTokenUsersListInfo['ToolsUrlsData'] = array(
            'toolUrls' => $toolUrls,
            'toolOptions' => $toolOptions,
            'count' => count(array_filter($toolUrls))
        );
    }

    /**
     * Find an existing user by email in same group
     * return \Token|null
     */
    private function findExistingUser()
    {
        $surveyId = $this->surveyId;
        $managertoken = $this->token;
        $tokenAttributeGroup = \TokenUsersListAndManagePlugin\Utilities::getTokenAttributeGroup($surveyId);
        $newAttributes = App()->getRequest()->getPost('tokenattribute');
        if ($managertoken) {
            $oManagerToken = \TokenManaged::model($surveyId)->findByToken($managertoken);
            if ($tokenAttributeGroup && $oManagerToken) {
                $newAttributes[$tokenAttributeGroup] = $oManagerToken->getAttribute($tokenAttributeGroup);
            }
        }
        if (empty($newAttributes['email'])) {
            return;
        }
        $email = trim($newAttributes['email']);
        if (empty($email)) {
            return null;
        }

        $criteria = new CDbcriteria();
        $criteria->compare('email', $email);
        if ($tokenAttributeGroup && isset($newAttributes[$tokenAttributeGroup])) {
            $criteria->compare($tokenAttributeGroup, $newAttributes[$tokenAttributeGroup]);
        }
        $oUserToken = \TokenManaged::model($surveyId)->find($criteria);
        if (!$oUserToken) {
            return null;
        }
        return $oUserToken;
    }

    /**
     * Create an empty token for form
     * @return array (Token)
     */
    private function getEmptyUser()
    {
        TokenManaged::sid($this->surveyId);
        $oToken = new TokenManaged();

        $attributesCopyWhenCreate = array_filter((array) Utilities::getSetting($this->surveyId, 'attributesCopyWhenCreate'));
        if ($this->token && !empty($attributesCopyWhenCreate)) {
            $oManagerToken = Token::model($this->surveyId)->findByToken($this->token);
            if ($oManagerToken) {
                foreach ($attributesCopyWhenCreate as $attribute) {
                    $oToken->setAttribute($attribute, $oManagerToken->getAttribute($attribute));
                }
            }
        }
        return $oToken;
    }

    /**
     * Set the restriction to survey
     * @parameters $token
     * @return void
     */
    private function setRestrictions($usertoken)
    {
        $surveyId = $this->surveyId;
        $tokenAttributeRestricted = Utilities::getSetting($surveyId, 'tokenAttributeRestricted');
        if (empty($tokenAttributeRestricted)) {
            return;
        }
        $restrictedSettings = App()->getRequest()->getPost($tokenAttributeRestricted);
        if (empty($restrictedSettings) || (!isset($restrictedSettings['restrictions']) && !isset($restrictedSettings['allaccess']))) {
            return;
        }
        $restrictions = array();
        if (!empty($restrictedSettings['restrictions'])) {
            $restrictions = (array) $restrictedSettings['restrictions'];
        }
        $allaccess = array();
        if (!empty($restrictedSettings['allaccess'])) {
            $allaccess = (array) $restrictedSettings['allaccess'];
        }
        $aRestrictedSurveys = array();
        $restrictedSurveyId = $this->originalSurveyId;
        /* Show the parent restricted survey */
        if (Utilities::getSetting($this->originalSurveyId, 'listSurveyRestrictedByParent')) {
            $restrictedSurveyId = $this->surveyId;
        }
        if (!Utilities::getSetting($restrictedSurveyId, 'useAttributeRestricted')) {
            $oChildSurveys = new \RelatedSurveyManagement\ChildrenSurveys($surveyId);
            $aChildrensSurveys = $oChildSurveys->getChildrensSurveys();
            foreach ($aChildrensSurveys as $childSurveyId) {
                $oSurvey = Survey::model()->active()->findByPk($childSurveyId);
                if ($oSurvey && Utilities::getSetting($childSurveyId, 'useAttributeRestricted')) {
                    $aRestrictedSurveys[$childSurveyId] = $childSurveyId;
                }
            }
        } else {
            $aRestrictedSurveys[$restrictedSurveyId] = $restrictedSurveyId;
        }
        foreach ($aRestrictedSurveys as $restrictedSurveyId) {
            if (empty($restrictions[$restrictedSurveyId])) {
                $restrictions[$restrictedSurveyId] = array();
            }
            $restricted = $restrictions[$restrictedSurveyId];
            if (Utilities::getSetting($restrictedSurveyId, 'allowAllAttributeRestricted')) {
                if (!empty($allaccess[$restrictedSurveyId])) {
                     models\TokenSridAccess::giveAllAccess($restrictedSurveyId, $usertoken);
                } else {
                    models\TokenSridAccess::deleteAllAccess($restrictedSurveyId, $usertoken);
                }
                unset($restricted['allaccess']);
            }
            models\TokenSridAccess::setRestrictionsToSurvey($restrictedSurveyId, $restricted, $usertoken, $this->token);
        }
    }

    /**
     * Set participant attribute after control
     * @param string $usertoken, if empty : to create
     * @param string $message check message at same time
     * @return void
     */
    private function setUserAttribute($usertoken = null, $checkMessage = false)
    {
        $surveyId = $this->surveyId;
        $currenttoken = $this->token;

        $newPostAttributes = App()->getRequest()->getPost('tokenattribute');
        if (empty($newPostAttributes)) {
            $this->throwAdaptedException(400);
        }
        /* @var array base filter for attribute to update, use keys */
        $baseAllowedAttributes = array_merge(
            array(
                'firstname' => 'firstname',
                'lastname' => 'lastname',
                'email' => 'email',
            ),
            $this->extractAttributesData($usertoken)
        );
        $allAttributes = TokenManaged::model($surveyId)->getAttributes();
        $aAttributesRegister = $this->extractAttributesData($usertoken);
        $tokenAttributeGroup = \TokenUsersListAndManagePlugin\Utilities::getTokenAttributeGroup($surveyId);
        if ($tokenAttributeGroup && !array_key_exists($tokenAttributeGroup, $allAttributes)) {
            $tokenAttributeGroup = null;
        }
        if ($tokenAttributeGroup && !array_key_exists($tokenAttributeGroup, $baseAllowedAttributes)) {
            $baseAllowedAttributes[$tokenAttributeGroup] = $tokenAttributeGroup;
        }
        $tokenAttributeGroupManager = \TokenUsersListAndManagePlugin\Utilities::getTokenAttributeGroupManager($surveyId);
        if ($tokenAttributeGroupManager && !array_key_exists($tokenAttributeGroupManager, $allAttributes)) {
            $tokenAttributeGroupManager = null;
        }
        if ($tokenAttributeGroupManager && !array_key_exists($tokenAttributeGroupManager, $baseAllowedAttributes)) {
            $baseAllowedAttributes[$tokenAttributeGroupManager] = $tokenAttributeGroupManager;
        }
        $tokenAttributeRestricted = Utilities::getSetting($surveyId, 'tokenAttributeRestricted');
        if ($tokenAttributeRestricted  && !array_key_exists($tokenAttributeRestricted, $allAttributes)) {
            $tokenAttributeRestricted = null;
        }
        if ($tokenAttributeRestricted && !array_key_exists($tokenAttributeRestricted, $baseAllowedAttributes)) {
            $baseAllowedAttributes[$tokenAttributeRestricted] = $tokenAttributeRestricted;
        }
        $readonlyAttributes = (array) \TokenUsersListAndManagePlugin\Utilities::getSetting($this->surveyId, 'attributesReadonly');
        $newPostAttributes = array_diff_key($newPostAttributes, array_flip($readonlyAttributes));
        $newAttributes = array_intersect_key($newPostAttributes, $baseAllowedAttributes);
        /* Var boolean updateOwnAccount : need to remove some other value */
        $updateOwnAccount = false;
        if ($usertoken) {
            $oUserToken = TokenManaged::model($surveyId)->findByToken($usertoken);
            $updateOwnAccount =  ($usertoken == $currenttoken);
        } else {
            $oUserToken = Token::create($surveyId);
            $oUserToken->generateToken();
            /* Set default */
            $attributesCopyWhenCreate = array_filter((array) Utilities::getSetting($surveyId, 'attributesCopyWhenCreate'));
            if ($currenttoken && !empty($attributesCopyWhenCreate)) {
                $oManagerToken = Token::model($surveyId)->findByToken($currenttoken);
                if ($oManagerToken) {
                    foreach ($attributesCopyWhenCreate as $attribute) {
                        $oUserToken->setAttribute($attribute, $oManagerToken->getAttribute($attribute));
                    }
                }
            }
        }
        if (isset($newPostAttributes['email']) && $oUserToken->email != $newPostAttributes['email']) {
            $oUserToken->emailstatus = "OK";
        }

        if ($currenttoken && $tokenAttributeGroup) {
            $oCurrentToken = Token::model($surveyId)->findByToken($currenttoken);
            if ($tokenAttributeGroup) {
                $newAttributes[$tokenAttributeGroup] = $oCurrentToken->getAttribute($tokenAttributeGroup);
            }
        }
        if ($updateOwnAccount) {
            unset($newAttributes[$tokenAttributeGroupManager]);
            unset($newAttributes[$tokenAttributeRestricted]);
            unset($newAttributes[$tokenAttributeGroup]);
        }
        if (!$this->haveSetRightManager()) {
            unset($newAttributes[$tokenAttributeGroupManager]);
        }
        if (!empty($newAttributes[$tokenAttributeGroupManager]) && !empty($baseAllowedAttributes[$tokenAttributeRestricted])) {
            $newAttributes[$tokenAttributeRestricted] = "";
        }
        foreach ($newAttributes as $attribute => $value) {
            $oUserToken->$attribute = $value;
        }

        /* core validation */
        $oUserToken->validate();
        /* Extend via plugin */
        if (isset($tokenAttributeGroup)) {
            if ($tokenAttributeGroupManager) {
                unset($aAttributesRegister[$tokenAttributeGroup]);
            }
            if ($tokenAttributeRestricted) {
                unset($aAttributesRegister[$tokenAttributeRestricted]);
            }
        }
        if (Utilities::getSetting($surveyId, 'tokenFirstnameUsage') == 'mandatory' && empty($oUserToken->firstname)) {
            $oUserToken->addError(
                'firstname',
                sprintf(self::translate("%s is mandatory"), self::translate("First name"))
            );
        }
        if (Utilities::getSetting($surveyId, 'tokenLastnameUsage') == 'mandatory' && empty($oUserToken->lastname)) {
            $oUserToken->addError(
                'lastname',
                sprintf(self::translate("%s is mandatory"), self::translate("last name"))
            );
        }
        if (Utilities::getSetting($surveyId, 'tokenEmailUsage') == 'mandatory' && empty($oUserToken->email)) {
            $oUserToken->addError(
                'email',
                sprintf(self::translate("%s is mandatory"), self::translate("Email"))
            );
        }
        foreach ($aAttributesRegister as $attribute => $attributeInformation) {
            if ($currenttoken && $attribute == $tokenAttributeGroup) {
                continue;
            }
            if ($attributeInformation['mandatory'] == 'Y' && empty($oUserToken->$attribute)) {
                $oUserToken->addError(
                    $attribute,
                    sprintf(self::translate("%s is mandatory"), $attributeInformation['caption'])
                );
            }
        }
        if ($updateOwnAccount && ($oUserToken->email != $oCurrentToken->email || !empty($newPostAttributes['email_confirm']))) {
            if ($newAttributes['email'] != $newPostAttributes['email_confirm']) {
                $oUserToken->addError(
                    'email',
                    sprintf(self::translate("Email confirmation is not equal to email"))
                );
                $oUserToken->emailconfirm = $newPostAttributes['email_confirm'];
            }
        }
        /* Find another email with this email */
        if (!empty($oUserToken->email)) {
            $criteria = new CDbcriteria();
            $criteria->compare('email', $oUserToken->email);
            $criteria->compare('token', "<>" . $oUserToken->token);
            if ($tokenAttributeGroup) {
                $criteria->compare($tokenAttributeGroup, $oUserToken->$tokenAttributeGroup);
            }
            $iCountUserEmail = Token::model($surveyId)->count($criteria);
            if ($iCountUserEmail > 0) {
                $oUserToken->addError(
                    'email',
                    sprintf(self::translate("this email was already used by another user"))
                );
            }
        }
        $message = App()->getRequest()->getPost('tulam_message');
        if ($checkMessage && empty($message)) {
            $oUserToken->addError(
                'email',
                sprintf(self::translate("Instructions is mandatory"))
            );
        }

        $beforeSaveUser = new \PluginEvent('beforeTULAMSaveSetUserAttribute');
        $beforeSaveUser->set('surveyid', $this->surveyId);
        $beforeSaveUser->set('originalSurveyId', $this->originalSurveyId);
        $beforeSaveUser->set('managerToken', $currenttoken);
        $beforeSaveUser->set('usertoken', $usertoken);
        $beforeSaveUser->set('newPostAttributes', $newPostAttributes);
        $beforeSaveUser->set('oUserToken', $oUserToken);
        App()->getPluginManager()->dispatchEvent($beforeSaveUser);
        $oUserToken = $beforeSaveUser->get('oUserToken');

        if (!$oUserToken->hasErrors()) {
            $oUserToken->save();
            $this->updateTokenInExtrasurveys($surveyId, $oUserToken->token);
        }
        return $oUserToken;
    }

    /**
     * extra the attribute information for edition
     * @param integer $surveyId
     * @return array
     */
    public static function getExtractedAttributeData($surveyId)
    {
        $TokenUsersListAndManagePlugin = new self($surveyId);
        $TokenUsersListAndManagePlugin->aSurveyInfo = getSurveyInfo($surveyId, App()->getLanguage());
        return $TokenUsersListAndManagePlugin->extractAttributesData();
    }

    /**
     * extra the attribute information for edition
     * @param string $usertoken
     * @return array
     */
    private function extractAttributesData($usertoken = null)
    {
        $aSurveyInfo = $this->aSurveyInfo;
        $aRegisterAttributes = $aSurveyInfo['attributedescriptions'];
        $booleanAttributes = (array) \TokenUsersListAndManagePlugin\Utilities::getSetting($this->surveyId, 'attributesBoolean');
        $readonlyAttributes = (array) \TokenUsersListAndManagePlugin\Utilities::getSetting($this->surveyId, 'attributesReadonly');
        $readonlyAttributesWhenUpdate = (array) \TokenUsersListAndManagePlugin\Utilities::getSetting($this->surveyId, 'attributesReadonlyWhenUpdate');
        $dropdownAttributes = (array) \TokenUsersListAndManagePlugin\Utilities::getSetting($this->surveyId, 'attributesSelect');
        $dropdownTokenSingleAttributes = (array) \TokenUsersListAndManagePlugin\Utilities::getSetting($this->surveyId, 'attributesSelectRestrictToken');
        foreach ($aRegisterAttributes as $key => $aRegisterAttribute) {
            if ($aRegisterAttribute['show_register'] != 'Y') {
                unset($aRegisterAttributes[$key]);
            } else {
                $aRegisterAttributes[$key]['caption'] = (!empty($aSurveyInfo['attributecaptions'][$key]) ? $aSurveyInfo['attributecaptions'][$key] : (!empty($aRegisterAttribute['description']) ? $aRegisterAttribute['description'] : $key));
                /* disabled */
                $aRegisterAttributes[$key]['disabled'] = in_array($key, $readonlyAttributes);
                if (!$aRegisterAttributes[$key]['disabled'] && $usertoken) {
                    $aRegisterAttributes[$key]['disabled'] = in_array($key, $readonlyAttributesWhenUpdate);
                }
                /* Checkbox */
                if (in_array($key, $booleanAttributes)) {
                    $aRegisterAttributes[$key]['type'] = "checkbox";
                }
                /* dropdown */
                if (in_array($key, $dropdownTokenSingleAttributes) || in_array($key, $dropdownAttributes)) {
                    /* Find question */
                    $code = $aRegisterAttribute['description'];
                    if (intval(App()->getConfig('versionnumber')) <= 3) {
                        $oQuestion = \Question::model()->find(
                            "sid = :sid AND title = :title AND language = :language and parent_qid = 0",
                            array(":sid" => $this->surveyId, ":title" => $code, ":language" => App()->getLanguage())
                        );
                    } else {
                        $oQuestion = \Question::model()->find(
                            "sid = :sid AND title = :title and parent_qid = 0",
                            array(":sid" => $this->surveyId, ":title" => $code)
                        );
                    }
                    if ($oQuestion) {
                        if (intval(App()->getConfig('versionnumber')) <= 3) {
                            $answers = \getQuestionInformation\helpers\surveyAnswers::getAnswers($oQuestion);
                        } else {
                            $answers = \getQuestionInformation\helpers\surveyAnswers::getAnswers($oQuestion, App()->getLanguage());
                        }
                        if (!empty($answers)) {
                            $answersOption = null;
                            if (in_array($key, $dropdownTokenSingleAttributes)) {
                                $answersOption = array();
                                $criteria = new CDbCriteria();
                                $criteria->select = $key;
                                $oTokenManaged = \TokenManaged::model($this->surveyId)->scopeByGroupOf($this->token, $usertoken);
                                $oTokens = $oTokenManaged->findAll($criteria);
                                $currentCode = null;
                                if ($usertoken) {
                                    $oToken = \TokenManaged::model($this->surveyId)->findByToken($usertoken);
                                    if (!empty($oToken) && !empty($oToken->getAttribute($key))) {
                                         $currentCode = $oToken->getAttribute($key);
                                    }
                                }
                                $aExisting = array_filter(
                                    CHtml::listData(
                                        $oTokens,
                                        $key,
                                        $key
                                    )
                                );
                                foreach ($answers as $code => $answer) {
                                    $answersOption[$code] = [
                                        'label' => $answer
                                    ];
                                    if (in_array($code, $aExisting) && $code != $currentCode) {
                                        $answersOption[$code]['disabled'] = true;
                                    }
                                }
                            }
                            $aRegisterAttributes[$key]['type'] = "dropdown";
                            $aRegisterAttributes[$key]['listData'] = $answers;
                            $aRegisterAttributes[$key]['listOptions'] = $answersOption;
                        }
                    }
                }
            }
        }
        return $aRegisterAttributes;
    }

    /**
     * Set view controller
     */
    private function setViewSettings()
    {
        $surveyId = $this->surveyId;
        $this->setFinalSurveyId();
        Yii::import('TokenUsersListAndManagePlugin.models.TokenManaged');
        $this->aTokenUsersListInfo['originalSurveyId'] = $this->originalSurveyId;
        $oTemplate = Template::model()->getInstance(Utilities::getSetting($surveyId, 'template'), $surveyId);
        $this->checkAccess($surveyId);
        $this->aTokenUsersListInfo['parentSurveyId'] = $surveyId;
        $this->aSurveyInfo = getSurveyInfo($surveyId, App()->getLanguage());
        $token = $this->token;
        if ($token) {
            $this->checkToken();
        }
        $this->isAdmin = $this->aTokenUsersListInfo['isAdmin'] = Permission::model()->hasSurveyPermission($surveyId, 'tokens', 'create')
            && Permission::model()->hasSurveyPermission($surveyId, 'tokens', 'update');
        if (empty($this->sid)) {
            $this->srid = App()->getRequest()->getParam("srid");
        }
    }

    /*
     * Send the email
     * @param integer $surveyId
     * @param \Token $oManagerToken
     * @param \Token $oUserToken
     * @param string $sMessage
     * @param string $emailType
     * @param integer $srid
     * @return boolean
     */
    private function sendMail($oUserToken, $oManagerToken, $sMessage = "", $emailType = 'register', $srid = null)
    {
        $surveyId = $this->originalSurveyId;
        if (Survey::model()->findByPk($surveyId)->getIsHtmlEmail()) {
            $message = nl2br(\CHtml::encode($sMessage));
        }
        if ($oManagerToken && $oManagerToken->email) {
            App()->setConfig('TokenUsersmanagerEmailReponseTo', $oManagerToken->email);
            App()->setConfig('TokenUsersmanagerNameReponseTo', $oManagerToken->firstname . " " . $oManagerToken->lastname);
        }
        return Utilities::sendMail($surveyId, $oUserToken, $oManagerToken, $sMessage, $emailType, $srid);
    }

    /**
     * Update all related token in childs
     * $param integer $surveyId current survey id
     * $param string $usertoken the token to udate
     * $param integer[] $surveysId already done
     * @return void
     */
    private function updateTokenInExtrasurveys($surveyId, $usertoken)
    {
        /* Find all extrat survey in managementByParentManual */
        $allHaveThisParentManual = Utilities::getSurveyIdWithValue('managementByParentManual', $surveyId);
        $oUserToken = \Token::model($surveyId)->findByToken($usertoken);
        $aUserToken = $oUserToken->getAttributes();
        $aUserToken = array_diff_key($aUserToken, array_flip(array('tid',  'sent', 'usesleft', 'completed', 'remindersent', 'remindercount', 'mpid')));

        foreach($allHaveThisParentManual as $manualSurveyId) {
            if (in_array($manualSurveyId, $this->childsSurveyIdDone)) {
                continue;
            }
            $this->childsSurveyIdDone[] = $manualSurveyId;
            $oManualSurvey = Survey::model()->findByPk($manualSurveyId);
            if (!$oManualSurvey->getHasTokensTable()) {
                continue;
            }
            $aAvailableAttributes = \Token::model($manualSurveyId)->getAttributes();
            $oTokenChildUser = \Token::model($manualSurveyId)->findByToken($usertoken);
            if (empty($oTokenChildUser)) {
                $oTokenChildUser = \Token::create($manualSurveyId);
                $oTokenChildUser->token = $usertoken;
                $oTokenChildUser->save();
            }
            $aNewAttributes = array_intersect_key($aUserToken, $aAvailableAttributes);
            /* Use updateAll : disable rules or anything else */
            \Token::model($manualSurveyId)->updateAll(
                $aNewAttributes,
                "token = :token",
                array('token' => $usertoken)
            );
            /* Do it in childs */
            $this->updateTokenInChildsurveys($manualSurveyId, $aUserToken);
        }
        $this->updateTokenInChildsurveys($surveyId, $aUserToken);
    }

    /**
     * Update all related token in childs
     * $param integer $surveyId current survey id
     * $param string[] $aUserToken the user token attribute
     * $param integer[] $surveysId already done
     * @return void
     */
    private function updateTokenInChildsurveys($surveyId, $aUserToken)
    {
        /* 1st get childs survey */
        if (empty(\RelatedSurveyManagement\Utilities::API) || version_compare(\RelatedSurveyManagement\Utilities::API, "0.4", "<")) {
            return;
        }
        $usertoken = $aUserToken['token'];
        $oChildSurveys = new \RelatedSurveyManagement\ChildrenSurveys($surveyId);
        $aChildrensSurveys = $oChildSurveys->getChildrensSurveys();
        $aChildrensSurveys = array_diff($aChildrensSurveys, $this->childsSurveyIdDone);
        foreach ($aChildrensSurveys as $childSurveyId) {
            if (in_array($childSurveyId, $this->childsSurveyIdDone)) {
                continue;
            }
            $this->childsSurveyIdDone[] = $childSurveyId;
            /* check if get parent */
            if (!Utilities::getSetting($childSurveyId, 'managementByParent')) {
                continue;
            }
            $oChildSurvey = Survey::model()->findByPk($childSurveyId);
            if (!$oChildSurvey->getHasTokensTable()) {
                continue;
            }
            $aAvailableAttributes = \Token::model($childSurveyId)->getAttributes();
            $oTokenChildUser = \Token::model($childSurveyId)->findByToken($usertoken);
            if (empty($oTokenChildUser)) {
                $oTokenChildUser = \Token::create($childSurveyId);
                $oTokenChildUser->token = $usertoken;
                $oTokenChildUser->save();
            }
            $aNewAttributes = array_intersect_key($aUserToken, $aAvailableAttributes);
            /* Use updateAll : disable rules or anything else */
            \Token::model($childSurveyId)->updateAll(
                $aNewAttributes,
                "token = :token",
                array('token' => $usertoken)
            );
            /* Loop */
            $this->updateTokenInChildsurveys($childSurveyId, $aUserToken);
        }
    }

    /**
     * Set final surveyId if needed
     */
    private function setFinalSurveyId()
    {
        /* check if get parent */
        if (!Utilities::getSetting($this->surveyId, 'managementByParent')) {
            return;
        }
        if (Utilities::getSetting($this->surveyId, 'managementByParent') == 'manual' && $manualParentId = Utilities::getSetting($this->surveyId, 'managementByParentManual')) {
            if (tableExists("{{tokens_" . $manualParentId . "}}")) {
                $parentSurvey = $manualParentId;
            }
        }
        if (empty($parentSurvey)) {
            if (!Yii::getPathOfAlias('RelatedSurveyManagement') || version_compare(\RelatedSurveyManagement\Utilities::API, "0.4", "<")) {
                return;
            }

            $oParentSurvey = new \RelatedSurveyManagement\ParentSurveys($this->surveyId);
            $parentSurvey = $oParentSurvey->getFinalAncestrySurvey();
        }
        if (empty($parentSurvey)) {
            return;
        }
        $this->surveyId = $parentSurvey;
        $settings = \TokenUsersListAndManagePlugin\Settings::getInstance();
        $settings->updateSettingsVersion($parentSurvey);
    }

    /**
     * Check if user is allowed to manage
     * @param integer $surveyId
     * @param string $token
     * @return void
     */
    private function checkManagerToken()
    {
        $surveyId = $this->surveyId;
        if (empty($this->token)) {
            if ($this->isAdmin) {
                return;
            }
            $this->throwAdaptedException(401, self::translate("No right to manage user."));
        }
        $userManagementAccess = Utilities::getSetting($surveyId, 'userManagementAccess');
        if ($userManagementAccess == 'none') {
            $this->throwAdaptedException(403, self::translate("No right to manage user."));
        }
        if ($userManagementAccess == 'limesurvey' && !$this->isAdmin) {
            $this->throwAdaptedException(403, self::translate("No right to manage user."));
        }
        if ($userManagementAccess == 'admin') {
            $tokenAttributeGroupManager = \TokenUsersListAndManagePlugin\Utilities::getTokenAttributeGroupManager($surveyId);
            $oUserToken = TokenManaged::model($surveyId)->findByToken($this->token);
            if (empty($oUserToken->$tokenAttributeGroupManager)) {
                $this->throwAdaptedException(403, self::translate("No right to manage user."));
            }
        }
        // $userManagementAccess == 'user'
        $tokenAttributeRestricted = \TokenUsersListAndManagePlugin\Utilities::getSetting($surveyId, 'tokenAttributeRestricted');
        $oUserToken = TokenManaged::model($surveyId)->findByToken($this->token);
        if ($tokenAttributeRestricted && !empty($oUserToken->$tokenAttributeRestricted)) {
            $this->throwAdaptedException(403, self::translate("No right to manage user."));
        }
    }

    /**
     * render twig part using current information
     */
    private function renderTwig()
    {
        $aSurveyInfo = array_merge(
            $this->aSurveyInfo,
            array(
                'showprogress' => 'N',
            )
        );
        $this->language['Management of participant of %s'] = self::translate("Management of participant of %s");
        $renderDataTwig = array_merge(
            $this->aTokenUsersListInfo,
            array(
                'aSurveyInfo' => $aSurveyInfo,
                'language' => $this->language,
            )
        );
        if ($this->allowAjax && App()->getRequest()->IsAjaxRequest) {
            $renderFile = "subviews/content/" . $aSurveyInfo['include_content'] . ".twig";
            $renderDataTwig['ajax'] = true;
            echo App()->twigRenderer->renderPartial(
                $renderFile,
                $renderDataTwig
            );
            App()->end();
        }
        $this->createAndRegisterPackage();
        App()->clientScript->registerScriptFile(App()->getConfig("generalscripts") . 'nojs.js', CClientScript::POS_HEAD);
        App()->twigRenderer->renderTemplateFromFile(
            'layout_global.twig',
            $renderDataTwig,
            false
        );
        App()->end();
    }

    /**
     * Check access to this specific survey with token
     * This validate only if token exist in this survey
     * @param integer $surveyId
     * @param string|null $token
     * @throws Exception : 404,401, 403
     */
    private function checkAccess($surveyId)
    {
        if (empty($surveyId)) {
            $this->throwAdaptedException(400, self::translate("Invalid survey id"));
        }
        if (strval(intval($surveyId)) !== strval($surveyId)) {
            throw new CHttpException(403, gT("Invalid survey id"));
        }
        $oSurvey = Survey::model()->findByPk($surveyId);
        if (empty($oSurvey)) {
            $this->throwAdaptedException(404, self::translate("Invalid survey id"));
        }
        if (!$oSurvey->getHasTokensTable()) {
            if (Permission::model()->hasSurveyPermission($surveyId, 'surveysettings', 'update') && Permission::model()->hasSurveyPermission($surveyId, 'tokens', 'create')) {
                App()->getController()->redirect(array("admin/tokens/sa/index", "surveyid" => $surveyId));
            }
            throw new CHttpException(400, self::translate("Invalid survey id"));
        }
        $active = Utilities::getSetting($surveyId, 'active');
        if (empty($active)) {
            $this->throwAdaptedException(403, self::translate("Invalid survey id"));
        }
    }

    /**
     * Throw Exception
     * Simple text if it's Ajax
     * @param integer $code
     * @param string $text
     * @throw Exception
     */
    private function throwAdaptedException($code, $message = "")
    {
        if (App()->getRequest()->getIsAjaxRequest()) {
            /* see CErrorHandler::getHttpHeader */
            $textFromCode = array(
                400 => 'Bad Request',
                401 => 'Unauthorized',
                402 => 'Payment Required',
                403 => 'Forbidden',
                404 => 'Not Found',
                405 => 'Method Not Allowed',
                406 => 'Not Acceptable',
                407 => 'Proxy Authentication Required',
                408 => 'Request Timeout',
                409 => 'Conflict',
                410 => 'Gone',
                411 => 'Length Required',
                412 => 'Precondition Failed',
                413 => 'Request Entity Too Large',
                414 => 'Request-URI Too Long',
                415 => 'Unsupported Media Type',
                416 => 'Requested Range Not Satisfiable',
                417 => 'Expectation Failed',
                418 => 'I’m a teapot',
                422 => 'Unprocessable entity',
                423 => 'Locked',
                424 => 'Method failure',
                425 => 'Unordered Collection',
                426 => 'Upgrade Required',
                428 => 'Precondition Required',
                429 => 'Too Many Requests',
                431 => 'Request Header Fields Too Large',
                449 => 'Retry With',
                450 => 'Blocked by Windows Parental Controls',
                451 => 'Unavailable For Legal Reasons',
                500 => 'Internal Server Error',
                501 => 'Not Implemented',
                502 => 'Bad Gateway',
                503 => 'Service Unavailable',
                504 => 'Gateway Timeout',
                505 => 'HTTP Version Not Supported',
                507 => 'Insufficient Storage',
                509 => 'Bandwidth Limit Exceeded',
                510 => 'Not Extended',
                511 => 'Network Authentication Required',
            );
            if (isset($textFromCode[$code])) {
                $httpVersion = Yii::app()->request->getHttpVersion();
                header("HTTP/$httpVersion {$code} {$textFromCode[$code]}", true, $code);
                echo "HTTP Error - $code {$textFromCode[$code]} - {$message}";
                App()->end();
            }
        }
        throw new CHttpException($code, $message);
    }

    /**
     * return parent survey (final one used for token)
     * @param $surveyId
     * @return int|null
     */
    public function getAncestrySurvey($surveyId)
    {
        $parentSurvey = false;
        if (!Yii::getPathOfAlias('RelatedSurveyManagement')) {
            return;
        }
        if (empty(\RelatedSurveyManagement\Utilities::API) || version_compare(\RelatedSurveyManagement\Utilities::API, "0.4", "<")) {
            return;
        }
        if (!Utilities::getSetting($surveyId, 'managementByParent')) {
            return;
        }
        if (version_compare(\RelatedSurveyManagement\Utilities::API, "0.8", ">=")) {
            $oParentSurvey = \RelatedSurveyManagement\ParentSurveys::getInstance($surveyId);
        } else {
            $oParentSurvey = new \RelatedSurveyManagement\ParentSurveys($surveyId);
        }
        $parentSurvey = $oParentSurvey->getFinalAncestrySurvey();
        if (empty($parentSurvey)) {
            return;
        }
        return $parentSurvey;
    }

    /**
     * Check current token and show login page if needed
     * @param integer $surveyId
     * @param string|null $token
     * @throws Exception : 404,401, 403
     */
    private function checkToken()
    {
        if (empty($this->token)) {
            if ($this->isAdmin) {
                return;
            }
            $this->aSurveyInfo['include_content'] = 'TULAM_login';
            $this->rendertwig();
        }
        /* Check if token is valid */
        $oToken = \TokenManaged::model($this->surveyId)->findByToken($this->token);
        $aErrors = array();
        if (empty($oToken)) {
            $aErrors[] = self::translate("The token you have provided is not valid.");
        } else {
            $now = dateShift(date("Y-m-d H:i:s"), "Y-m-d H:i:s", Yii::app()->getConfig("timeadjust"));
            if ($oToken->validfrom && strtotime($now) < strtotime($oToken->validfrom)) {
                $aErrors[] = self::translate("This invitation is not valid yet.");
            } elseif ($oToken->validuntil && strtotime($now) > strtotime($oToken->validuntil)) {
                $aErrors[] = self::translate("This invitation is not valid anymore.");
            }
        }

        if (!empty($aErrors)) {
            $this->aSurveyInfo['aForm'] = array(
                'aEnterErrors' => $aErrors,
            );
            $this->aSurveyInfo['include_content'] = 'TULAM_login';
            $this->rendertwig();
        }
        $tokenAttributeGroupManager = \TokenUsersListAndManagePlugin\Utilities::getTokenAttributeGroupManager($this->surveyId);
        $tokenAttributeGroup = \TokenUsersListAndManagePlugin\Utilities::getTokenAttributeGroup($this->surveyId);
        $this->aTokenUsersListInfo['managerInformation'] = $oToken->getAttributes();
        if ($tokenAttributeGroup) {
            $this->aTokenUsersListInfo['managerInformation']['tokenGroup'] = !empty($oToken->$tokenAttributeGroup) ? $oToken->getAttribute($tokenAttributeGroup) : null;
        }
        if ($tokenAttributeGroupManager) {
            $this->aTokenUsersListInfo['managerInformation']['isManager'] = !empty($oToken->$tokenAttributeGroupManager) ? $oToken->getAttribute($tokenAttributeGroupManager) : null;
        }
    }

    /**
     * Check the javascript package
     * @return boolean
     */
    public function createAndRegisterPackage()
    {
        if (!App()->clientScript->hasPackage('TokenUsersListAndManage')) {
            App()->clientScript->addPackage('TokenUsersListAndManage', array(
                'basePath'    => 'TokenUsersListAndManagePlugin.assets.tokenuserslistandmanage',
                'css'         => array(
                    'TokenUsersListAndManage.css'
                ),
                'js'          => array(
                    'TokenUsersListAndManageClass.js'
                ),
                'depends' => array(
                    'jquery',
                    'bootstrap',
                    'bootstrap-select2'
                )
            ));
        }
        App()->getClientScript()->registerPackage('TokenUsersListAndManage');
        App()->getClientScript()->registerScript('TokenUsersListAndManageInit', 'TokenUsersListAndManage.init()', \CClientScript::POS_END);
        $TULAMRegisterPackage = new \PluginEvent('TULAMRegisterPackage');
        App()->getPluginManager()->dispatchEvent($TULAMRegisterPackage);
    }

    /**
     * get translation
     * @param string
     * @return string
     */
    private static function translate($string)
    {
        return Yii::t('', $string, array(), 'TokenUsersListAndManageMessages');
    }
}
