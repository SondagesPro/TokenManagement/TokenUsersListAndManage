<?php

/**
 * Token Users List And Manage
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2020-2025 Denis Chenu <http://www.sondages.pro>
 * @license AGPL v3
 * @version 0.36.2
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

class TokenUsersListAndManage extends PluginBase
{
    protected $storage = 'DbStorage';
    protected static $description = 'Allow to create groups of token users, manager of this group and restriction to specific surveys. To be used with responseListAndManage and reloadAnyReponse';
    protected static $name = 'TokenUsersListAndManage';

    /* @var integer|null surveyId get track of current surveyId between action */
    private $surveyId;

    /* @var integer|null surveyId get track of original surveyId if not same than parent */
    private $surveyOriginalId;

    /* @var boolean $allowAjax for direct request : allow to show as ajax */
    private $allowAjax = true;

    /* @var array used for rendering*/
    private $aSurveyInfo = array();

    /* @var array used for rendering*/
    private $aTokenUsersListInfo = array();

    /* @var array of lanuage string used for rendering*/
    public $language = array();

    /* @var boolean $modalDone  keep track if modal is done in beforeQuestionRender */
    private $modalDone = false;

    /* @inheritdoc */
    public $allowedPublicMethods = [
        'actionSettings'
    ];

    /**
     * @var array[] the settings
     */
    protected $settings = array(
        'active' => array(
            'type' => 'checkbox',
            'htmlOptions' => array(
                'value' => 1,
                'uncheckValue' => 0,
            ),
            'label' => "Activate by default.",
            'default' => 0,
        ),
        'managementByParent' => array(
            'type' => 'checkbox',
            'htmlOptions' => array(
                'value' => 1,
                'uncheckValue' => 0,
            ),
            'label' => "Use the parent for the manager, and track token changes.",
            'default' => 0,
        ),
        'useAttributeRestricted' => array(
            'type' => 'checkbox',
            'htmlOptions' => array(
                'value' => 1,
                'uncheckValue' => 0,
            ),
            'label' => "Use restricted attribute.",
            'default' => 1,
        ),
        'allowAllAttributeRestricted' => array(
            'type' => 'checkbox',
            'htmlOptions' => array(
                'value' => 1,
                'uncheckValue' => 0,
            ),
            'label' => "Allow all on attribute.",
            'default' => 1,
        ),
        'listSurveyRestrictedByParent' => array(
            'type' => 'checkbox',
            'htmlOptions' => array(
                'value' => 1,
                'uncheckValue' => 0,
            ),
            'label' => "Force usage of parent survey in user management for managing restricted survey.",
            'default' => 0,
        ),
    );

    /* @var integer $DBVersion currently setup */
    const DBVERSION = 1;

    public function init()
    {
        Yii::setPathOfAlias('TokenUsersListAndManagePlugin', dirname(__FILE__));
        App()->setConfig('TokenUsersListAndManageAPI', \TokenUsersListAndManagePlugin\Utilities::API);

        /* Primary system: show the list of token and allow managing */
        $this->subscribe('newDirectRequest');

        /* Really disable account */
        $this->subscribe('beforeSurveyPage');

        /* DB and others */
        $this->subscribe('beforeActivate');

        /* The tool question */
        $this->subscribe('newQuestionAttributes');
        $this->subscribe('beforeQuestionRender');

        /* In tool */
        $this->subscribe('beforeToolsMenuRender');
    }

    /** @inheritdoc **/
    public function getPluginSettings($getValues = true)
    {
        if (!Permission::model()->hasGlobalPermission('settings', 'read')) {
            throw new CHttpException(403);
        }
        if (Yii::app() instanceof CConsoleApplication) {
            return;
        }
        if (!$this->getIsUsable()) {
            $warningMessages = array();
            if (!version_compare(Yii::app()->getConfig('versionnumber'), "3.10", ">=")) {
                $warningMessages[] = sprintf($this->translate("You need LimeSurvey version %s and up."), '3.10');
            }
            $this->settings = array(
                'unable' => array(
                    'type' => 'info',
                    'content' => CHtml::tag(
                        "ul",
                        array('class' => 'alert alert-warning'),
                        "<li>" . implode("</li><li>", $warningMessages) . "</li>"
                    ),
                ),
            );
            return $this->settings;
        }
        $pluginSettings = parent::getPluginSettings($getValues);
        $accesUrl = Yii::app()->createUrl("plugins/direct", array('plugin' => get_class(), 'sid' => '{SID}', 'token' => '{TOKEN}'));
        $accesHtmlUrl = "<code>" . urldecode($accesUrl) . "</code>";
        $pluginSettings['information']['content'] = sprintf($this->translate("Access link for token management (in survey) listing : %s."), $accesHtmlUrl);
        $oSurveymenuTokenUsersListAndManage = SurveymenuEntries::model()->find("name = :name", array(":name" => 'TokenUsersListAndManage'));
        $state = !empty($oSurveymenuTokenUsersListAndManage);
        $help = $state ? $this->translate('Menu exist, to delete : uncheck box and validate.') : $this->translate("Menu didn‘t exist, to create check box and validate.");
        $pluginSettings['createTokenMenu'] = array(
            'type' => 'checkbox',
            'label' => $this->translate('Add a menu to token management in surveys.'),
            'default' => false,
            'help' => $help,
            'current' => $state,
        );
        $oSurveymenuManage = SurveymenuEntries::model()->find("name = :name", array(":name" => 'TokenUsersListAndManageSetting'));
        $state = !empty($oSurveymenuManage);
        $help = $state ? $this->translate('Menu exist, to delete : uncheck box and validate.') : $this->translate("Settings menu didn‘t exist, to create check box and validate.");
        $pluginSettings['createSettingMenu'] = array(
            'type' => 'checkbox',
            'label' => $this->translate('Add a menu to manage token user list.'),
            'default' => false,
            'help' => $help,
            'current' => $state,
        );
        if ($state) {
            $pluginSettings['ShowToolMenu'] = array(
                'type' => 'checkbox',
                'label' => $this->translate('Add a link in tool menu for settings.'),
                'default' => false,
                'current' => $this->get('ShowToolMenu'),
            );
        }
        return $pluginSettings;
    }

    /**
     * see beforeToolsMenuRender event
     *
     * @return void
     */
    public function beforeToolsMenuRender()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $event = $this->getEvent();
        $surveyId = $event->get('surveyId');
        $oSurvey = Survey::model()->findByPk($surveyId);
        $iSurveyMenuEntries = SurveymenuEntries::model()->count("name = :name", array(":name" => 'TokenUsersListAndManageSetting'));
        if ($iSurveyMenuEntries && !$this->get('ShowToolMenu')) {
            return;
        }
        if (Permission::model()->hasSurveyPermission($surveyId, 'surveysettings', 'update')) {
            $aMenuItem = array(
                'label' => $this->translate('Token users list settings'),
                'iconClass' => 'fa fa-address-card',
                'href' => Yii::app()->createUrl(
                    'admin/pluginhelper',
                    array(
                        'sa' => 'sidebody',
                        'plugin' => get_class($this),
                        'method' => 'actionSettings',
                        'surveyId' => $surveyId
                    )
                ),
            );
            $menuItem = new \LimeSurvey\Menu\MenuItem($aMenuItem);
            $event->append('menuItems', array($menuItem));
        }
    }

    /**
     * Add the 'management tool attribute'
    **/
    public function newQuestionAttributes()
    {
        $buttonsAttributes = array(
            'TulamUserManagement' => array(
                'name'      => 'TulamUserManagement',
                'types'     => 'X', /*Only text display */
                'category'  => $this->translate('User management'),
                'sortorder' => 100,
                'inputtype' => 'switch',
                'options'   => array(
                    0 => gT('No'),
                    1 => gT('Yes'),
                ),
                'caption'   => $this->translate('Use as token user management question'),
                'default'   => '0',
            ),
            'TulamRelatedSurveyId' => array(
                'name'      => 'TulamRelatedSurveyId',
                'types'     => 'X', /*Only text display */
                'category'  => $this->translate('User management'),
                'sortorder' => 105,
                'inputtype' => 'integer',
                'caption'   => $this->translate('Related survey id to use for management'),
                'help'   => $this->translate('Warning : all link and test goes to the new survey, some automatic link in manager go to the new surveyId. Token used is the current one, must exist in related survey.'),
                'default'   => '',
            ),
            'TulamLinkToManagment' => array(
                'name'      => 'TulamLinkToManagment',
                'types'     => 'X',
                'category'  => $this->translate('User management'),
                'sortorder' => 200,
                'inputtype' => 'switch',
                'options'   => array(
                    0 => gT('No'),
                    1 => gT('Yes'),
                ),
                'caption'   => $this->translate('Show link to management'),
                'help'   => $this->translate('Show according to right of user, this open a new windows and can use parent survey setting.'),
                'default'   => '0',
            ),
            'TulamFrameToManagment' => array(
                'name'      => 'TulamFameToManagment',
                'types'     => 'X',
                'category'  => $this->translate('User management'),
                'sortorder' => 210,
                'inputtype' => 'switch',
                'options'   => array(
                    0 => gT('No'),
                    1 => gT('Yes'),
                ),
                'caption'   => $this->translate('Show management in iframe'),
                'help'   => $this->translate('Show according to right of user, same than open in new windows, but allow to show it directly. The header is hidden by default.'),
                'default'   => '0',
            ),
            'TulamMyAccount' => array(
                'name'      => 'TulamMyAccount',
                'types'     => 'X',
                'category'  => $this->translate('User management'),
                'sortorder' => 210,
                'inputtype' => 'switch',
                'options'   => array(
                    0 => gT('No'),
                    1 => gT('Yes'),
                ),
                'caption'   => $this->translate('Show button to update own account'),
                'default'   => '1',
            ),
            'TulamSendMessage' => array(
                'name'      => 'TulamSendMessage',
                'types'     => 'X',
                'category'  => $this->translate('User management'),
                'sortorder' => 220,
                'inputtype' => 'switch',
                'options'   => array(
                    0 => gT('No'),
                    1 => gT('Yes'),
                ),
                'caption'   => $this->translate('Show button to send message'),
                'help'   => $this->translate('This allow to send message to user. Open a dialog box for sending message.'),
                'default'   => '1',
            ),
            'TulamAssignUser' => array(
                'name'      => 'TulamAssignUser',
                'types'     => 'X',
                'category'  => $this->translate('User management'),
                'sortorder' => 230,
                'inputtype' => 'switch',
                'options'   => array(
                    0 => gT('No'),
                    1 => gT('Yes'),
                ),
                'caption'   => $this->translate('Show button to assign user'),
                'help'   => $this->translate('This allow to send message to user and give right to restricted user. Open a dialog box for sending message.'),
                'default'   => '1',
            ),
            'TulamCreateUser' => array(
                'name'      => 'TulamCreateUser',
                'types'     => 'X',
                'category'  => $this->translate('User management'),
                'sortorder' => 240,
                'inputtype' => 'switch',
                'options'   => array(
                    0 => gT('No'),
                    1 => gT('Yes'),
                ),
                'caption'   => $this->translate('Show button to create user'),
                'help'   => $this->translate('This allow to create new user and give right to restricted user. Open a dialog box for sending message.'),
                'default'   => '0',
            ),
            'TulamContactUser' => array(
                'name'      => 'TulamContactUser',
                'types'     => 'X',
                'category'  => $this->translate('User management'),
                'sortorder' => 240,
                'inputtype' => 'switch',
                'options'   => array(
                    0 => gT('No'),
                    1 => gT('Yes'),
                ),
                'caption'   => $this->translate('Show contact button'),
                'help'   => $this->translate('Same than send message button with button to create or assign user.'),
                'default'   => '0',
            ),
            'TulamListUser' => array(
                'name'      => 'TulamListUser',
                'types'     => 'X',
                'category'  => $this->translate('User management'),
                'sortorder' => 300,
                'inputtype' => 'singleselect',
                'options'   => array(
                    0 => $this->translate('None'),
                    'group' => $this->translate('Users in same group'),
                    'manager' => $this->translate('Managers of group'),
                ),
                'caption'   => $this->translate('Show user group list'),
                'help'   => $this->translate('Display the current participant\'s on same group in a list with an e-mail link.'),
                'default'   => '0',
            ),
            'TulamListUserOrder' => array(
                'name'      => 'TulamListUserOrder',
                'types'     => 'X',
                'category'  => $this->translate('User management'),
                'sortorder' => 310,
                'inputtype' => 'text',
                'caption'   => $this->translate('Order for user listing'),
                'help'   => $this->translate('Set the order for user listing using a column, default is lastname ASC, firstname ASC.'),
                'default'   => '',
            ),
        );
        $this->getEvent()->append('questionAttributes', $buttonsAttributes);
    }

    /**
     * Really disable token->emailstatus === 'disable'
     */
    public function beforeSurveyPage()
    {
        $surveyId = $this->getEvent()->get('surveyId');
        if (!$this->getSetting($surveyId, 'active')) {
            return;
        }
        $oSurvey = Survey::model()->findByPk($surveyId);
        if (empty($oSurvey)) {
            return;
        }
        if (!$oSurvey->getHasTokensTable()) {
            return;
        }
        $token = App()->getRequest()->getParam('token');
        $oToken = Token::model($surveyId)->findByToken($token);
        if (empty($oToken)) {
            return;
        }
        if ($oToken->emailstatus != 'disable') {
            return;
        }
        /* This is an inbvalid token */
        $event = new PluginEvent('onSurveyDenied');
        $event->set('surveyId', $surveyId);
        $event->set('reason', 'invalidToken');
        App()->getPluginManager()->dispatchEvent($event);
        $aMessage = array(
            gT("We are sorry but you are not allowed to enter this survey."),
        );
        $sError = gT("This invitation is not valid anymore.");
        App()->getController()->renderExitMessage(
            $surveyId,
            'survey-notstart',
            $aMessage,
            null,
            array($sError)
        );
        App()->end();
    }

    /**
     * Question attribute to add button
     */
    public function beforeQuestionRender()
    {
        if ($this->getEvent()->get('type') != "X") {
            return;
        }
        $beforeQuestionRenderEvent = $this->getEvent();
        $aAttributes = QuestionAttribute::model()->getQuestionAttributes($beforeQuestionRenderEvent->get('qid'));
        if (empty($aAttributes['TulamUserManagement'])) {
            return;
        }
        $qid = $beforeQuestionRenderEvent->get('qid');
        $currentSurveyId = $surveyId = $settingsSurveyId = $beforeQuestionRenderEvent->get('surveyId');
        $userToken = \reloadAnyResponse\Utilities::getCurrentToken($surveyId);
        if (empty($userToken)) {
            return;
        }
        if (!empty($aAttributes['TulamRelatedSurveyId'])) {
            $settingsSurveyId = $surveyId = $aAttributes['TulamRelatedSurveyId'];
        }
        if (!$this->getSetting($surveyId, 'active')) {
            return;
        }
        $oSurvey = Survey::model()->findByPk($surveyId);
        if (!$oSurvey || !$oSurvey->getHasTokensTable()) {
            return;
        }
        $TokenUsersListAndManagePlugin = new \TokenUsersListAndManagePlugin\TokenUsersManager($surveyId);
        $parentSurvey = $TokenUsersListAndManagePlugin->getAncestrySurvey($surveyId);
        if ($parentSurvey) {
            $settingsSurveyId = $parentSurvey;
        }
        if (!$this->getSetting($settingsSurveyId, 'active')) {
            return;
        }
        $userToken = \reloadAnyResponse\Utilities::getCurrentToken($surveyId);
        if (empty($userToken)) {
            return;
        }
        Yii::import('TokenUsersListAndManagePlugin.models.TokenManaged');
        /* Set the current token to this survey and parent */
        $this->getCurrentToken($surveyId, $userToken);
        $this->getCurrentToken($settingsSurveyId, $userToken);
        $tokenAttributeGroup = \TokenUsersListAndManagePlugin\Utilities::getTokenAttributeGroup($settingsSurveyId);
        if (empty($tokenAttributeGroup)) {
            /* Or allow it ? */
            return;
        }

        $oUserToken = Token::model($surveyId)->find('token = :token', array( ":token" => $userToken));
        if (empty($oUserToken)) {
            return;
        }
        $this->createSurveyPackage();
        Yii::import('TokenUsersListAndManagePlugin.models.TokenManaged');
        $TokenUsersListAndManagePlugin->createAndRegisterPackage();
        $buttonHtml = $this->ownGetButtonHtml(
            $qid,
            $aAttributes,
            $currentSurveyId,
            $surveyId,
            $settingsSurveyId,
            $oUserToken,
        );
        /* Add managerList */
        $userListHtml = $this->ownGetUserListHtml(
            $qid,
            $aAttributes,
            $currentSurveyId,
            $surveyId,
            $settingsSurveyId,
            $oUserToken,
        );
        $question = $beforeQuestionRenderEvent->get('text');
        $question .= $buttonHtml;
        $question .= $userListHtml;
        $beforeQuestionRenderEvent->set('text', $question);
        $this->modalDone = true;
    }

    /**
     * Return the HTML to be used in question rendering for buttons
     * @param $qid question id
     * @param array $aAttributes of the question
     * @param integer $currentSurveyId, the surveyId of the current page
     * @param integer $surveyId, the surveyId to be used for buttons link and token data
     * @param integer $settingsSurveyId the settings surveyId
     * @param \Token $oUserToken the current token
     * @return string HTML
     */
    private function ownGetButtonHtml($qid, $aAttributes, $currentSurveyId, $surveyId, $settingsSurveyId, $oUserToken)
    {
        $tokenAttributeGroupManager = \TokenUsersListAndManagePlugin\Utilities::getTokenAttributeGroupManager($settingsSurveyId);
        $tokenAttributeRestricted = \TokenUsersListAndManagePlugin\Utilities::getSetting($settingsSurveyId, 'tokenAttributeRestricted');
        $useAttributeRestricted = \TokenUsersListAndManagePlugin\Utilities::getSetting($surveyId, 'useAttributeRestricted');
        $allowAllAttributeRestricted = \TokenUsersListAndManagePlugin\Utilities::getSetting($surveyId, 'allowAllAttributeRestricted');
        $userManagementAccess = \TokenUsersListAndManagePlugin\Utilities::getSetting($settingsSurveyId, 'userManagementAccess');
        $allowGiveRight = \TokenUsersListAndManagePlugin\Utilities::getSetting($settingsSurveyId, 'allowGiveRight');
        $allowSendEmail = \TokenUsersListAndManagePlugin\Utilities::getSetting($settingsSurveyId, 'allowSendEmail');
        $allowEditAccount = \TokenUsersListAndManagePlugin\Utilities::getSetting($settingsSurveyId, 'allowEditAccount');
        $allowCreateUser = \TokenUsersListAndManagePlugin\Utilities::getSetting($settingsSurveyId, 'allowCreateUser');
        $bUserManager = !empty($oUserToken->$tokenAttributeGroupManager);
        /* restricted user : tokenAttributeRestricted and not ALL access */
        $bUserRestricted = !empty($oUserToken->$tokenAttributeRestricted);
        if ($bUserRestricted && $allowAllAttributeRestricted) {
            if (TokenUsersListAndManagePlugin\models\TokenSridAccess::haveAllAccess($surveyId, $oUserToken->token)) {
                $bUserRestricted = false;
            }
        }
        $allowedEditAccount = ($allowEditAccount == 'admin' && $bUserManager)
            || ($allowEditAccount == 'user' && ($bUserManager || !$bUserRestricted))
            || $allowEditAccount == 'all';
        $allowedManagement = ($userManagementAccess == 'admin' && $bUserManager)
            || ($userManagementAccess == 'user' && ($bUserManager || !$bUserRestricted))
            || $userManagementAccess == 'all';
        $allowedAssignUser = ($allowGiveRight == 'admin' && $bUserManager)
            || ($allowGiveRight == 'user' && ($bUserManager || !$bUserRestricted))
            || $allowGiveRight == 'all';
        $allowedSendEmail = ($allowSendEmail == 'admin' && $bUserManager)
            || ($allowSendEmail == 'user' && ($bUserManager || !$bUserRestricted))
            || $allowSendEmail == 'all';
        $allowedCreateUser = ($allowCreateUser == 'admin' && $bUserManager)
            || ($allowCreateUser == 'user' && ($bUserManager || !$bUserRestricted))
            || $allowCreateUser == 'all';
        /* Construct the data */
        $aSurveyInfo = getSurveyInfo($surveyId, App()->getLanguage());
        $aTokenInfo = array();
        $toolUrls = array(
            'survey' => null,
            'management' => null,
            'myaccount' => null,
            'createuser' => null,
            'assignuser' => null,
            'sendmessage' => null,
        );
        $toolOptions = array(
            'survey' => array(),
            'management' => array(),
            'myaccount' => array(),
            'createuser' => array(),
            'assignuser' => array(),
            'sendmessage' => array(),
        );
        $srid = reloadAnyResponse\Utilities::getCurrentSrid($surveyId);
        if ($currentSurveyId != $surveyId) {
            $srid = null;
        }
        if (!empty($aAttributes['TulamMyAccount']) && $allowedEditAccount) {
            $toolUrls['myaccount'] = App()->createUrl(
                "plugins/direct",
                array(
                    "plugin" => 'TokenUsersListAndManage',
                    "surveyid" => $surveyId,
                    "function" => "myaccount",
                )
            );
        }
        if (!empty($aAttributes['TulamLinkToManagment']) && $allowedManagement) {
            $toolUrls['management'] = App()->createUrl(
                "plugins/direct",
                array(
                    "plugin" => 'TokenUsersListAndManage',
                    "surveyid" => $surveyId,
                    "returnsurveyid" => $currentSurveyId,
                    "function" => "listparticipant",
                    'originalSrid' => $srid,
                )
            );
        }
        /* Save current user in current srid */
        if ($srid && $bUserRestricted && $useAttributeRestricted) {
            $oSridList = \TokenUsersListAndManagePlugin\models\TokenSridAccess::model()->findByPk(array(
                'sid' => $surveyId,
                'token' => $oUserToken->token,
                'srid' => $srid
            ));
            if (empty($oSridList)) {
                $oSridList = new TokenUsersListAndManagePlugin\models\TokenSridAccess();
                $oSridList->sid = $surveyId;
                $oSridList->token = $oUserToken->token;
                $oSridList->srid = $srid;
                $oSridList->save();
            }
        }
        $haveMessageToken = false;
        $haveAssignToken = false;
        if ($srid) {
            $aGroupTokens = \TokenUsersListAndManagePlugin\Utilities::getTokensList($surveyId, $oUserToken->token);
            foreach ($aGroupTokens as $selecttoken) {
                /* check validity when search (using editable) */
                $oTestToken = TokenManaged::model($surveyId)->findByToken($selecttoken);
                if (empty($oTestToken)) {
                    continue;
                }
                if (!$oTestToken->getActive()) {
                    continue;
                }
                $mailsIsValid = !empty($oTestToken->email) && $oTestToken->emailstatus == 'OK';
                if (empty($oTestToken->$tokenAttributeRestricted) || TokenUsersListAndManagePlugin\models\TokenSridAccess::haveAllAccess($surveyId, $selecttoken)) {
                    $haveMessageToken = $haveMessageToken || $mailsIsValid;
                } else {
                    $oTokenSridAccess = TokenUsersListAndManagePlugin\models\TokenSridAccess::model()->findByPk(array(
                        'sid' => $surveyId,
                        'token' => $selecttoken,
                        'srid' => $srid,
                    ));
                    if (empty($oTokenSridAccess)) {
                        $haveAssignToken =  $haveAssignToken || $mailsIsValid;
                    } else {
                        $haveMessageToken = $haveMessageToken || $mailsIsValid;
                    }
                }
            }
            if ($tokenAttributeRestricted && $useAttributeRestricted && !empty($aAttributes['TulamAssignUser']) && $allowedAssignUser) {
                $toolUrls['assignuser'] = App()->createUrl(
                    "plugins/direct",
                    array(
                        "plugin" => 'TokenUsersListAndManage',
                        "surveyid" => $surveyId,
                        "srid" => $srid,
                        "function" => "assignuser",
                    )
                );
                $toolOptions['assignuser'] = array(
                    'disabled' => !$haveAssignToken
                );
            }
            if (!empty($aAttributes['TulamSendMessage']) && $allowedSendEmail) {
                $toolUrls['sendmessage'] = App()->createUrl(
                    "plugins/direct",
                    array(
                        "plugin" => 'TokenUsersListAndManage',
                        "surveyid" => $surveyId,
                        "srid" => $srid,
                        "function" => "sendmessage",
                    )
                );
                $toolOptions['sendmessage'] = array(
                    'disabled' => !$haveMessageToken
                );
            }
            if (!empty($aAttributes['TulamContactUser']) && $allowedSendEmail) {
                $toolUrls['contact'] = App()->createUrl(
                    "plugins/direct",
                    array(
                        "plugin" => 'TokenUsersListAndManage',
                        "surveyid" => $surveyId,
                        "srid" => $srid,
                        "function" => "sendmessage",
                        "qid" => $qid
                    )
                );
                //~ $toolOptions['contact'] = array(
                    //~ 'disabled' => !$haveMessageToken
                //~ );
            }
            if (!empty($aAttributes['TulamCreateUser']) && $allowedCreateUser) {
                $toolUrls['newuser'] = App()->createUrl(
                    "plugins/direct",
                    array(
                        "plugin" => 'TokenUsersListAndManage',
                        "surveyid" => $surveyId,
                        "srid" => $srid,
                        "function" => "newuser",
                    )
                );
            }
        }
        $aTokenInfo = array(
            'toolUrls' => $toolUrls,
            'activeToolUrls' => array_filter($toolUrls),
            'toolOptions' => $toolOptions,
            'modalDone' => $this->modalDone,
        );
        $language = array(
            "Update my account" => $this->translate("Update my account"),
            "Manage users" => $this->translate("Manage users"),
            "Create a user and send an email with survey link." => self::translate("Create a user and send an email with survey link."),
            "Create a user" => self::translate("Create a user"),
            "Assign user and send an email with survey link." => $this->translate("Assign user and send an email with survey link."),
            "Assign user" => $this->translate("Assign user"),
            "Send message" => $this->translate("Send message"),
            "Contact user" => $this->translate("Contact user"),
            "No valid user to send message" => $this->translate("No valid user to send message"),
            "No user to assign" => $this->translate("No user to assign"),
        );
        $renderDataTwig = array_merge(
            $aTokenInfo,
            array(
                'aSurveyInfo' => $aSurveyInfo,
                'language' => $language,
            )
        );
        $TokenUsersListAndManagePlugin = new \TokenUsersListAndManagePlugin\TokenUsersManager($surveyId);
        $TokenUsersListAndManagePlugin->createAndRegisterPackage();
        $this->subscribe('getPluginTwigPath');
        $renderFile = "subviews/usersmanage/buttons/TULAM_buttongroup_question.twig";
        $extraPart = App()->twigRenderer->renderPartial(
            $renderFile,
            $renderDataTwig
        );

        if (!empty($aAttributes['TulamFrameToManagment']) && $allowedManagement) {
            $frameUrl = App()->createUrl(
                "plugins/direct",
                array(
                    "plugin" => 'TokenUsersListAndManage',
                    "surveyid" => $surveyId,
                    "returnsurveyid" => $currentSurveyId,
                    "function" => "listparticipant",
                    'originalSrid' => $srid,
                    'frame' => true,
                )
            );
            $renderDataTwig['frameUrl'] = $frameUrl;
            $renderDataTwig['assetUrl'] = App()->assetManager->publish(dirname(__FILE__) . '/assets');
            ;
            $renderFile = "subviews/include/TULAM_frame.twig";
            $extraPart .= App()->twigRenderer->renderPartial(
                $renderFile,
                $renderDataTwig
            );
        }
        $this->unsubscribe('getPluginTwigPath');
        return $extraPart;
    }

    /**
     * Return the HTML to be used in question rendering for list of manager
     * @param $qid question id
     * @param array $aAttributes of the question
     * @param integer $currentSurveyId, the surveyId of the current page
     * @param integer $surveyId, the surveyId to be used for buttons link and token data
     * @param integer $settingsSurveyId the settings surveyId
     * @param \Token $oUserToken the current token
     * @return string|null HTML
     */
    private function ownGetUserListHtml($qid, $aAttributes, $currentSurveyId, $surveyId, $settingsSurveyId, $oUserToken)
    {
        if (empty($aAttributes['TulamListUser'])) {
            return;
        }
        $showUserType = $aAttributes['TulamListUser'];
        /* Get the list of user, we know we have Group, unsure we have manager */
        $tokenAttributeGroup = \TokenUsersListAndManagePlugin\Utilities::getTokenAttributeGroup($settingsSurveyId);
        /* No group : return nothing */
        if ($oUserToken->getAttribute($tokenAttributeGroup) === "" || is_null($oUserToken->getAttribute($tokenAttributeGroup))) {
            return "";
        }
        $oUserToken = Token::model($surveyId)->find('token = :token', array( ":token" => $oUserToken->token));
        $tokenAttributeGroupManager = \TokenUsersListAndManagePlugin\Utilities::getTokenAttributeGroupManager($settingsSurveyId);
        /* Now get the list in surveyId */
        $tokenCriteria = new CDbCriteria();
        $tokenCriteria->compare($tokenAttributeGroup, $oUserToken->getAttribute($tokenAttributeGroup));
        $tokenCriteria->compare("emailstatus", '<>disable');
        if ($aAttributes['TulamListUser'] == 'manager') {
            $tokenCriteria->addCondition("$tokenAttributeGroupManager IS NOT NULL AND  $tokenAttributeGroupManager <> '' AND $tokenAttributeGroupManager <> '0'");
        }
        if (trim($aAttributes['TulamListUserOrder'])) {
            $tokenCriteria->order = trim($aAttributes['TulamListUserOrder']);
        } else {
            $tokenCriteria->order = 'lastname ASC, firstname ASC';
        }

        $oTokens = Token::model($surveyId)->findAll($tokenCriteria);
        $aTokens = [];
        foreach ($oTokens as $oToken) {
            $aTokens[] = $oToken->attributes;
        }
        /*Data for twig */
        $aSurveyInfo = getSurveyInfo($currentSurveyId, App()->getLanguage());
        $aTokenSurveyInfo = getSurveyInfo($surveyId, App()->getLanguage());
        $aSettingsSurveyInfo = getSurveyInfo($settingsSurveyId, App()->getLanguage());
        $language = array(
            "No email address" => $this->translate("No email address"),
            "Send an email" => $this->translate("Send an email"),
        );
        if (intval(App()->getConfig('versionnumber') > 3)) {
            $oQuestion = Question::model()->with('questionl10ns')->find(
                "t.qid = :qid",
                [":qid" => $qid]
            );
            $aQuestionInfo = $oQuestion->attributes;
            if (!empty($oQuestion->questionl10ns[App()->getLanguage()])) {
                $aQuestionInfo['question'] = $oQuestion->questionl10ns[App()->getLanguage()]->question;
                $aQuestionInfo['help'] = $oQuestion->questionl10ns[App()->getLanguage()]->help;
            }
        } else {
            $oQuestion = Question::model()->find(
                "qid = :qid and language = :language",
                [":qid" => $qid, ":language" => App()->getLanguage()]
            );
            $aQuestionInfo = $oQuestion->attributes;
        }
        /* Specific attributes */
        $aAttributesInfo = \TokenUsersListAndManagePlugin\TokenUsersManager::getExtractedAttributeData($surveyId);
        $renderDataTwig = array(
            'oTokens' => $oTokens,
            'aTokens' => $aTokens,
            'aSurveyInfo' => $aSurveyInfo,
            'language' => $language,
            'aQuestionInfo' => $aQuestionInfo,
            'aQuestionAttributes' => $aAttributes,
            'aAttributesInfo' => $aAttributesInfo,
            'oUserToken' => $oUserToken,
            'aTokenSurveyInfo' => $aTokenSurveyInfo,
            'aSettingsSurveyInfo' => $aSettingsSurveyInfo,
        );
        $renderFile = "subviews/include/TULAM_userlist.twig";
        $this->subscribe('getPluginTwigPath');
        $extraPart = App()->twigRenderer->renderPartial(
            $renderFile,
            $renderDataTwig
        );
        $this->unsubscribe('getPluginTwigPath');
        /* Return */
        return $extraPart;
    }

    /**
     * @inheritdoc
     * and set menu if needed
    **/
    public function saveSettings($settings)
    {
        if (!Permission::model()->hasGlobalPermission('settings', 'update')) {
            throw new CHttpException(403);
        }
        parent::saveSettings($settings);
        if (version_compare(App()->getConfig("versionnumber"), "3", "<")) {
            return;
        }
        /* Token Menu */
        $oSurveymenuEntries = SurveymenuEntries::model()->find("name = :name", array(":name" => 'TokenUsersListAndManage'));
        $createTokenMenu = App()->getRequest()->getPost('createTokenMenu');
        if (empty($oSurveymenuEntries) && $createTokenMenu) {
            $parentMenu = 1;
            $order = 3;
            /* Find response menu */
            $oTokensMenuEntries = SurveymenuEntries::model()->find("name = :name", array(":name" => 'participants'));
            if ($oTokensMenuEntries) {
                $parentMenu = $oTokensMenuEntries->menu_id;
                $order = $oTokensMenuEntries->ordering;
                $order++;
            }
            /* Unable to translate it currently … */
            $aNewMenu = array(
                'name' => 'TokenUsersListAndManage', // staticAddMenuEntry didn't use it but parse title
                'language' => 'en-GB',
                'title' => "Tokens user management",
                'menu_title' => "Tokens user management",
                'menu_description' => "Management of tokens user by token manager.",
                'menu_icon' => 'user-circle-o',
                'menu_icon_type' => 'fontawesome',
                'menu_link' => 'plugins/direct',
                'manualParams' => array(
                    'plugin' => 'TokenUsersListAndManage',
                ),
                'permission' => 'tokens',
                'permission_grade' => 'update',
                'pjaxed' => false,
                'hideOnSurveyState' => 'none',
                'addSurveyId' => true,
                'addQuestionGroupId' => false,
                'addQuestionId' => false,
                'linkExternal' => false,
                'ordering' => $order,
            );
            $iMenu = SurveymenuEntries::staticAddMenuEntry($parentMenu, $aNewMenu);
            $oSurveymenuEntries = SurveymenuEntries::model()->findByPk($iMenu);
            if ($oSurveymenuEntries) {
                $oSurveymenuEntries->ordering = $order;
                $oSurveymenuEntries->name = 'TokenUsersListAndManage'; // SurveymenuEntries::staticAddMenuEntry cut name, then reset
                $oSurveymenuEntries->save();
                SurveymenuEntries::reorderMenu($parentMenu);
            }
        }
        if (!empty($oSurveymenuEntries) && empty($createTokenMenu)) {
            SurveymenuEntries::model()->deleteAll("name = :name", array(":name" => 'TokenUsersListAndManage'));
        }
        /* Manage Menu */
        $oSurveymenuEntries = SurveymenuEntries::model()->find("name = :name", array(":name" => 'TokenUsersListAndManageSetting'));
        $createSettingMenu = App()->getRequest()->getPost('createSettingMenu');
        if (empty($oSurveymenuEntries) && $createSettingMenu) {
            $parentMenu = 1;
            $order = 3;
            /* Find response menu */
            $oTokensMenuEntries = SurveymenuEntries::model()->find("name = :name", array(":name" => 'tokens'));
            if ($oTokensMenuEntries) {
                $parentMenu = $oTokensMenuEntries->menu_id;
                $order = $oTokensMenuEntries->ordering;
                $order++;
            }
            /* Unable to translate it currently … */
            $aNewMenu = array(
                'name' => 'TokenUsersListAndManageSetting', // staticAddMenuEntry didn't use it but parse title
                'language' => 'en-GB',
                'title' => "Tokens user list management",
                'menu_title' => "Tokens user list management",
                'menu_description' => "Management of tokens list by token manager.",
                'menu_icon' => 'address-card',
                'menu_icon_type' => 'fontawesome',
                'menu_link' => 'admin/pluginhelper',
                'manualParams' => array(
                    'sa' => 'sidebody',
                    'plugin' => 'TokenUsersListAndManage',
                    'method' => 'actionSettings',
                ),
                'permission' => 'tokens',
                'permission_grade' => 'update',
                'pjaxed' => false,
                'hideOnSurveyState' => 'none',
                'addSurveyId' => true,
                'addQuestionGroupId' => false,
                'addQuestionId' => false,
                'linkExternal' => false,
                'ordering' => $order,
            );
            $iMenu = SurveymenuEntries::staticAddMenuEntry($parentMenu, $aNewMenu);
            $oSurveymenuEntries = SurveymenuEntries::model()->findByPk($iMenu);
            if ($oSurveymenuEntries) {
                $oSurveymenuEntries->ordering = $order;
                $oSurveymenuEntries->name = 'TokenUsersListAndManageSetting'; // SurveymenuEntries::staticAddMenuEntry cut name, then reset
                $oSurveymenuEntries->save();
                SurveymenuEntries::reorderMenu($parentMenu);
            }
        }
        if (!empty($oSurveymenuEntries) && empty($createSettingMenu)) {
            SurveymenuEntries::model()->deleteAll("name = :name", array(":name" => 'TokenUsersListAndManageSetting'));
        }
    }

    /** @inheritdoc **/
    public function beforeActivate()
    {
        if (!version_compare(Yii::app()->getConfig('versionnumber'), "3.10", ">=")) {
            $this->getEvent()->set(
                'success',
                false
            );
            $this->getEvent()->set(
                'message',
                sprintf($this->translate("You need LimeSurvey version %s and up."), '3.10')
            );
        }
        $this->setDb();
    }

    /**
     * Main function to replace surveySetting
     * @param int $surveyId Survey id
     *
     * @return string
     */
    public function actionSettings($surveyId)
    {
        $oSurvey = Survey::model()->findByPk($surveyId);
        if (!$oSurvey) {
            throw new CHttpException(404, gT("This survey does not seem to exist."));
        }
        if (!Permission::model()->hasSurveyPermission($surveyId, 'surveysettings', 'update')) {
            throw new CHttpException(403);
        }
        $settings = \TokenUsersListAndManagePlugin\Settings::getInstance();
        $settings->updateSettingsVersion($surveyId);
        if (App()->getRequest()->getPost('save' . get_class($this))) {
            if (App()->getRequest()->getPost('save' . get_class($this)) == 'tokenAttributeExportWithRestricted') {
                $this->exportWithRestricted($surveyId);
            }
            /* Basic settings */
            $settings = [
                "active",
                "managementByParent",
                //"managementByParentManual", // Specific setting
                "useAttributeRestricted",
                "allowAllAttributeRestricted",
                "responseListTitle",
                "listSurveyRestrictedByParent",
                "allowNoMessageCreate",
                "allowNoMessageAssign",
                "forcedTextMessage",
                "setReplyTo",
                "template",
                "tokenAttributeGroup",
                "tokenAttributeGroupManager",
                "tokenAttributeRestricted",
                "tokenFirstnameUsage",
                "tokenLastnameUsage",
                "tokenEmailUsage",
                "tokenTokenShow",
                "userManagementAccess",
                "allowSetManager",
                "allowCreateUser",
                "allowGiveRight",
                "allowSendEmail",
                "allowEditAccount",
                "keepMandatoryAttributeWhenDelete",
            ];
            /* List of attributes : can not be used elsewhere */
            $settingsAttributes = array(
                'attributesBoolean',
                'attributesSelect',
                'attributesSelectRestrictToken',
            );
            /* List of attributes : can be used elsewhere */
            $settingsAttributesExtra = array(
                'attributesReadonly',
                'attributesReadonlyWhenUpdate',
                'attributesCopyWhenCreate'
            );

            foreach ($settings as $setting) {
                $this->set($setting, App()->getRequest()->getPost($setting), 'Survey', $surveyId);
            }
            $usedAttributes = array(
                'tokenAttributeGroup' => App()->getRequest()->getPost('tokenAttributeGroup'),
                'tokenAttributeGroupManager' => App()->getRequest()->getPost('tokenAttributeGroupManager'),
                'tokenAttributeRestricted' => App()->getRequest()->getPost('tokenAttributeRestricted'),
            );
            $showAsAttributes = array();
            $pluginSettings = App()->request->getPost('plugin', array());
            foreach ($pluginSettings as $plugin => $settings) {
                $settingsEvent = new PluginEvent('newTULAMSurveySettings');
                $settingsEvent->set('settings', $settings);
                $settingsEvent->set('survey', $surveyId);
                $settingsEvent->set('surveyId', $surveyId);
                $settingsEvent->set('usedAttributes', $usedAttributes);
                $settingsEvent->set('showAsAttributes', $showAsAttributes);
                App()->getPluginManager()->dispatchEvent($settingsEvent, $plugin);
                $usedAttributes = $settingsEvent->get('usedAttributes', []);
                $showAsAttributes = $settingsEvent->get('showAsAttributes', []);
            }
            foreach ($settingsAttributes as $settingAttribute) {
                $values = (array) App()->getRequest()->getPost($settingAttribute);
                $values = array_diff($values, $showAsAttributes);
                $showAsAttributes = array_merge($showAsAttributes, $values);
                $this->set($settingAttribute, $values, 'Survey', $surveyId);
            }
            foreach ($settingsAttributesExtra as $settingAttributesExtra) {
                $values = (array) App()->getRequest()->getPost($settingAttributesExtra);
                $this->set($settingAttributesExtra, $values, 'Survey', $surveyId);
            }
            /*  Specifc settings*/
            /* Fix about permission for "managementByParentManual" */
            $managementByParentManual = trim(App()->getRequest()->getPost('managementByParentManual'));
            if (empty($managementByParentManual)) {
                $this->set('managementByParentManual', '', 'Survey', $surveyId);
            } else {
                if (
                    !Permission::model()->hasSurveyPermission($managementByParentManual, 'tokens', 'create') // token permission
                    || !Permission::model()->hasSurveyPermission($managementByParentManual, 'tokens', 'update')
                    || !Permission::model()->hasSurveyPermission($managementByParentManual, 'tokens', 'delete')
                    || !Permission::model()->hasSurveyPermission($managementByParentManual, 'surveysettings', 'update') // surveysettings permission
                ) {
                    App()->setFlashMessage(sprintf($this->translate("You don't have access to survey %s"), CHtml::encode($managementByParentManual)));
                } else {
                    $this->set('managementByParentManual', $managementByParentManual, 'Survey', $surveyId);
                }
            }
            /* Action on childs */
            if (App()->getRequest()->getPost('save' . get_class($this)) == 'tokenAttributeSetChildrensAction') {
                $this->setChildrensAction($surveyId);
            }


            if (App()->getRequest()->getPost('save' . get_class($this)) == 'redirect') {
                if (intval(App()->getConfig('versionnumber')) < 4) {
                    Yii::app()->getController()->redirect(
                        Yii::app()->getController()->createUrl(
                            'admin/survey',
                            array('sa' => 'view','surveyid' => $surveyId)
                        )
                    );
                }
                Yii::app()->getController()->redirect(
                    Yii::app()->getController()->createUrl(
                        'surveyAdministration/view',
                        array('surveyid' => $surveyId)
                    )
                );
            }
        }

        /* Get some settings and variables */
        $tokenAttributeGroup = \TokenUsersListAndManagePlugin\Utilities::getTokenAttributeGroup($surveyId);
        $tokenAttributeGroupManager = \TokenUsersListAndManagePlugin\Utilities::getTokenAttributeGroupManager($surveyId);
        $responseListAndManageApi = \TokenUsersListAndManagePlugin\Utilities::getResponseListAndManageApi();
        $managedInReponseListAndManage = $responseListAndManageApi && version_compare($responseListAndManageApi, "2.5", "<");
        $TokensAttributeList = \TokenUsersListAndManagePlugin\Utilities::getTokensAttributeList($surveyId);
        $oTemplates = TemplateConfiguration::model()->findAll(array(
            'condition' => 'sid IS NULL',
        ));
        $aTemplates = CHtml::listData($oTemplates, 'template_name', 'template_name');
        $currentTemplate = $this->get('template', 'Survey', $surveyId, '');
        if (!isset($aTemplates[$currentTemplate])) {
            $currentTemplate = null;
        }
        if (!Permission::model()->hasGlobalPermission('templates', 'read')) {
            $aTemplates = array_filter($aTemplates, function ($templateName) {
                return Permission::model()->hasTemplatePermission($templateName);
            });
            if (!empty($currentTemplate) && !isset($aTemplates[$currentTemplate])) {
                $aTemplates[$currentTemplate] = $currentTemplate;
            }
        }
        /* Check parent */
        $managementByParentHelp = $this->translate("Need RelatedSurveyManagement plugin.") . $this->translate("You need to choose manually.");
        $haveRelatedSurveyManagement = Yii::getPathOfAlias('RelatedSurveyManagement') && version_compare(\RelatedSurveyManagement\Utilities::API, "0.4", ">=");
        if ($haveRelatedSurveyManagement) {
            $managementByParentHelp = $this->translate("No parent found, token management is done here.") . $this->translate("You need to choose manually.");
            $oParentSurvey = new RelatedSurveyManagement\ParentSurveys($surveyId);
            $parentSurvey = $oParentSurvey->getFinalAncestrySurvey();
            if (!is_null($parentSurvey)) {
                if ($parentSurvey) {
                    $managementByParentHelp = "<div class='text-success'>" . CHtml::link(
                        sprintf($this->translate("Current parent survey %s."), $parentSurvey),
                        array("surveyAdministration/view", 'surveyid' => $parentSurvey)
                    ) . "</div>";
                    if (intval(App()->getConfig('versionnumber')) <= 3) {
                        $managementByParentHelp = "<div class='text-success'>" . CHtml::link(
                            sprintf($this->translate("Current parent survey %s."), $parentSurvey),
                            array("admin/survey/sa/view", 'surveyid' => $parentSurvey)
                        ) . "</div>";
                    }
                    $managementByParentHelp .= "<div class=''>" . $this->translate("With the exception of the texts of all messages and usage of restricted attribute, which always come from the current survey, all parameters are taken from the parent questionnaire. The user is created in the parent questionnaire and in all its child questionnaires.") . "</div>";
                } else {
                    $managementByParentHelp = "<div class='text-warning'>" . $this->translate("Multiple parents found, unable to choose the valid one. ") . $this->translate("You need to choose manually.") . "</div>";
                }
                $managementByParentHelp .= "<div class=''>" . $this->translate("This parameter also controls whether actions on parent surveys token (via Token user List Management) apply to this survey.") . "</div>";
            }
        }
        $managementByParentManualHelp = "";
        if ($this->get('managementByParentManual', 'Survey', $surveyId, 0)) {
            /* Find the title */
            $oSurvey = Survey::model()->findByPk(intval($this->get('managementByParentManual', 'Survey', $surveyId, 0)));
            if (!$oSurvey) {
                $managementByParentManualHelp = "<div class='text-warning'>" . sprintf($this->translate("Parent survey %s not found."), CHtml::encode($this->get('managementByParentManual', 'Survey', $surveyId, 0)))   . "</div>";
            } else {
                $manuelParentId = intval($this->get('managementByParentManual', 'Survey', $surveyId, 0));
                $managementByParentManualHelp = "<div class='text-success'>" . CHtml::link(
                    sprintf($this->translate("Current parent survey %s (%s)."), viewHelper::flatEllipsizeText($oSurvey->getLocalizedTitle()), $manuelParentId),
                    array("surveyAdministration/view", 'surveyid' => $manuelParentId)
                ) . "</div>";
                if (intval(App()->getConfig('versionnumber')) <= 3) {
                    $managementByParentManualHelp = "<div class='text-success'>" . CHtml::link(
                        sprintf($this->translate("Current parent survey %s (%s)."), viewHelper::flatEllipsizeText($oSurvey->getLocalizedTitle()), $manuelParentId),
                        array("admin/survey/sa/view", 'surveyid' => $manuelParentId)
                    ) . "</div>";
                }
                $managementByParentManualHelp .= "<div class=''>" . $this->translate("This parameter also controls whether actions on parent surveys token (via Token user List Management) apply to this survey.") . "</div>";
            }
        }
        $surveyColumnsInformation = new \getQuestionInformation\helpers\surveyColumnsInformation($surveyId, App()->getLanguage());
        $surveyColumnsInformation->ByEmCode = true;
        $aQuestionList = $surveyColumnsInformation->allQuestionListData();
        $isActive = $this->get('active', null, null, 0) ? gT('Yes') : gT('No');
        $managementByParentDefault = ($haveRelatedSurveyManagement && $this->get('managementByParent', null, null, 0)) ? gT('Yes') : gT('No');
        $useAttributeRestrictedDefault = $this->get('useAttributeRestricted', null, null, 1) ? gT('Yes') : gT('No');
        $allowAllAttributeRestrictedDefault = $this->get('allowAllAttributeRestricted', null, null, 1) ? gT('Yes') : gT('No');
        $listSurveyRestrictedByParentDefault = $this->get('listSurveyRestrictedByparent', null, null, 0) ? gT('Yes') : gT('No');

        /* Basic settings (on this surey) */
        $aSettings[$this->translate('This survey setting')] = array(
            'active' => array(
                'type' => 'select',
                'options' => array(
                    '0' => gT("No"),
                    '1' => gT("Yes"),
                ),
                'htmlOptions' => array(
                    'empty' => sprintf($this->translate("Leave default (%s)"), $isActive)
                ),
                'label' => $this->translate('User management is active for this survey.'),
                'help' => $this->translate('This is necessary for all access of other system.'),
                'current' => $this->get('active', 'Survey', $surveyId, "")
            ),
            'managementByParent' => array(
                'type' => 'select',
                'options' => array(
                    '0' => gT("No"),
                    '1' => gT("Yes"),
                    'manual' => $this->translate("Manually"),
                ),
                'htmlOptions' => array(
                    'empty' => sprintf($this->translate("Leave default (%s)"), $managementByParentDefault)
                ),
                'label' => $this->translate('Use the parent for the manager, and track token changes.'),
                'help' => $managementByParentHelp,
                'current' => $this->get('managementByParent', 'Survey', $surveyId, '')
            ),
            'managementByParentManual' => array(
                'type' => 'string',
                'label' => $this->translate('Survey ID to be used for parent management.'),
                'help' => $managementByParentManualHelp,
                'current' => $this->get('managementByParentManual', 'Survey', $surveyId, '')
            ),
            'useAttributeRestricted' => array(
                'type' => 'select',
                'label' => $this->translate('Use restricted attribute.'),
                'help' => $this->translate('Even if set, you can deactivate usage of restricted attribute. This setting is used only for access to survey response.'),
                'options' => array(
                    '0' => gT("No"),
                    '1' => gT("Yes"),
                ),
                'htmlOptions' => array(
                    'empty' => sprintf($this->translate("Leave default (%s)"), $useAttributeRestrictedDefault)
                ),
                'current' => $this->get('useAttributeRestricted', 'Survey', $surveyId, '')
            ),
            'allowAllAttributeRestricted' => array(
                'type' => 'select',
                'label' => $this->translate('Allow to access to all.'),
                'help' => $this->translate('Allow manager to select all survey response for access restriction.'),
                'options' => array(
                    '0' => gT("No"),
                    '1' => gT("Yes"),
                ),
                'htmlOptions' => array(
                    'empty' => sprintf($this->translate("Leave default (%s)"), $allowAllAttributeRestrictedDefault)
                ),
                'current' => $this->get('allowAllAttributeRestricted', 'Survey', $surveyId, '')
            ),
            'responseListTitle' => array(
                'type' => 'select',
                'label' => $this->translate('Question code to be used for listing.'),
                'help' => $this->translate('If not set use the first one find by parent.'),
                'options' => $aQuestionList['data'],
                'htmlOptions' => array(
                    'empty' => $this->translate("Automatic"),
                    'options' => $aQuestionList['options'], // In dropdown, but not in select2
                ),
                'selectOptions' => array(
                    //'placeholder'=>gT("None"),
                    //~ 'templateResult'=>"formatQuestion",
                ),
                'current' => $this->get('responseListTitle', 'Survey', $surveyId, '')
            ),
            'listSurveyRestrictedByParent' => array(
                'type' => 'select',
                'label' => $this->translate('Get the restricted survey by parent.'),
                'help' => $this->translate('In user managament : when edit an user : list of restricted survey show childs of parent survey.'),
                'options' => array(
                    '0' => gT("No"),
                    '1' => gT("Yes"),
                ),
                'htmlOptions' => array(
                    'empty' => sprintf($this->translate("Leave default (%s)"), $listSurveyRestrictedByParentDefault)
                ),
                'current' => $this->get('listSurveyRestrictedByParent', 'Survey', $surveyId, 0)
            ),
            'template' => array(
                'type' => 'select',
                'label' => $this->translate('Template to be used.'),
                'options' => $aTemplates,
                'htmlOptions' => array(
                    'empty' => $this->translate("Use same than survey."),
                ),
                'current' => $currentTemplate,
                'help' => $this->translate("Theme options for this survey are applied (inherithed too)."),
            ),
        );
        /* Message settings */
        $aSettings[$this->translate('Messages settings')] = array(
            'messageInfo' => array(
                'type' => 'info',
                'content' => "<div class='well'>" .
                    "<p>" . $this->translate("Settings here are used for current survey, when use button question type.") . "</p>" .
                    "<ul>" .
                        "<li>" . $this->translate("Send message inside survey use reminder email template") . "</li>" .
                        "<li>" . $this->translate("Create user message use register email template") . "</li>" .
                        "<li>" . $this->translate("Send message inside manager use invite email template") . "</li>" .
                    "</ul>" .
                    "<p>" . $this->translate("You can use these specific keywords.") . "</p>" .
                    "<ul>" .
                        "<li>" . $this->translate("All {TOKEN:} keyword like LimeSurvey") . "</li>" .
                        "<li>" . $this->translate("{ADMINNAME} is replaced by the current user firstname and lastname.") . "</li>" .
                        "<li>" . $this->translate("{ADMINEMAIL} is replaced by the current user email.") . "</li>" .
                        "<li>" . $this->translate("{SURVEYDESCRIPTION} and {MESSAGE} is replaced by the mesage from current user.") . "</li>" .
                        "<li>" . $this->translate("{SURVEYURL} is replaced by the link to the current responses with token and srid.") . "</li>" .
                        "<li>" . $this->translate("{PARENTURL} is replaced by the link to the last ancestry url if exist.") . "</li>" .
                        "<li>" . $this->translate("{OPTOUTURL} is replaced by the link to disable future email. This link didn't replace deletion.") . "</li>" .
                    "</ul>" .
                    "</div>",
            ),
            'forcedTextMessage' => array(
                'type' => 'select',
                'label' => $this->translate('Instruction on message are mandatory.'),
                'options' => array(
                    '0' => gT("No"),
                    '1' => gT("Yes"),
                ),
                //~ 'htmlOptions' => array(
                    //~ 'empty' => sprintf($this->translate("Leave default (%s)"), $useAttributeRestrictedDefault)
                //~ ),
                'current' => $this->get('forcedTextMessage', 'Survey', $surveyId, '0')
            ),
            'allowNoMessageCreate' => array(
                'type' => 'select',
                'label' => $this->translate('Allow to disable send message when create user.'),
                'options' => array(
                    '0' => gT("No"),
                    '1' => gT("Yes"),
                ),
                //~ 'htmlOptions' => array(
                    //~ 'empty' => sprintf($this->translate("Leave default (%s)"), $useAttributeRestrictedDefault)
                //~ ),
                'current' => $this->get('allowNoMessageCreate', 'Survey', $surveyId, '0')
            ),
            'allowNoMessageAssign' => array(
                'type' => 'select',
                'label' => $this->translate('Allow to disable send message when assign user.'),
                'options' => array(
                    '0' => gT("No"),
                    '1' => gT("Yes"),
                ),
                //~ 'htmlOptions' => array(
                    //~ 'empty' => sprintf($this->translate("Leave default (%s)"), $useAttributeRestrictedDefault)
                //~ ),
                'current' => $this->get('allowNoMessageAssign', 'Survey', $surveyId, '0')
            ),
            'setReplyTo' => array(
                'type' => 'select',
                'label' => $this->translate('Set the Reply-To to the email adress of the manager.'),
                'help' => $this->translate('Due to potential issue with any email : From-To use current email admin user settings. Here you can set the Reply-to'),
                'options' => array(
                    '0' => gT("No"),
                    '1' => gT("Yes"),
                    'both' => $this->translate("Both : set to admin user and manager email"),
                ),
                'current' => $this->get('setReplyTo', 'Survey', $surveyId, '1')
            ),
        );
        if (intval(App()->getConfig('versionnumber')) < 4 && !App()->getConfig('eventBeforeTokenEmailExtendedApi')) {
            unset($aSettings[$this->translate('Messages settings')]['setReplyTo']);
            if (intval(App()->getConfig('versionnumber')) < 4) {
                unset($aSettings[$this->translate('Messages settings')]['setReplyTo']['options']['both']);
            }
        }
        $aSettings[$this->translate('Token attribute settings')] = array(
            'tokenAttributeGroup' => array(
                'type' => 'select',
                'label' => \TokenUsersListAndManagePlugin\Utilities::translate('Token attributes for group'),
                'help' => $managedInReponseListAndManage ?
                    $this->translate("Managed in responseListAndManage plugin") :
                    $this->translate("Choose the attribute to be used to group participant"),
                'options' => $TokensAttributeList,
                'htmlOptions' => array(
                    'empty' =>  $this->translate("None"),
                    'disabled' => $managedInReponseListAndManage,
                ),
                'current' => $tokenAttributeGroup
            ),
            'tokenAttributeGroupManager' => array(
                'type' => 'select',
                'label' => \TokenUsersListAndManagePlugin\Utilities::translate('Token attributes for group manager'),
                'help' => $managedInReponseListAndManage ?
                    $this->translate("Managed in responseListAndManage plugin") :
                    $this->translate("Attribute for participant user manager, more right by default than other particpant. Attribute use boolean comparaison."),
                'options' => $TokensAttributeList,
                'htmlOptions' => array(
                    'empty' =>  $this->translate("None"),
                    'disabled' => $managedInReponseListAndManage,
                ),
                'current' => $tokenAttributeGroupManager
            ),
            'tokenAttributeRestricted' => array(
                'type' => 'select',
                'label' => $this->translate('Token attributes for survey restriction'),
                'help' => $this->translate('Any value except 0 and empty string set this to true. User with restriction can edit only some response. This attribute are shown in management using specific system.'),
                'options' => $TokensAttributeList,
                'htmlOptions' => array(
                    'empty' => $this->translate("None"),
                ),
                'current' => $this->get('tokenAttributeRestricted', 'Survey', $surveyId)
            ),
            'keepMandatoryAttributeWhenDelete' => array(
                'type' => 'select',
                'label' => $this->translate('Keep the mandatory attribute when delete participant.'),
                'help' => $this->translate("When deleting a participant's data, the group is still retained. You can choose to keep the mandatory attributes (excluding last name, first name and email address)."),
                'options' => array(
                    '0' => gT("No"),
                    '1' => gT("Yes"),
                ),
                //~ 'htmlOptions' => array(
                    //~ 'empty' => sprintf($this->translate("Leave default (%s)"), $listSurveyRestrictedByParentDefault)
                //~ ),
                'current' => $this->get('keepMandatoryAttributeWhenDelete', 'Survey', $surveyId, 0)
            ),
        );
        /* Copy to childs surveys */
        if ($haveRelatedSurveyManagement) {
            $oChildSurveys = new RelatedSurveyManagement\ChildrenSurveys($surveyId);
            if ($oChildSurveys->getChildrensSurveys()) {
                $aSettings[$this->translate('Childs token attribute update')] = array();
                $aSettings[$this->translate('Childs token attribute update')]['tokenAttributeSetChildrens'] = array(
                    'type' => 'select',
                    'label' => $this->translate('particpants and attributes of child questionnaires update'),
                    'help' => $this->translate("You can update particpants and attributes of child survey, you need to click the next button for the action"),
                    'options' => array(
                        'settings' => $this->translate("Settings of Token management"),
                        'attributes' => $this->translate("Attributes definition and settings of Token management."),
                        'tokens' => $this->translate("Attributes definition, settings of Token management and complete participants table (can delete tokens)"),
                        'onlytokens' => $this->translate("Whole participants table (delete tokens in childs if don't exist in this one)."),
                        'existingtokens' => $this->translate("Copy and update existing participants (find and update particpant with tokens, don't delete tokens in childs)."),
                    ),
                );
                $aSettings[$this->translate('Childs token attribute update')]['tokenAttributeSetChildrensAction'] = array(
                    'type' => 'info',
                    'content' => '<div class="col-md-6 col-md-offset-6">'
                        . '<p class="text-warning"><strong>' . $this->translate("Warning") . '</strong> ' . $this->translate('This update child survey and can delete attribute information and token') . '</p>'
                        . '<button type="submit" name="saveTokenUsersListAndManage" value="tokenAttributeSetChildrensAction" class="btn btn-default"><i class="fa fa-check-circle-o " aria-hidden="true"></i>'
                        . $this->translate("Save and set attributes to all descendant surveys (childs and childs of childs).")
                        . '</button>'
                        . '</div>'
                );
                $disableAttribute = "";
                $haveRightExportTokens = Permission::model()->hasSurveyPermission($surveyId, 'tokens', 'export');
                if (!$haveRightExportTokens) {
                    $disableAttribute = " disabled";
                }
                $aSettings[$this->translate('Childs token attribute update')]['tokenAttributeExportWithChildrens'] = array(
                    'type' => 'info',
                    'content' => '<div class="col-md-6 col-md-offset-6">'
                        . '<button type="submit" name="saveTokenUsersListAndManage" value="tokenAttributeExportWithRestricted" ' . $disableAttribute . ' class="btn btn-default"><i class="fa fa-download" aria-hidden="true"></i>'
                        . $this->translate("Export tokens with restricted list on survey and childs (only childs, not all descendant).")
                        . '</button>'
                        . '</div>'
                );
            }
        }
        /* Basic settings (on this surey) */
        $aSettings[$this->translate('Token management settings')] = array(
            'parentInfo' => array(
                'type' => 'info',
                'class' => 'well',
                'content' => $this->translate("This part are not used (for Token list manager) if you use parent survey, remind to review responseListAndManage, reloadAnyReponse and other plugin for usage of this settings too."),
            ),
            'tokenFirstnameUsage' => array(
                'type' => 'select',
                'label' => $this->translate('Usage of firstname'),
                'options' => array(
                    'show' => $this->translate("Show it"),
                    'mandatory' => $this->translate("Show it as mandatory"),
                    'hidden' => $this->translate("Hide it"),
                ),
                //~ 'htmlOptions'=>array(
                    //~ 'empty'=>$this->translate("Leave default"),
                //~ ),
                'current' => $this->get('tokenFirstnameUsage', 'Survey', $surveyId, 'mandatory')
            ),
            'tokenLastnameUsage' => array(
                'type' => 'select',
                'label' => $this->translate('Usage of lastname'),
                'options' => array(
                    'show' => $this->translate("Show it"),
                    'mandatory' => $this->translate("Show it as mandatory"),
                    'hidden' => $this->translate("Hide it"),
                ),
                //~ 'htmlOptions'=>array(
                    //~ 'empty'=>$this->translate("Leave default"),
                //~ ),
                'current' => $this->get('tokenLastnameUsage', 'Survey', $surveyId, 'mandatory')
            ),
            'tokenEmailUsage' => array(
                'type' => 'select',
                'label' => $this->translate('Usage of email'),
                'help' => $this->translate('Set to hidden disable send email when create or assign, but not send mesage directly.'),
                'options' => array(
                    'show' => $this->translate("Show it"),
                    'mandatory' => $this->translate("Show it as mandatory"),
                    'hidden' => $this->translate("Hide it"),
                ),
                //~ 'htmlOptions'=>array(
                    //~ 'empty'=>$this->translate("Leave default"),
                //~ ),
                'current' => $this->get('tokenEmailUsage', 'Survey', $surveyId, 'mandatory')
            ),
            'tokenTokenShow' => array(
                'type' => 'select',
                'label' => $this->translate('Show token in management'),
                'help' => $this->translate('With this settings you can choose to shown token to all user with access to edition or list of user.'),
                'options' => array(
                    'hide' => $this->translate("Hide it"),
                    'readonly' => $this->translate("Show as readonly input"),
                    'link' => $this->translate("Show as link to copy"),
                ),
                //~ 'htmlOptions'=>array(
                    //~ 'empty'=>$this->translate("Leave default"),
                //~ ),
                'current' => $this->get('tokenTokenShow', 'Survey', $surveyId, 'hide')
            ),

            'userManagementAccess' => array(
                'type' => 'select',
                'label' => $this->translate('Allow to user management.'),
                'options' => array(
                    'none' => gT("None"),
                    'limesurvey' => $this->translate("Only for LimeSurvey administrator (According to LimeSurvey Permission)"),
                    'admin' => $this->translate("For group administrator and LimeSurvey administrator"),
                    'user' => $this->translate("For all user except restricted"),
                ),
                'help' => $this->translate('Management user access give right to edit other user, create new user and give any right. This right is need to create,update or disable user.'),
                'current' => $this->get('userManagementAccess', 'Survey', $surveyId, 'limesurvey')
            ),
            'allowSetManager' => array(
                'type' => 'select',
                'label' => $this->translate('Allow set administrator.'),
                'options' => array(
                    'none' => gT("None"),
                    'limesurvey' => $this->translate("Only for LimeSurvey administrator (According to LimeSurvey Permission)"),
                    'admin' => $this->translate("For group administrator and LimeSurvey administrator"),
                ),
                'help' => $this->translate('This allow user to create, set and unset new manager. This not manage edition of manager.'),
                'current' => $this->get('allowSetManager', 'Survey', $surveyId, 'admin')
            ),
            'allowCreateUser' => array(
                'type' => 'select',
                'label' => $this->translate('Allow to create user.'),
                'options' => array(
                    'none' => gT("None"),
                    'limesurvey' => $this->translate("Only for LimeSurvey administrator (According to LimeSurvey Permission)"),
                    'admin' => $this->translate("For group administrator and LimeSurvey administrator"),
                    'user' => $this->translate("For all user except restricted"),
                ),
                'help' => $this->translate('This allow user to create new user only. Can not be used to update existing user, see user list management for update.'),
                'current' => $this->get('allowCreateUser', 'Survey', $surveyId, 'admin')
            ),
            'allowGiveRight' => array(
                'type' => 'select',
                'label' => $this->translate('Allow to give right to other user.'),
                'help' => $this->translate('Allow to show a button inside survey, not tested in user management.'),
                'options' => array(
                    'none' => gT("None"),
                    'admin' => $this->translate("For group administrator"),
                    'user' => $this->translate("For all user except restricted"),
                    'all' => $this->translate("For all user with right access"),
                ),
                'current' => $this->get('allowGiveRight', 'Survey', $surveyId, 'user')
            ),
            'allowSendEmail' => array(
                'type' => 'select',
                'label' => $this->translate('Allow to send message to other user.'),
                'help' => $this->translate('Replace give right button if give right access is disable.'),
                'options' => array(
                    'none' => gT("None"),
                    'admin' => $this->translate("For group administrator"),
                    'user' => $this->translate("For all user except restricted"),
                    'all' => $this->translate("For all user with right access"),
                ),
                'current' => $this->get('allowSendEmail', 'Survey', $surveyId, 'all')
            ),
            'allowEditAccount' => array(
                'type' => 'select',
                'label' => $this->translate('Allow to update own account.'),
                'options' => array(
                    'none' => gT("None"),
                    'admin' => $this->translate("For manager"),
                    'user' => $this->translate("For all user except restricted"),
                    'all' => $this->translate("For all user"),
                ),
                'current' => $this->get('allowEditAccount', 'Survey', $surveyId, 'user')
            ),
        );
        /* Basic settings (on this surey) */
        $aSettings[$this->translate('Attributes usage')] = array(
            'attributesRegisterInfo' => array(
                'type' => 'info',
                'content' => "<div class='well well-sm'>" . $this->translate('The attribute shown in management are the attribute show in regsitration. You can set differnt usage for each attribute here.') . "</div>",
            ),
            'attributesBoolean' => array(
                'type' => 'select',
                'label' => $this->translate('Boolean token attributes'),
                'help' =>  $this->translate("Show as checkbox when edit, to be used as booelan in expression. Only if attribute is shown in register"),
                'options' => $TokensAttributeList,
                'htmlOptions' => array(
                    'empty' =>  $this->translate("None"),
                    'multiple' => true
                ),
                'current' => $this->get('attributesBoolean', 'Survey', $surveyId, null)
            ),
            'attributesSelect' => array(
                'type' => 'select',
                'label' => $this->translate('Dropdown token attributes'),
                'help' =>  $this->translate("Show as a dropdown, dropdown list from single choice question with same code than description of attribute."),
                'options' => $TokensAttributeList,
                'htmlOptions' => array(
                    'empty' =>  $this->translate("None"),
                    'multiple' => true
                ),
                'current' => $this->get('attributesSelect', 'Survey', $surveyId, [])
            ),
            'attributesSelectRestrictToken' => array(
                'type' => 'select',
                'label' => $this->translate('Dropdown token attributes, allow one choice only'),
                'help' =>  $this->translate("Show as a dropdown, dropdown list from single choice question with same code than description of attribute.<br>Remove already choose value from dropdown by group. This can be used to allow one participant with this attribute value by group."),
                'options' => $TokensAttributeList,
                'htmlOptions' => array(
                    'empty' =>  $this->translate("None"),
                    'multiple' => true
                ),
                'current' => $this->get('attributesSelectRestrictToken', 'Survey', $surveyId, [])
            ),
            'attributesReadonly' => array(
                'type' => 'select',
                'label' => $this->translate('Read only token attributes'),
                'help' =>  $this->translate("Show as readonly when edit (if attribute is shown in register)."),
                'options' => $TokensAttributeList,
                'htmlOptions' => array(
                    'empty' =>  $this->translate("None"),
                    'multiple' => true
                ),
                'current' => $this->get('attributesReadonly', 'Survey', $surveyId, null)
            ),
            'attributesReadonlyWhenUpdate' => array(
                'type' => 'select',
                'label' => $this->translate('Read only token attributes after creation'),
                'help' =>  $this->translate("Show as readonly when edit (if attribute is shown in register). This allow to set a value when create, but disallow edition after creation."),
                'options' => $TokensAttributeList,
                'htmlOptions' => array(
                    'empty' =>  $this->translate("None"),
                    'multiple' => true
                ),
                'current' => $this->get('attributesReadonlyWhenUpdate', 'Survey', $surveyId, null)
            ),
            'attributesCopyWhenCreate' => array(
                'type' => 'select',
                'label' => $this->translate('Copied token attributes'),
                'help' =>  $this->translate("When user was create, this attributes was set with the same values than current user. Allow to set value on hidden attribute or to set default value for register attribute."),
                'options' => $TokensAttributeList,
                'htmlOptions' => array(
                    'empty' =>  $this->translate("None"),
                    'multiple' => true
                ),
                'current' => $this->get('attributesCopyWhenCreate', 'Survey', $surveyId, null)
            ),
        );
        $beforeTULAMSurveySettings = new PluginEvent('beforeTULAMSurveySettings');
        $beforeTULAMSurveySettings->set('survey', $surveyId);
        $beforeTULAMSurveySettings->set('surveyId', $surveyId);
        $beforeTULAMSurveySettings->set('aQuestionList', $aQuestionList);
        $beforeTULAMSurveySettings->set('aTokensAttributeList', $TokensAttributeList);
        App()->getPluginManager()->dispatchEvent($beforeTULAMSurveySettings);
        $aSurveyPluginsSettings = $beforeTULAMSurveySettings->get('settings');
        if (!empty($aSurveyPluginsSettings)) {
            $aData['aSurveyPluginsSettings'] = $aSurveyPluginsSettings;
        }
        $aData['pluginClass'] = get_class($this);
        $aData['surveyId'] = $surveyId;
        $aData['title'] = $this->translate("Token user list management");
        $aData['warningString'] = null;
        $aData['aSettings'] = $aSettings;
        $aData['assetUrl'] = Yii::app()->assetManager->publish(dirname(__FILE__) . '/assets/settings');
        $content = $this->renderPartial('settings', $aData, true);
        return $content;
    }

    /** @inheritdoc **/
    public function newDirectRequest()
    {
        if ($this->getEvent()->get('target') != get_class($this)) {
            return;
        }
        $this->setDb();
        $this->subscribe('getPluginTwigPath');
        $surveyId = App()->getRequest()->getQuery('sid', App()->getRequest()->getQuery('surveyid'));
        if (strval(intval($surveyId)) !== strval($surveyId)) {
            throw new CHttpException(403, gT("Invalid survey id"));
        }
        $function = $this->getEvent()->get('function', 'listparticipant');

        if (!$this->getSetting($surveyId, 'active')) {
            if ($function == 'listparticipant' && Permission::model()->hasSurveyPermission($surveyId, 'surveysettings', 'update')) {
                App()->setFlashMessage($this->translate("Token user list management not activated"), 'danger');
                $redirectUrl = App()->createUrl(
                    'admin/pluginhelper',
                    [
                        'sa' => 'sidebody',
                        'plugin' => get_class($this),
                        'method' => 'actionSettings',
                        'surveyId' => $surveyId
                    ]
                );
                App()->getRequest()->redirect($redirectUrl);
                App()->end();
            }
            throw new CHttpException(403, gT("Invalid survey id"));
        }
        $settings = \TokenUsersListAndManagePlugin\Settings::getInstance();
        $settings->updateSettingsVersion($surveyId);
        $TokenUsersManager = new TokenUsersListAndManagePlugin\TokenUsersManager($surveyId);
        $TokenUsersManager->token = $this->getCurrentToken($surveyId);
        /* We are here : token is OK */
        switch ($function) {
            case 'email':
                $this->subscribe('beforeTokenEmailExtended', 'setEmailResponseTo');
                $TokenUsersManager->actionEmail();
                App()->end();
                break;
            case 'disable':
                $TokenUsersManager->actionDisable();
                App()->end();
                break;
            case 'empty':
                $TokenUsersManager->actionDelete();
                App()->end();
                break;
            case 'edit':
                $TokenUsersManager->actionEdit();
                App()->end();
                break;
            case 'newuser':
                $this->subscribe('beforeTokenEmailExtended', 'setEmailResponseTo');
                $TokenUsersManager->actionNewUser();
                App()->end();
                break;
            case 'myaccount':
                $this->subscribe('beforeTokenEmailExtended', 'setEmailResponseTo');
                $TokenUsersManager->actionMyAccount();
                App()->end();
                break;
            case 'assignuser':
                $this->subscribe('beforeTokenEmailExtended', 'setEmailResponseTo');
                $srid = App()->getRequest()->getParam("srid");
                if (empty($srid) || strval(intval($srid)) !== $srid) {
                    $this->throwAdaptedException(400, $this->translate("Invalid response id"));
                }
                $TokenUsersManager->srid = $srid;
                $TokenUsersManager->actionAssignUser();
                App()->end();
                break;
            case 'sendmessage':
                $this->subscribe('beforeTokenEmailExtended', 'setEmailResponseTo');
                $srid = App()->getRequest()->getParam("srid");
                if (empty($srid) || strval(intval($srid)) !== $srid) {
                    $this->throwAdaptedException(400, $this->translate("Invalid response id"));
                }
                $TokenUsersManager->srid = $srid;
                $TokenUsersManager->actionSendMessage();
                App()->end();
                break;
            case 'restriction':
                $restrictedSurvey = App()->getRequest()->getParam("restrictedSurvey");
                if (empty($restrictedSurvey) || strval(intval($restrictedSurvey)) !== $restrictedSurvey) {
                    $this->throwAdaptedException(400, $this->translate("Invalid restrictedSurvey id"));
                }
                $TokenUsersManager->restrictedSurvey = $restrictedSurvey;
                $TokenUsersManager->actionRestriction();
                App()->end();
                break;
            case 'listparticipant':
                $TokenUsersManager->actionListParticipant();
                App()->end();
                break;
            default:
                $this->throwAdaptedException('400', $this->translate("Invalid function"));
                App()->end();
                break;
        }
    }

    /**
     * Set the current responseTo if needed
     * Usage of App()->getConfig
     * @todo : find a better way to set responseTo
     */
    public function setEmailResponseTo()
    {
        if (!App()->getConfig('TokenUsersmanagerEmailReponseTo')) {
            return;
        }
        if (!$this->get('setReplyTo', 'Survey', $this->getEvent()->get('survey'), '1')) {
            return;
        }
        $phpMailer = $this->getEvent()->get("PhpMailer");
        if (empty($phpMailer)) {
            return;
        }
        $phpMailer->addReplyTo(
            App()->getConfig('TokenUsersmanagerEmailReponseTo'),
            App()->getConfig('TokenUsersmanagerNameReponseTo')
        );
    }

    /**
     * Get the current token by URI or session
     * @param integer $surveyId
     * @param string $forcedToken
     * @return string|null
     */
    private function getCurrentToken($surveyId, $token = null)
    {
        if (empty($token)) {
            $token = App()->getRequest()->getParam('token');
        }
        $sessionTokens = App()->session['usersListManagementTokens'];
        if (is_null($sessionTokens)) {
            $sessionTokens = array();
        }
        if ($token && is_string($token)) {
            $sessionTokens[$surveyId] = $token;
            App()->session['usersListManagementTokens'] = $sessionTokens;
        }
        if (!empty($sessionTokens[$surveyId])) {
            return $sessionTokens[$surveyId];
        }
        return null;
    }

    /**
     * Add some views for this and other plugin
     */
    public function getPluginTwigPath()
    {
        $viewPath = dirname(__FILE__) . "/twig";
        $this->getEvent()->append('add', array($viewPath));
    }

    /**
     * register to needed event according to usability
     * @see event afterPluginLoad
     */
    public function afterPluginLoad()
    {
        if (!$this->getIsUsable()) {
            /* unregister */
        }
        // messageSource for this plugin:
        $messageSource = array(
            'class' => 'CGettextMessageSource',
            'cacheID' => get_class($this) . 'Lang',
            'cachingDuration' => 3600 * 24,
            'forceTranslation' => true,
            'useMoFile' => true,
            'basePath' => __DIR__ . DIRECTORY_SEPARATOR . 'locale',
            'catalog' => 'messages',// default from Yii
        );
        Yii::app()->setComponent(get_class($this) . 'Messages', $messageSource);
    }

    /**
     * Action on childrens
     * @param integer $surveyId
     * @return void
     */
    private function setChildrensAction($surveyId)
    {
        $oChildSurvey = new RelatedSurveyManagement\ChildrenSurveys($surveyId);
        $aChildrensSurveys = $oChildSurvey->getChildrensSurveys();
        foreach ($aChildrensSurveys as $qid => $childSurveyId) {
            if (!self::surveyHaveTokenTable($childSurveyId)) {
                $this->setChildrenAction($surveyId, $childSurveyId, App()->getRequest()->getPost('tokenAttributeSetChildrens'));
            } else {
                App()->setFlashMessage(sprintf($this->translate("Survey %s didn't have token table"), CHtml::encode($childSurveyId)), 'warning');
            }
        }
    }

    private function exportWithRestricted($surveyId)
    {
        if (!Permission::model()->hasSurveyPermission($surveyId, 'tokens', 'export')) {
            throw new CHttpException(403);
        }
        $oRecordSet = Yii::app()->db->createCommand()->from("{{tokens_$surveyId}} lt");
        $oRecordSet->order("lt.tid");
        $tokensresult = $oRecordSet->query();
        $headers = array(
            'tid',
            'firstname',
            'lastname',
            'email',
            'emailstatus',
            'token',
            'language',
            'validfrom',
            'validuntil',
            'invited',
            'reminded',
            'remindercount',
            'completed',
            'usesleft'
        );
        $attrfieldnames = getAttributeFieldNames($surveyId);
        $attrfielddescr = getTokenFieldsAndNames($surveyId, true);
        foreach ($attrfieldnames as $attr_name) {
            $headers[] = $attr_name . "<" . CHtml::encode($attrfielddescr[$attr_name]['description']) . ">";
        }
        $getRestricted = false;
        $getRestrictedChilds = array();
        /* current one */
        $tokenAttributeRestricted = $this->get('tokenAttributeRestricted', 'Survey', $surveyId);
        $useAttributeRestricted = $this->get('useAttributeRestricted', 'Survey', $surveyId);
        if ($tokenAttributeRestricted && $useAttributeRestricted) {
            $headers[] = "restricted" . "<" . CHtml::encode($this->translate("Restricted")) . ">";
            $getRestricted = true;
        }
        $oChildSurvey = new RelatedSurveyManagement\ChildrenSurveys($surveyId);
        $aChildrensSurveys = $oChildSurvey->getChildrensSurveys();
        foreach ($aChildrensSurveys as $qid => $childSurveyId) {
            $managementByParent = $this->get('managementByParent', 'Survey', $childSurveyId);
            $useAttributeRestricted = $this->get('useAttributeRestricted', 'Survey', $childSurveyId);
            if ($managementByParent && $useAttributeRestricted) {
                $headers[] = "restricted_[$childSurveyId}" . "<" . CHtml::encode(sprintf($this->translate("Restricted on %s"), $childSurveyId)) . ">";
                $getRestrictedChilds[] = $childSurveyId;
            }
        }
        header("Content-Disposition: attachment; filename=tokens_testricted_" . $surveyId . ".csv");
        header("Content-type: text/comma-separated-values; charset=UTF-8");
        header("Cache-Control: must-revalidate, no-store, no-cache");
        $output = fopen("php://output", 'w');
        fputcsv($output, $headers);
        while ($tokenrow = $tokensresult->read()) {
            $aToken = array(
                'tid' => $tokenrow['tid'],
                'firstname' => $tokenrow['firstname'],
                'lastname' => $tokenrow['lastname'],
                'email' => $tokenrow['email'],
                'emailstatus' => $tokenrow['emailstatus'],
                'token' => $tokenrow['token'],
                'language' => $tokenrow['language'],
                'validfrom' => $tokenrow['validfrom'],
                'validuntil' => $tokenrow['validuntil'],
                'invited' => $tokenrow['sent'],
                'reminded' => $tokenrow['remindersent'],
                'remindercount' => $tokenrow['remindercount'],
                'completed' => $tokenrow['completed'],
                'usesleft' => $tokenrow['usesleft'],
            );
            foreach ($attrfieldnames as $attr_name) {
                $aToken[] = $tokenrow[$attr_name];
            }
            $token = $tokenrow['token'];
            /* restricted self */
            if ($getRestricted) {
                if ($token) {
                    $oSridList = \TokenUsersListAndManagePlugin\models\TokenSridAccess::model()->findAll(
                        "sid = :sid and token = :token",
                        array(
                            ':sid' => $surveyId,
                            ':token' => $token,
                        )
                    );
                    $aToken[] = implode(",", \CHtml::listData($oSridList, 'srid', 'srid'));
                } else {
                    $aToken[] = "";
                }
            }
            foreach ($getRestrictedChilds as $ChildId) {
                if ($token) {
                    $oSridList = \TokenUsersListAndManagePlugin\models\TokenSridAccess::model()->findAll(
                        "sid = :sid and token = :token",
                        array(
                            ':sid' => $ChildId,
                            ':token' => $token,
                        )
                    );
                    $aToken[] = implode(",", \CHtml::listData($oSridList, 'srid', 'srid'));
                } else {
                    $aToken[] = "";
                }
            }
            fputcsv($output, $aToken);
        }
        fclose($output);
        App()->end();
    }
    /**
     * Action on single children with loop
     * @param integer $surveyId
     * @param integer $childSurveyId
     * @param string $action in ['settings','attributes','tokens']
     * @param boolean $createtable
     * @return void
     */
    private function setChildrenAction($surveyId, $childSurveyId, $action, $createtable = true)
    {
        if (!in_array($action, ['settings', 'attributes', 'tokens', 'onlytokens', 'existingtokens'])) {
            throw new CHttpException(400, $this->translate("Invalid action on children"));
        }
        static $surveyDone = array();
        if (isset($surveyDone[$childSurveyId])) {
            return;
        }
        if (!Permission::model()->hasSurveyPermission($childSurveyId, 'surveysettings', 'update')) {
            return;
        }
        if (!Permission::model()->hasSurveyPermission($childSurveyId, 'tokens', 'update')) {
            return;
        }
        /* settings */
        if ($action != 'onlytokens' || $action != 'onlytokens') {
            $this->set(
                "tokenAttributeGroup",
                $this->get('tokenAttributeGroup', 'Survey', $surveyId, ''),
                'Survey',
                $childSurveyId
            );
            $this->set(
                "tokenAttributeGroupManager",
                $this->get('tokenAttributeGroupManager', 'Survey', $surveyId, ''),
                'Survey',
                $childSurveyId
            );
            $this->set(
                "tokenAttributeRestricted",
                $this->get('tokenAttributeRestricted', 'Survey', $surveyId, ''),
                'Survey',
                $childSurveyId
            );
        }

        /* */
        if ($action == 'attributes' || $action == 'existingtokens') {
            Survey::model()->updateByPk(
                $childSurveyId,
                array(
                    'attributedescriptions' => Survey::model()->findByPk($surveyId)->getAttribute('attributedescriptions')
                )
            );
            if (Survey::model()->findByPk($surveyId)->hasTokensTable) {
                if (!Survey::model()->findByPk($childSurveyId)->hasTokensTable) {
                    if (!$createtable) {
                        return;
                    }
                    Token::createTable($childSurveyId);
                    App()->db->getSchema()->getTable("{{tokens_$childSurveyId}}", true);
                }
                $surveyTokenAttributes = Token::model($surveyId)->getAttributes();
                $childTokenAttributes = Token::model($childSurveyId)->getAttributes();
                $sTableName = "{{tokens_{$childSurveyId}}}";
                $newAttributes = array_diff_key($surveyTokenAttributes, $childTokenAttributes);
                $oldAttributes = array_diff_key($childTokenAttributes, $surveyTokenAttributes);
                foreach ($newAttributes as $attribute => $value) {
                    App()->db->createCommand(App()->db->getSchema()->addColumn($sTableName, $attribute, 'text'))->execute();
                }
                foreach ($oldAttributes as $attribute => $value) {
                    App()->db->createCommand(App()->db->getSchema()->dropColumn($sTableName, $attribute))->execute();
                }
                App()->db->getSchema()->getTable("{{tokens_$childSurveyId}}", true);
            }
        }
        if (!Survey::model()->findByPk($surveyId)->hasTokensTable) {
            return;
        }
        if ($action == 'tokens' || $action == 'onlytokens') {
            Token::model($childSurveyId)->deleteAll();
            $parentTable = "{{tokens_{$surveyId}}}";
            $chieldTable = "{{tokens_{$childSurveyId}}}";
            $surveyTokenAttributes = array_map(
                function ($attribute) {
                    return App()->db->quoteColumnName($attribute);
                },
                array_keys(Token::model($surveyId)->getAttributes())
            );
            $columnsToInsert = implode(',', $surveyTokenAttributes);
            switchMSSQLIdentityInsert("tokens_{$childSurveyId}", true);
            App()->db->createCommand("INSERT INTO $chieldTable ({$columnsToInsert}) SELECT * FROM $parentTable WHERE tid > 0")->execute();
            switchMSSQLIdentityInsert("tokens_{$childSurveyId}", false);
            App()->db->schema->getTable($chieldTable, true);
        }
        if ($action == 'existingtokens') {
            $chieldTable = "{{tokens_{$childSurveyId}}}";
            $childSurveyTokenAttributes = array_map(
                function ($attribute) {
                    return App()->db->quoteColumnName($attribute);
                },
                array_keys(Token::model($childSurveyId)->getAttributes())
            );
            unset($surveyTokenAttributes['tid']);
            $oParentTokens = Token::model($surveyId)->findAll("token <> '' and token IS NOT NULL");
            $columnsToSet = implode(',', $childSurveyTokenAttributes);
            foreach ($oParentTokens as $oParentToken) {
                $oToken = Token::model($childSurveyId)->findByToken($oParentToken->token);
                if (!$oToken) {
                    $oToken = Token::create($childSurveyId);
                    /* Find if tid exist, set it if not */
                    $oTidToken = Token::model($childSurveyId)->findByPk($oParentToken->tid);
                    if (empty($oTidToken)) {
                        switchMSSQLIdentityInsert("tokens_{$childSurveyId}", true);
                        $oToken->tid = $oParentToken->tid;
                        $oToken->token = $oParentToken->token;
                        $oToken->save(false, ['tid', 'token']);
                        switchMSSQLIdentityInsert("tokens_{$childSurveyId}", false);
                    } else {
                        $oToken = Token::create($childSurveyId);
                        $oToken->token = $oParentToken->token;
                        $oToken->save(false, ['token']);
                    }
                }
                $attributesToSet = array_intersect_key($oParentToken->getAttributes(), $surveyTokenAttributes);
                Token::model($childSurveyId)->updateAll(
                    $attributesToSet,
                    'token = :token',
                    [':token' => $oParentToken->token]
                );
            }
        }
        App()->setFlashMessage(sprintf($this->translate("Survey %s updated with success"), CHtml::encode($childSurveyId)));
        $surveyDone[$childSurveyId] = $childSurveyId;
        /* And the childs */
        $oChildSurvey = new RelatedSurveyManagement\ChildrenSurveys($childSurveyId);
        $aChildrensSurveys = $oChildSurvey->getChildrensSurveys();
        foreach ($aChildrensSurveys as $qid => $grandChildSurveyId) {
            $this->setChildrenAction($surveyId, $grandChildSurveyId, $action);
        }
    }

    /**
     * Create own page for JS and CSS
     * @return void
     */
    private function createSurveyPackage()
    {
        if (!App()->clientScript->hasPackage('SurveyTokenUsersListAndManage')) {
            App()->clientScript->addPackage('SurveyTokenUsersListAndManage', array(
                'basePath'    => 'TokenUsersListAndManagePlugin.assets.survey',
                'css'         => array(
                    'TokenUsersListAndManage.css'
                ),
                'js'          => array(
                    'TokenUsersListAndManage.js'
                ),
                'depends' => array(
                    'jquery',
                    'bootstrap',
                )
            ));
        }
    }

    /**
     * Check if we can use this plugin
     * @return boolean
     */
    private function getIsUsable()
    {
        return version_compare(Yii::app()->getConfig('versionnumber'), "3.10", ">=");
    }

    /**
     * get translation
     * @param string
     * @return string
     */
    private function translate($string)
    {
        return Yii::t('', $string, array(), get_class($this) . 'Messages');
    }

    /**
     * set and fix DB
     */
    private function setDb()
    {
        if (intval($this->get("dbVersion")) >= self::DBVERSION) {
            return;
        }
        if (!$this->api->tableExists($this, 'tokensrid')) {
            $this->api->createTable($this, 'tokensrid', array(
                'sid' => "int NOT NULL",
                'token' => "string(100) NOT NULL DEFAULT ''",
                'srid' => "int not NULL",
            ));
            $tableName = $this->api->getTable($this, 'tokensrid')->tableName();
            App()->getDb()->createCommand()->addPrimaryKey('tokensrid_sidtokensrid', $tableName, 'sid,token,srid');
            $this->set("dbVersion", 1);
        }
        $this->set("dbVersion", self::DBVERSION);
    }

    /**
     * Get final settings
     * Start by survey, by globak and finally by default
     * @param integer $surveyId
     * @param string $param
     * @return mixed
     */
    private function getSetting($surveyId, $param)
    {
        return \TokenUsersListAndManagePlugin\Utilities::getSetting($surveyId, $param);
    }

    /**
     * Check if a survey have a token table
     * Shortcut to tableExists in common_helper
     * @var interger $surveyId
     * @return boolean
     */
    private static function surveyHaveTokenTable($surveyId)
    {
        return tableExists('token_' . $surveyId);
    }
}
