/**
 * Action for TokenUsersListAndManage
 * @author Denis Chenu
 * @licence MIT
 * @version 1.5.2
 */
var TokenUsersListAndManage = {
    GlobalOptions : { },
    options : { },
    language : { },
    init : function () {
        if($("form").length) {
            $("#modal-tulam").insertAfter("form");
        }
        $(document).on("ajax:updated","#users-grid",function(event){
            $(this).find('[data-toggle="tooltip"]').tooltip();
        });
        /* edit part */
        $(document).on("hidden.bs.modal","#modal-tulam", function(e) {
            $("#modal-tulam .modal-body").html("");
            $("#modal-tulam .modal-title").html("");
            $("a[data-modal='tulam-form']").removeClass("active");
        });
        $(document).on("hide.bs.modal","#modal-tulam", function(e) {
            if($(this).data("update-users-grid")) {
                if($("#users-grid").length) {
                    $.fn.yiiGridView.update("users-grid");
                }
            }
            $(this).data("update-users-grid",false);
        });
        $(document).on("click", "a[data-modal='tulam-form']", function(event) {
            if(!$("#modal-tulam .modal-body").length) {
                return;
            }
            event.preventDefault();
            $(this).addClass("active");
            TokenUsersListAndManage.actionForm(event,$(this));
        });
        $(document).on("change", "#tulam_sendMessage", function(event) {
            $("#tulam_message").attr('disabled',!$(this).is(":checked"));
        });
        TokenUsersListAndManage.initRelatedCheckbox();
        TokenUsersListAndManage.initEmailConfirm();
        $(document).on("submit", "form.form-tulam", function(event) {
           $(this).addClass("form-submitted");
        });
    },
    /* checkbox hide/show */
    initRelatedCheckbox : function () {
        $("[data-related-checkbox]").each(function() {
            var element = $(this);
            var checkboxId = $(this).data("related-checkbox");
            if(!$("#"+checkboxId).prop('checked')) {
                $(this).hide();
            }
            $("#"+checkboxId).on('change', function () {
                if($("#"+checkboxId).prop('checked')) {
                    $(element).show();
                } else {
                    $(element).hide();
                }
            });
        });
        $("[data-related-checkbox-inverse]").each(function() {
            var element = $(this);
            var checkboxId = $(this).data("related-checkbox-inverse");
            if($("#"+checkboxId).prop('checked')) {
                $(this).hide();
            }
            $("#"+checkboxId).on('change', function () {
                if($("#"+checkboxId).prop('checked')) {
                    $(element).hide();
                } else {
                    $(element).show();
                }
            });
        });
    },
    initEmailConfirm : function () {
        if ($("#tokenattribute_email").val() != $("#tokenattribute_email").data("original")) {
            $(".tulam-email_confirm-group").removeClass("ls-js-hidden");
        }
        $(document).on("keyup", "#tokenattribute_email", function(event) {
            if ($("#tokenattribute_email").val() != $("#tokenattribute_email").data("original")) {
                $(".tulam-email_confirm-group").removeClass("ls-js-hidden");
            } else {
                $(".tulam-email_confirm-group").addClass("ls-js-hidden");
            }
        });
        $(document).on("ajax:update", "#modal-tulam .modal-body", function(event) {
            if ($("#tokenattribute_email").val() != $("#tokenattribute_email").data("original")) {
                $(".tulam-email_confirm-group").removeClass("ls-js-hidden");
            } else {
                $(".tulam-email_confirm-group").addClass("ls-js-hidden");
            }
        });
    },
    confirmForm : function (event,that) {
        event.preventDefault();
        var actionUrl = $(that).attr('href');
        var modalTitle = $(that).text();
        if(!actionUrl) {
            return;
        }
        $("#modal-tulam .modal-footer").hide();
        $.ajax({
            async: true,
            url : actionUrl,
            type : 'GET',
            dataType : 'html',
        })
        .done (function(htmlData, textStatus, jqXHR) {
            $("#modal-tulam .modal-body").html(htmlData);
            $("#modal-tulam").data("update-users-grid",true);
            $("#modal-tulam").modal('show');
            $("#modal-tulam .modal-body form button[name='cancel']").on("click" ,function(event){
                event.preventDefault();
                $("#modal-tulam").modal('hide');
            });
            $("#modal-tulam .modal-body form button[name!='cancel']").on("click" ,function(event){
                event.preventDefault();
                $(this).closest('form').append(
                    $("<input type='hidden'>").attr( { 
                        name: $(this).attr('name'), 
                        value: $(this).attr('value') })
                );
                var postData = $(this).closest('form').serialize();
                $.ajax({
                    async: true,
                    url : actionUrl,
                    data : postData,
                    type : 'POST',
                    dataType : 'html',
                })
                .done (function(htmlData, textStatus, jqXHR) { 
                    $("#modal-tulam .modal-body").html(htmlData);
                    $("#modal-tulam .modal-body").trigger('ajax:update').trigger('html:updated');
                    $("#modal-tulam").data("update-users-grid",true);
                })
                .fail (function(jqXHR, textStatus, errorThrown) {
                    var errorMessage = jqXHR.responseText;
                    $("#modal-tulam .modal-body").html(errorMessage);
                    $("#modal-tulam .modal-body").trigger('ajax:update').trigger('html:updated');
                    $("#modal-tulam").modal('show');
                    
                })
                .always (function(jqXHROrData, textStatus, jqXHROrErrorThrown) {
                    // Nothing
                });
            });

        })
        .fail (function(jqXHR, textStatus, errorThrown) {
            var errorMessage = jqXHR.responseText;
            $("#modal-tulam .modal-body").html(errorMessage);
            $("#modal-tulam .modal-body").trigger('ajax:update').trigger('html:updated');
            $("#modal-tulam").modal('show');
        })
        .always (function(jqXHROrData, textStatus, jqXHROrErrorThrown) {
            $("#modal-tulam .modal-title").html(modalTitle);
        });
    },
    actionForm : function (event,that) {
        event.preventDefault();
        var actionUrl = $(that).attr('href');
        var modalTitle = $(that).text();
        var modalUpdateusersgrid = $(that).data("updateusersgrid");
        if(!actionUrl) {
            return;
        }
        $("#modal-tulam .modal-footer").show();
        $.ajax({
            async: true,
            url : actionUrl,
            type : 'GET',
            dataType : 'html',
        })
        .done (function(htmlData, textStatus, jqXHR) { 
            $("#modal-tulam .modal-body").html(htmlData);
            $("#modal-tulam .modal-body").trigger('ajax:update').trigger('html:updated');
            $("#modal-tulam").modal('show');
            if(modalUpdateusersgrid) {
                $("#modal-tulam").data("update-users-grid",true);
            }
            TokenUsersListAndManage.manageModalFormSubmit(actionUrl);
        })
        .fail (function(jqXHR, textStatus, errorThrown) {
            var errorMessage = jqXHR.responseText;
            $("#modal-tulam .modal-body").html(errorMessage);
            $("#modal-tulam .modal-body").trigger('ajax:update').trigger('html:updated');
            $("#modal-tulam").modal('show');
        })
        .always (function(jqXHROrData, textStatus, jqXHROrErrorThrown) {
            $("#modal-tulam .modal-title").html(modalTitle);
        });
    },
    closeDialog : function () {
        $("#modal-tulam").modal('hide');
    },
    manageModalFormSubmit: function (actionUrl) {
        $("#modal-tulam .modal-body form").on("submit" ,function(event){
            event.preventDefault();
            var arrayDatas = $(this).serializeArray();
            var postData = $.param(arrayDatas);
            $.ajax({
                async: true,
                url : actionUrl,
                data : postData,
                type : 'POST',
                dataType : 'html',
            })
            .done (function(htmlData, textStatus, jqXHR) { 
                $("#modal-tulam .modal-body").html(htmlData);
                $("#modal-tulam .modal-body").trigger('ajax:update').trigger('html:updated');
                TokenUsersListAndManage.manageModalFormSubmit(actionUrl);
            })
            .fail (function(jqXHR, textStatus, errorThrown) {
                var errorMessage = jqXHR.responseText;
                $("#modal-tulam .modal-body").html(errorMessage);
                $("#modal-tulam .modal-body").trigger('ajax:update').trigger('html:updated');
                $("#modal-tulam").modal('show');
            })
            .always (function(jqXHROrData, textStatus, jqXHROrErrorThrown) {
                // Nothink currently
            });
        });
    },
};
