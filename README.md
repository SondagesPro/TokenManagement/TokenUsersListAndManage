# TokenUsersListAndManage

Token Users management by token manager user (see [responseListAndManage](https://gitlab.com/SondagesPro/managament/responseListAndManage))

This plugin is done to be used with responseListAndManage plugin, offer a complete system to manage user by token manager.

## Quick usage

If survey have token table : you can allow some particpant to manage other participants. Participant to contact other particpants on same group, or update some of account data.

Group and manager rights used existing attributes, you can use Display text qustion to show related button inside survey.

This plugin is used by responseListAndManage to replace the group and manager.

This plugin is used too  with reloadAnyReponse when checking right when reload.

An attribute can be used for restrict some user to edit specific response.

### Emails URL and KEYWORD

When sending emails :
- {CURRENTURL} include current response id by default if reloadAnyResponse allow group edition
- {SURVEYURL} replaced by {CURRENTURL} if used this plugin
- {BASEURL} is the default SURVEYURL not updated
- {PARENTURL} if you have RelatedSurveyManagement plugins and current survey have a parent it's the uirl to the final parent
- {MESSAGE} the message set by the manager or participant
- {SURVEYDESCRIPTION} replaced by {MESSAGE}
- {ADMINNAME} Replace by current user name
- {ADMINEMAIL} Replace by current user email

## Extend this plugin

This plugin have way to extend form of user to allow to create your own system. 

### beforeTULAMSurveySettings

Allow to add setting inside this plugin setting on the ame way than [core beforeSurveySettings](https://manual.limesurvey.org/BeforeSurveySettings).

- Input
    - `survey` the id of the current survey
    - `surveyId` the id of the current survey
    - `aQuestionList` The question list to be used in dropdown
    - `aTokensAttributeList` The token attribute list to be used in droipdown
- Possible output
    - `settings` : array of settings for survey, same format than [core beforeSurveySettings](https://manual.limesurvey.org/BeforeSurveySettings).

### newTULAMSurveySettings

Allow to save settings, same way than [core newSurveySettings](https://manual.limesurvey.org/NewSurveySettings).

## Home page & Copyright
- HomePage <https://extensions.sondages.pro/>
- Code repository <https://gitlab.com/SondagesPro/TokenManagement/TokenUsersListAndManage>
- Copyright © 2020-2024 Denis Chenu <www.sondages.pro>
- Copyright © 2020 OECD (Organisation for Economic Co-operation and Development ) <www.oecd.org>
- [![Donate](https://liberapay.com/assets/widgets/donate.svg)](https://liberapay.com/SondagesPro/) : [Donate on Liberapay](https://liberapay.com/SondagesPro/)

Distributed under [GNU AFFERO PUBLIC LICENSE Version 3](https://gnu.org/licenses/agpl-3.0.txt) licence
