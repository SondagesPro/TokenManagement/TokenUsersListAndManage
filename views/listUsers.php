<?php

$this->widget('ext.LimeGridView.LimeGridView', array(
    'dataProvider'=> $model->search(),
    'columns' => $columns,
    'filter' => $model,
    'itemsCssClass' => 'table table-condensed table-striped table-bordered',
    'id'            => 'users-grid',
    'ajaxUpdate'    => 'users-grid',
    'htmlOptions'   => array('class'=>'grid-view table-responsive'),
    'ajaxType'      => 'POST',
    'afterAjaxUpdate' => "js:function(id,data){ $('#'+id).trigger('ajax:updated'); }",
));
