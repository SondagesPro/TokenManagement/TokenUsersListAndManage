<?php

/**
 * get settings
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2020-2024 Denis Chenu <http://www.sondages.pro>
 * @license AGPL v3
 * @version 0.33.1
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

namespace TokenUsersListAndManagePlugin;

use App;

class Settings
{
    /* @var array DefaultSettings : the default settings if not set by DB @see reloadAnyResponse::settings */
    const DefaultSettings = array(
        'active' => 1,
        'managementByParent' => 0,
        'useAttributeRestricted' => 1,
        'allowAllAttributeRestricted' => 1,
        'listSurveyRestrictedByParent' => 0,
        'userManagementAccess' => 'admin',
        'allowSetManager' => 'admin',
        'allowCreateUser' => 'admin',
        'allowEditAccount' => 'all',
        'allowGiveRight' => 'user',
        'allowSendEmail' => 'all',
        /* Core attribute usage */
        'tokenFirstnameUsage' => 'mandatory',
        'tokenLastnameUsage' => 'mandatory',
        'tokenEmailUsage' => 'mandatory',
        'tokenTokenShow' => 'hide',
        'allowNoMessageCreate' => 0,
        'forcedSendMessageCreate' => 0,
        /* Set to empty string to fill instance */
        'tokenAttributeGroup' => false,
        'tokenAttributeGroupManager' => false,
        'tokenAttributeRestricted' => false,
        'keepMandatoryAttributeWhenDelete' => 0,
    );

    /* @var integer current setting version for PHP */
    const SettingsVersion = 1;

    /**
     * Singleton
     * @var self
     */
    private static $instance = null;

    /**
     * @var null|integre
     */
    private $pluginid;

    /**
     * @var []
     */
    private $settings = array();

    /**
     * constructor
     * @param integer survey id
     */
    public function __construct()
    {
        $oPlugin = \Plugin::model()->find(
            "name = :name",
            array(":name" => 'TokenUsersListAndManage')
        );
        $this->pluginid = $oPlugin->id;
    }

    /**
     * Get the singleton
     * @param integer survey id
     * return self
     */
    public static function getInstance()
    {
        if ((null === self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * Get a DB setting from a plugin
     * @param integer survey id
     * @param string setting name
     * @return mixed
     */
    public function getSetting($surveyId, $sSetting)
    {
        if (isset($this->settings[$surveyId][$sSetting])) {
            return $this->settings[$surveyId][$sSetting];
        }

        /* This survey setting */
        $oSetting = \PluginSetting::model()->find(
            'plugin_id = :pluginid AND ' . App()->getDb()->quoteColumnName('key') . ' = :key AND model = :model AND model_id = :surveyid',
            array(
                ':pluginid' => $this->pluginid,
                ':key' => $sSetting,
                ':model' => 'Survey',
                ':surveyid' => $surveyId,
            )
        );
        if (!empty($oSetting)) {
            $value = json_decode($oSetting->value, true);
            if ($value !== '') {
                $this->settings[$surveyId][$sSetting] = $value;
                return $value;
            }
        }
        /* empty (string) : then get by default setting */
        $oSetting = \PluginSetting::model()->find(
            'plugin_id = :pluginid AND ' . App()->getDb()->quoteColumnName('key') . ' = :key AND model IS NULL',
            array(
                ':pluginid' => $this->pluginid,
                ':key' => $sSetting,
            )
        );

        if (!empty($oSetting)) {
            $value = json_decode($oSetting->value, true);
            if ($value !== '') {
                $this->settings[$surveyId][$sSetting] = $value;
                return $value;
            }
        }

        /* empty (string) : then get setting by default */
        if (isset(self::DefaultSettings[$sSetting])) {
            $this->settings[$surveyId][$sSetting] = self::DefaultSettings[$sSetting];
            return self::DefaultSettings[$sSetting];
        }

        /* Then finally : null */
        return null;
    }

    /**
     * Get all survey setting from this plugin and return sureyid
     * @param string $setting setting name
     * @param string $value setting value
     * @return array
     */
    public function getSurveyIdWithValue($setting, $value)
    {
        $oSetting = \PluginSetting::model()->findAll(
            'plugin_id = :pluginid AND ' . App()->getDb()->quoteColumnName('key') . ' = :key AND model = :model AND ' . App()->getDb()->quoteColumnName('value') . ' = :value',
            array(
                ':pluginid' => $this->pluginid,
                ':key' => $setting,
                ':model' => 'Survey',
                ':value' => json_encode($value),
            )
        );
        return \Chtml::listData($oSetting, 'model_id', 'model_id');
    }

    /**
     * Fix settings version of surveyid
     * @param integer $surveyId
     * @return void
     **/
    public function updateSettingsVersion($surveyId)
    {
        if (isset($this->settings[$surveyId]['settingsversion']) && $this->settings[$surveyId]['settingsversion'] >= self::SettingsVersion) {
            return;
        }
        $oSettingVersion = \PluginSetting::model()->find(
            'plugin_id = :pluginid AND ' . App()->getDb()->quoteColumnName('key') . ' = :key AND model = :model AND model_id = :surveyid',
            array(
                ':pluginid' => $this->pluginid,
                ':key' => 'settingsversion',
                ':model' => 'Survey',
                ':surveyid' => $surveyId,
            )
        );
        $this->settings[$surveyId]['settingsversion'] = 0;
        if (!empty($oSettingVersion)) {
            $value = json_decode($oSettingVersion->value, true);
            $this->settings[$surveyId]['settingsversion'] = $value;
            if ($this->settings[$surveyId]['settingsversion'] >= self::SettingsVersion) {
                return;
            }
        }
        if ($this->settings[$surveyId]['settingsversion'] < 1) {
            /* Update needed settings */
            $toUpdateSettings = array(
                'tokenFirstnameUsageShow' => array(
                    'fromKey' => 'tokenFirstnameMandatory',
                    'fromValue' => '0',
                    'toKey' => 'tokenFirstnameUsage',
                    'toValue' => 'show',
                ),
                'tokenFirstnameUsageMandatory' => array(
                    'fromKey' => 'tokenFirstnameMandatory',
                    'fromValue' => '1',
                    'toKey' => 'tokenFirstnameUsage',
                    'toValue' => 'mandatory',
                ),
                'tokenLastnameUsageShow' => array(
                    'fromKey' => 'tokenLastnameMandatory',
                    'fromValue' => '0',
                    'toKey' => 'tokenLastnameUsage',
                    'toValue' => 'show',
                ),
                'tokenLastnameUsageMandatory' => array(
                    'fromKey' => 'tokenLastnameMandatory',
                    'fromValue' => '1',
                    'toKey' => 'tokenLastnameUsage',
                    'toValue' => 'mandatory',
                ),
            );
            foreach ($toUpdateSettings as $toUpdateSetting) {
                \PluginSetting::model()->updateAll(
                    ['key' => $toUpdateSetting['toKey'], "value" => json_encode($toUpdateSetting['toValue'])],
                    "plugin_id = :pluginid AND  " . App()->getDb()->quoteColumnName('key') . " = :key AND model = :model AND model_id = :surveyid and value = :value",
                    array(
                        ':pluginid' => $this->pluginid,
                        ':key' => $toUpdateSetting['fromKey'],
                        ':model' => 'Survey',
                        ':surveyid' => $surveyId,
                        ':value' => json_encode($toUpdateSetting['fromValue']),
                    )
                );
            }

            $this->settings[$surveyId]['settingsversion'] = 1;
        }
        $this->settings[$surveyId]['settingsversion'] = self::SettingsVersion;
    }
}
