<?php
/**
 * This file is part of reloadAnyResponse plugin
 * @version 0.35.1
 */

namespace TokenUsersListAndManagePlugin\models;
use Yii;
use CActiveRecord;
use CDbCriteria;
use Response;

class TokenSridAccess extends CActiveRecord
{
    /**
     * Class TokenUsersListAndManagePlugin\models\TokenSridAccess
     *
     * @property integer $sid survey
     * @property string $token : token
     * @property integer $srid : response id
    */

    /** @inheritdoc */
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }
    /** @inheritdoc */
    public function tableName()
    {
        return '{{tokenuserslistandmanage_tokensrid}}';
    }

    /** @inheritdoc */
    public function primaryKey()
    {
        return array('sid', 'token', 'srid');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $aRules = array(
            array('sid', 'required'),
            array('srid', 'required'),
            array('token', 'required'),
            array('sid,srid', 'numerical', 'integerOnly'=>true),
            array('srid', 'unique', 'criteria'=>array(
                    'condition'=>'sid=:sid and token=:token',
                    'params'=>array(':sid'=>$this->sid, ':token'=>$this->token )
                ),
                'message'=>sprintf("Srid : '%s' already set for '%s' with '%s'.",$this->srid,$this->sid,$this->token),
            ),
        );
        return $aRules;
    }

    /**
     * Set the restrictions to the list of response for token
     * If replace token is set : replace token in response too.
     * @param integer $surveyId
     * @param integer[] $responseIds
     * @param string token
     * @param string replaceToken
     * @return void
     */
    public static function setRestrictionsToSurvey($surveyId, $reponseIds, $token, $tokenreplace = null)
    {
        $criteria = new \CDbCriteria;
        $criteria->compare('sid', $surveyId);
        $criteria->compare('token', $token);
        $criteria->compare('srid', ">0");
        $criteria->addNotInCondition('srid', $reponseIds);
        self::model()->deleteAll($criteria);
        foreach($reponseIds as $srid) {
            if($srid === 0) {
                continue;
            }
            $oTokenSridAccess = self::model()->findByPk(array(
                'sid' => $surveyId,
                'token' => $token,
                'srid' => $srid
            ));
            if(!$oTokenSridAccess) {
                $oTokenSridAccess = new self;
                $oTokenSridAccess->sid = $surveyId;
                $oTokenSridAccess->token = $token;
                $oTokenSridAccess->srid = $srid;
                $oTokenSridAccess->save();
            }
        }
        if (!$tokenreplace) {
            return;
        }
        $responseCriteria = new CDbCriteria;
        $responseCriteria->compare('token', $token);
        $responseCriteria->addNotInCondition('id', $reponseIds);
        $updated = Response::model($surveyId)->updateAll(
            array('token' => $tokenreplace),
            $responseCriteria
        );
    }

    /**
     * Get list of id for a survey and a token
     * @param integer $surveyId
     * @parm string $token
     * @return integer[]
     */
    public static function getReponseForToken($surveyId, $token)
    {
        /* this srid */
        $criteria = new \CDbCriteria;
        $criteria->compare('sid', $surveyId);
        $criteria->compare('token', $token);
        $criteria->compare('srid', ">0");
        $aSridAccess = \Chtml::listData(self::model()->findAll($criteria), 'srid', 'srid');
        /* response srid */
        $criteria = new \CDbCriteria;
        $criteria->select = array('id', 'token');
        $criteria->compare('token', $token);
        $aResponses = \Chtml::listData(\Response::model($surveyId)->findAll($criteria), 'id', 'id');
        return array_unique(array_merge(
            $aSridAccess,
            $aResponses
        ));
    }

    /**
     * get if an user have all access
     * @param integer $surveyId
     * @parm string $token
     * @return boolean
     */
    public static function haveAllAccess($surveyId, $token)
    {
        if (!\TokenUsersListAndManagePlugin\Utilities::getSetting($surveyId, 'allowAllAttributeRestricted')) {
            return false;
        }
        $oTokenGlobalSridAccess = self::model()->count(
            'sid = :sid AND token = :token and srid = :srid',
            array(
                ':sid' => $surveyId,
                ':token' => $token,
                ':srid' => 0,
            )
        );
        return boolval($oTokenGlobalSridAccess);
    }

    /**
     * Set all access for a user
     * @param integer $surveyId
     * @param string $token
     * @return self::model|null
     */
    public static function giveAllAccess($surveyId, $token)
    {
        if (!\TokenUsersListAndManagePlugin\Utilities::getSetting($surveyId, 'allowAllAttributeRestricted')) {
            return null;
        }
        $oTokenGlobalSridAccess = self::model()->findByPk(array(
            'sid' => $surveyId,
            'token' => $token,
            'srid' => 0,
        ));
        if ($oTokenGlobalSridAccess) {
            return $oTokenGlobalSridAccess;
        }
        $oTokenGlobalSridAccess = new self;
        $oTokenGlobalSridAccess->sid = $surveyId;
        $oTokenGlobalSridAccess->token = $token;
        $oTokenGlobalSridAccess->srid = 0;
        $oTokenGlobalSridAccess->save();
        return $oTokenGlobalSridAccess;
    }

    /**
     * Set all access for a user
     * @param integer $surveyId
     * @parm string $token
     * @return interger (number of line return by PDO object)
     */
    public static function deleteAllAccess($surveyId, $token)
    {
        return self::model()->deleteAll(
            "sid =:sid and token = :token",
            array(
                'sid' => $surveyId,
                'token' => $token,
            )
        );
    }

}
