<?php
/**
 * This file is part of TokenUsersListAndManage plugin
 * @see Token
 * @version 0.34.4
 */
/** Disable namespace ;: seem unusable with bootstrap.widgets.TbGridView */
//~ namespace TokenUsersListAndManage\models;
//~ use Yii;
//~ use LSActiveRecord;
//~ use Survey;
//~ use CDbCriteria;
//~ use CActiveDataProvider;

class TokenManaged extends LSActiveRecord
{

    /** @var int $sid */
    protected static $sid = 0;
    /* @var int $originalSid */
    public $originalSid = 0;

    /** @var Survey $survey */
    protected static $survey;

    /* @var string The token attribute group : hidden (except for admin ?) */
    public static $attributeGroup;
    /* @var string The token attribute manager : show as boolean */
    public static  $attributeGroupManager;
    /* @var string The token attribute restricted : show as boolean */
    public static  $attributeRestricted;
    /* @var string[] Attribute to show as boolean */
    public static  $attributesBoolean;
    /* @var string[] Attribute to show as select (get text from answer) */
    public static  $attributesSelect;
    /* @var string[] Attribute to show as select (get text from answer), restricted by token */
    public static  $attributesSelectRestrictToken;
    /* @var boolean Keep mandatory attribute when delete */
    public static  $keepMandatoryAttributeWhenDelete;

    /* @var boolean is active */
    public $active = '';

    /* @var string email_confirm */
    public $emailconfirm = '';

    /* @var string the current token */
    public $currentToken;

    /* @var string the plugin name for url */
    public $pluginName;

    /* @var boolean[] the avaialble buttons actionUrls */
    public $availableButtons = array(
        'edit' => true,
        'email' => true,
        'invalidemail' => true,
        'disable' => true,
        'empty' => true,
    );

    /**
     * @inheritdoc
     * @return SurveyDynamic
     */
    public static function model($sid = null) {
        $sid = intval($sid);
        $survey = Survey::model()->findByPk($sid);
        self::sid($survey->sid);
        self::$survey = $survey;
        self::$attributeGroup = \TokenUsersListAndManagePlugin\Utilities::getTokenAttributeGroup($sid);
        self::$attributeGroupManager = \TokenUsersListAndManagePlugin\Utilities::getTokenAttributeGroupManager($sid);
        self::$attributeRestricted = \TokenUsersListAndManagePlugin\Utilities::getSetting($sid, 'tokenAttributeRestricted');
        self::$attributesBoolean = (array) \TokenUsersListAndManagePlugin\Utilities::getSetting($sid, 'attributesBoolean');
        self::$attributesSelect = (array) \TokenUsersListAndManagePlugin\Utilities::getSetting($sid, 'attributesSelect');
        self::$attributesSelectRestrictToken = (array) \TokenUsersListAndManagePlugin\Utilities::getSetting($sid, 'attributesSelectRestrictToken');
        self::$keepMandatoryAttributeWhenDelete = (array) \TokenUsersListAndManagePlugin\Utilities::getSetting($sid, 'keepMandatoryAttributeWhenDelete');

        /** @var self $model */
        $model = parent::model(__CLASS__);
        return $model;
    }

    /** @inheritdoc */
    public function init() {
        $this->originalSid = self::$sid;
        $this->firstname = "";
        $this->lastname = "";
        $this->email = "";
        $this->emailstatus = "OK";
    }
    /** @inheritdoc */
    public function afterFind()
    {
        $this->active = $this->getActive();
        return parent::afterFind();
    }

    /**
     * Sets the survey ID for the next model
     *
     * @static
     * @access public
     * @param int $sid
     * @return void
     */
    public static function sid($sid)
    {
        self::$sid = (int) $sid;
    }

    /** @inheritdoc */
    public function tableName()
    {
        return '{{tokens_'.self::$sid.'}}';
    }

    /** @inheritdoc */
    public function primaryKey()
    {
        return 'tid';
    }

    /**
     * @inheritdoc
     */
    public function setScenario($scenario) {
        parent::setScenario($scenario);
    }

    /**
     * @inheritdoc
     * @see \Token::model()->rules
     **/
    public function rules()
    {
        $Token = \Token::model(self::$sid);
        return $Token->rules();
    }

    /** @inheritdoc */
    public function relations()
    {
        return array(
            'survey' => array(self::BELONGS_TO, 'Survey', array(), 'condition'=>'sid='.self::$sid, 'together' => true),
        );
    }

    /**
     * @param string $token
     * @return \TokenManaged
     */
    public function findByToken($token)
    {
        return $this->findByAttributes(array(
            'token' => $token
        ));
    }

    /**
     * @return CActiveDataProvider
     */
    public function search()
    {
        $pageSize = Yii::app()->user->getState('TokenUsersListAndManagePageSize', Yii::app()->params['defaultPageSize']);
        /* Function from common_helper */
        $now = dateShift(date("Y-m-d H:i:s"), "Y-m-d H:i:s", Yii::app()->getConfig("timeadjust"));
        $this->setScenario('search');
        $criteria = new CDbCriteria;
        $sort = $this->getSort();
        $sort->defaultOrder = 'tid ASC';

        $criteria = new LSDbCriteria;
        $criteria->compare("emailstatus", "<>disable");

        if($this->active == 'Y') {
            $criteria->addCondition("COALESCE(validuntil, '$now') >= '$now' AND COALESCE(validfrom, '$now') <= '$now'");
        }
        if($this->active == 'N') {
            $criteria->addCondition("COALESCE(validuntil, '$now') < '$now' OR COALESCE(validfrom, '$now') > '$now'");
        }
        /* Admin user didn't have token */
        if($this->currentToken) {
            $this->scopeByGroupOf();
            $criteria->compare('token', "<>" . $this->currentToken, false);
        }
        $criteria->compare('tid', $this->tid, false);
        $criteria->compare('firstname', $this->firstname, true);
        $criteria->compare('lastname', $this->lastname, true);
        $criteria->compare('email', $this->email, true);

        foreach($this->getTokenAttributes() as $attribute => $information) {
            if ($attribute == self::$attributeGroup && $this->currentToken) {
                continue;
            }
            if ($information['show_register'] == "Y" &&
                (in_array($attribute, self::$attributesSelect) || in_array($attribute, self::$attributesSelectRestrictToken))
            ) {
                $criteria->compare($attribute, $this->$attribute, true);
                continue;
            }
            if ($attribute == self::$attributeGroupManager) {
                if ($this->getAttribute($attribute) == "Y") {
                    $criteria->addCondition("t.{$attribute} IS NOT NULL AND t.{$attribute} <> '' AND t.{$attribute} <> '0'");
                }
                if ($this->getAttribute($attribute) == "N") {
                    $criteria->addCondition("t.{$attribute} IS NULL OR t.{$attribute} = '' OR t.{$attribute} = '0'");
                }
                continue;
            }
            if ($attribute == self::$attributeRestricted) {
                if ($this->getAttribute($attribute) == "Y") {
                    $criteria->addCondition("t.{$attribute} IS NOT NULL AND t.{$attribute} <> '' AND t.{$attribute} <> '0'");
                }
                if ($this->getAttribute($attribute) == "N") {
                    $criteria->addCondition("t.{$attribute} IS NULL OR t.{$attribute} = '' OR t.{$attribute} = '0'");
                }
                continue;
            }
            if( $information['show_register'] != "Y") {
                continue;
            }
            if (in_array($attribute, self::$attributesBoolean)) {
                if ($this->getAttribute($attribute) == "Y") {
                    $criteria->addCondition("t.{$attribute} IS NOT NULL AND t.{$attribute} <> '' AND t.{$attribute} <> '0'");
                }
                if ($this->getAttribute($attribute) == "N") {
                    $criteria->addCondition("t.{$attribute} IS NULL OR t.{$attribute} = '' OR t.{$attribute} = '0'");
                }
                continue;
            }
        }

        /* Add bad deletion */
        $badDeletionCriteria = new LSDbCriteria;
        $badDeletionCriteria->compare("emailstatus", "disable");
        $badDeletionCriteria->addCondition("COALESCE(validuntil, '$now') >= '$now' AND COALESCE(validfrom, '$now') <= '$now'");
        $criteria->mergeWith($badDeletionCriteria, 'OR');

        $dataProvider = new CActiveDataProvider('TokenManaged', array(
            'sort' => $sort,
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => $pageSize,
            ),
        ));
        return $dataProvider;
    }

    /**
     * Scope for group restriction
     * @param string $token use $this->currentToken else
     * @param string $excludetoken tokens to be exluded from restriction (allow to get specific token)
     * @return self
     */
    public function scopeByGroupOf($token = null, $excludetoken = null)
    {
        if (empty($token)) {
            $token = $this->currentToken;
        }
        if($token) {
            $tokenList = \TokenUsersListAndManagePlugin\Utilities::getTokensList(self::$sid, $token);
            if ($excludetoken) {
                unset($tokenList[$excludetoken]);
            }
            if (!empty($tokenList)) {
                $criteria = new CDbCriteria();
                $criteria->addInCondition(
                    'token',
                    $tokenList
                );
                $this->getDbCriteria()->mergeWith($criteria);
            }
        }
        return $this;
    }

    /**
     * get columns for grid
     * @return array
     */
    public function getGridColumns()
    {
        $aColumns = array();
        $tokenAttributes = $this->getTokenAttributes();
        $actionsAvailable = array_keys(array_filter($this->availableButtons));
        $template = implode(
            array_map(
                function($button) { return '{'.$button.'}'; },
                $actionsAvailable
            )
        );

        $aColumns['buttons']=array(
            'header' => gT('Action'),
            'class'=>'bootstrap.widgets.TbButtonColumn',
            'template'=>$template,
            'htmlOptions' => array('class' => 'button-column'),
            'buttons'=> $this->getGridButtons('responses-grid'),
            //'filterInputOptions' => array('class'=>'form-control input-sm filter-id'),
        );
        if (\TokenUsersListAndManagePlugin\Utilities::getSetting(self::$sid, 'tokenFirstnameUsage') != 'hidden') {
            $aColumns['firstname']=array(
                'header' => gT('First name'),
                'name' => 'firstname',
                'htmlOptions' => array('class' => 'data-column column-firstname'),
                'filterInputOptions' => array('class'=>'form-control input-sm filter-firstname'),
            );
        }
        if (\TokenUsersListAndManagePlugin\Utilities::getSetting(self::$sid, 'tokenLastnameUsage') != 'hidden') {
            $aColumns['lastname']=array(
                'header' => gT('Last name'),
                'name' => 'lastname',
                'htmlOptions' => array('class' => 'data-column column-lastname'),
                'filterInputOptions' => array('class'=>'form-control input-sm filter-lastname'),
            );
        }
        if (\TokenUsersListAndManagePlugin\Utilities::getSetting(self::$sid, 'tokenEmailUsage') != 'hidden') {
            $aColumns['email']=array(
                'header' => gT('Email address'),
                'name' => 'email',
                'htmlOptions' => array('class' => 'data-column column-email'),
                'filterInputOptions' => array('class'=>'form-control input-sm filter-email'),
            );
        }
        if (\TokenUsersListAndManagePlugin\Utilities::getSetting(self::$sid, 'tokenTokenShow') != 'hide') {
            $aColumns['token']=array(
                'header' => gT('Token'),
                'name' => 'token',
                'type' => 'raw',
                'value'=> 'TokenManaged::formatToken($data)',
                'htmlOptions' => array('class' => 'data-column column-token'),
                'filterInputOptions' => array('class'=>'form-control input-sm filter-token'),
            );
        }
        /* specific attribute */
        foreach($tokenAttributes as $attribute => $information) {
            if ($attribute == self::$attributeGroup && $this->currentToken) {
                continue;
            }
            if( $information['show_register'] != "Y") {
                continue;
            }
            if (self::$attributesBoolean && in_array($attribute, self::$attributesBoolean)) {
                $aColumns[$attribute]=array(
                    'header' => $information['caption'] ?? $information['description'],
                    'name' => $attribute,
                    'type' => 'raw',
                    'value' => 'TokenManaged::formatBooleanAttribute($data, "' . $attribute .'")',
                    'htmlOptions' => array('class' => 'data-column boolean-column column-manager'),
                    'filter'=> array('Y'=>gT('Yes'),'N'=>gT('No')),
                    'filterInputOptions' => array('class'=>'form-control input-sm filter-manager'),
                );
                continue;
            }
            if (in_array($attribute, self::$attributesSelect) || in_array($attribute, self::$attributesSelectRestrictToken)) {
                $code = $information['description'];
                if (intval(App()->getConfig('versionnumber') <= 3)) {
                    $oQuestion = \Question::model()->find(
                        "sid = :sid AND title = :title AND language = :language and parent_qid = 0",
                        array(":sid" => self::$sid, ":title" => $code, ":language" => App()->getLanguage())
                    );
                } else {
                    $oQuestion = \Question::model()->find(
                        "sid = :sid AND title = :title and parent_qid = 0",
                        array(":sid" => self::$sid, ":title" => $code)
                    );
                }
                if ($oQuestion) {
                    if (intval(App()->getConfig('versionnumber') <= 3)) {
                        $answers = \getQuestionInformation\helpers\surveyAnswers::getAnswers($oQuestion);
                    } else {
                        $answers = \getQuestionInformation\helpers\surveyAnswers::getAnswers($oQuestion, App()->getLanguage());
                    }
                }
                $aColumns[$attribute]=array(
                    'header' => $information['caption'] ?? $information['description'],
                    'name' => $attribute,
                    'type' => 'raw',
                    'value' => 'TokenManaged::formatDropdownAttribute($data, "' . $attribute .'")',
                    'htmlOptions' => array('class' => 'data-column boolean-column column-dropdown'),
                    'filter'=> $answers,
                    'filterInputOptions' => array('class'=>'form-control input-sm filter-dropdown'),
                );
                continue;
            }
            if ($attribute == self::$attributeGroupManager) {
                $aColumns[$attribute]=array(
                    'header' => $information['caption'] ?? ($information['description'] ? $information['description'] : self::translate('Manager')),
                    'name' => $attribute,
                    'type' => 'raw',
                    'value' => '$data->isManager',
                    'htmlOptions' => array('class' => 'data-column boolean-column column-manager'),
                    'filter'=> array('Y'=>gT('Yes'),'N'=>gT('No')),
                    'filterInputOptions' => array('class'=>'form-control input-sm filter-manager'),
                );
                continue;
            }
            if ($attribute == self::$attributeRestricted) {
                $aColumns[$attribute]=array(
                    'header' => $information['caption'] ?? $information['description'] ? $information['description'] : self::translate('Restricted'),
                    'name' => $attribute,
                    'type' => 'raw',
                    'value' => '$data->isRestricted',
                    'htmlOptions' => array('class' => 'data-column boolean-column column-manager'),
                    'filter'=> array('Y'=>gT('Yes'),'N'=>gT('No')),
                    'filterInputOptions' => array('class'=>'form-control input-sm filter-manager'),
                );
                continue;
            }
            $aColumns[$attribute]=array(
                'header' => $information['caption'] ?? $information['description'],
                'name' => $attribute,
                'htmlOptions' => array('class' => 'data-column column-'.$attribute),
                'filterInputOptions' => array('class'=>'form-control input-sm filter-'.$attribute),
            );
        }
        $aColumns['active'] = array(
            'header' => self::translate('Active'),
            'name' => 'active',
            'type' => 'raw',
            'value' => '$data->isActive',
            'htmlOptions' => array('class' => 'data-column boolean-column column-active'),
            'filter'=> array('Y'=>gT('Yes'),'N'=>gT('No')),
            'filterInputOptions' => array('class'=>'form-control input-sm filter-active'),
        );
        return $aColumns;
    }

    public static function formatToken($data)
    {
        if(empty($data->token)) {
            return "";
        }
        $value = CHTML::encode($data->token);
        if (\TokenUsersListAndManagePlugin\Utilities::getSetting(self::$sid, 'tokenTokenShow') == 'link'){
            $value = App()->getController()->createAbsoluteUrl(
                'survey/index',
                array(
                    'sid' => self::$sid,
                    'token' => $data->token,
                    'newtest' => 'Y'
                )
            );
        }
        return $value;
    }
    /**
     * Return the buttons columns
     * @see https://www.yiiframework.com/doc/api/1.1/CButtonColumn
     * @return array
     */
    public function getGridButtons()
    {
        $aAttributes = self::$survey->getTokenAttributes();
        $gridButtons = array();
        if($this->availableButtons['edit']) {
            $gridButtons['edit'] = array(
                'label'=>'<span class="sr-only">'.self::translate("Edit account").'</span><span class="fa fa-pencil-square-o text-info" aria-hidden="true"></span>',
                'url' => 'App()->createUrl("plugins/direct", array("plugin"=>"'.$this->pluginName.'", "surveyid"=>'.$this->originalSid.', "usertoken"=>$data->token, "function"=>"edit"));',
                'imageUrl'=>false,
                'options' => array(
                    'class' => "btn btn-link btn-xs btn-edit",
                    'data-toggle' => "tooltip",
                    'data-updateusersgrid' => 1,
                    'title' => self::translate("Edit"),
                ),
                'click' => 'function(event){ window.TokenUsersListAndManage.actionForm(event,$(this)); }',
            );
        }
        if($this->availableButtons['email']) {
            $gridButtons['email'] = array(
                'label'=>'<span class="sr-only">'.self::translate("Send a message to this user").'</span><span class="fa fa-envelope-o text-success" aria-hidden="true"></span>',
                'url' => 'App()->createUrl("plugins/direct", array("plugin"=>"'.$this->pluginName.'", "surveyid"=>'.$this->originalSid.', "usertoken"=>$data->token, "function"=>"email"));',
                'imageUrl'=>false,
                'options' => array(
                    'class' => "btn btn-link btn-xs btn-email",
                    'data-toggle' => "tooltip",
                    'data-updateusersgrid' => 1,
                    'title' => self::translate("Send a message"),
                ),
                'click' => 'function(event){ window.TokenUsersListAndManage.actionForm(event,$(this)); }',
                'visible'=> '$data->emailstatus == "OK" && $data->email',
            );
        }
        if($this->availableButtons['invalidemail']) {
            $gridButtons['invalidemail'] = array(
                'label'=>'<span class="sr-only">'.self::translate("Invalid email, edit user to fix.").'</span><span class="fa fa-envelope-o text-muted" aria-hidden="true"></span>',
                'url' => '#',
                'imageUrl'=>false,
                'options' => array(
                    'class' => "btn btn-link btn-xs btn-emailspacer",
                    'data-toggle' => "tooltip",
                    'title' => self::translate("Invalid email."),
                ),
                'click' => 'function(event){ event.preventDefault(); }',
                'visible'=> '$data->emailstatus != "OK" || empty($data->email)',
            );
        }
        if($this->availableButtons['disable']) {
            $gridButtons['disable'] = array(
                'label' => '<span class="sr-only">'.self::translate("Disable access of this user").'</span><span class="fa fa-user-times text-info" aria-hidden="true"></span>',
                'url' => 'App()->createUrl("plugins/direct", array("plugin"=>"'.$this->pluginName.'", "surveyid"=>'.$this->originalSid.', "usertoken"=>$data->token, "function"=>"disable"));',
                'imageUrl' => false,
                'options' => array(
                    'class' => "btn btn-link btn-xs btn-disable btn-disableaccess",
                    'data-toggle' => "tooltip",
                    'title' => self::translate("Disable access"),
                ),
                'visible' => '$data->getActive() == "Y"',
                'click' => 'function(event){ window.TokenUsersListAndManage.confirmForm(event,$(this)); }',
            );
        }
        if($this->availableButtons['empty']) {
            $gridButtons['empty'] = array(
                'label' => '<span class="sr-only">'.self::translate("Delete information about this user").'</span><span class="fa fa-user-times text-danger" aria-hidden="true"></span>',
                'url' => 'App()->createUrl("plugins/direct", array("plugin"=>"'.$this->pluginName.'", "surveyid"=>'.$this->originalSid.', "usertoken"=>$data->token, "function"=>"empty"));',
                'imageUrl' => false,
                'options' => array(
                    'class' => "btn btn-link btn-xs btn-disable btn-disableaccount",
                    'data-toggle' => "tooltip",
                    'title' => self::translate("Disable access and delete information"),
                ),
                'visible' => '$data->getActive() == ""',
                'click' => 'function(event){ window.TokenUsersListAndManage.confirmForm(event,$(this)); }',
            );
        }
        return $gridButtons;
    }

    /**
     * @return string
     */
    public function getIsManager()
    {
        if(!self::$attributeGroupManager) {
            return "";
        }
        if ($this->getAttribute(self::$attributeGroupManager)) {
            return "<i class='fa fa-check-circle' aria-hidden='true'></i><span class='sr-only'>".gT("Yes")."</span>";
        } else {
            return "";
        }
    }

    /**
     * @return string
     */
    public function getIsRestricted()
    {
        if(!self::$attributeRestricted) {
            return "";
        }
        if ($this->getAttribute(self::$attributeRestricted)) {
            return "<i class='fa fa-check-circle' aria-hidden='true' title='" . gT("Yes") . "'></i><span class='sr-only'>" . gT("Yes") . "</span>";
        } else {
            return "";
        }
    }

    /**
     * Return foramtted boolean
     * @var $data attribnutes
     * @var $attribute to take
     * @return string
     */
    public static function formatBooleanAttribute($data, $name) {
        if(empty($data->$name)) {
            return "";
        }
        if ($data->$name === "0") {
            return "";
        }
        return "<i class='fa fa-check-circle' aria-hidden='true' title='" . gT("Yes") . "'></i><span class='sr-only'>" . gT("Yes") . "</span>";
    }

    /**
     * Return foramtted boolean
     * @var $data attribnutes
     * @var $attribute to take
     * @return string
     */
    public static function formatDropdownAttribute($data, $name) {
        if(empty($data->$name)) {
            return "";
        }
        $TokensAttributeList = \TokenUsersListAndManagePlugin\Utilities::getTokensAttributeList($data->originalSid);
        if (!empty($TokensAttributeList[$name])) {
            if (intval(App()->getConfig('versionnumber') <= 3)) {
                $oQuestion = \Question::model()->find(
                    "sid = :sid AND title = :title AND language = :language and parent_qid = 0",
                    array(":sid" => $data->originalSid, ":title" => $TokensAttributeList[$name], ":language" => App()->getLanguage())
                );
            } else {
                $oQuestion = \Question::model()->find(
                    "sid = :sid AND title = :title and parent_qid = 0",
                    array(":sid" => $data->originalSid, ":title" => $TokensAttributeList[$name])
                );
            }
            if ($oQuestion) {
                if (intval(App()->getConfig('versionnumber') <= 3)) {
                    $answers = \getQuestionInformation\helpers\surveyAnswers::getAnswers($oQuestion);
                } else {
                    $answers = \getQuestionInformation\helpers\surveyAnswers::getAnswers($oQuestion, App()->getLanguage());
                }
                if (!empty($answers[$data->$name])) {
                    return "<code>[" . $data->$name . "]</code> " . $answers[$data->$name];
                }
            }
        }
        return $data->$name;
    }

    /**
     * @return string
     */
    public function getActive()
    {
        $now = dateShift(date("Y-m-d H:i:s"), "Y-m-d H:i:s", App()->getConfig('timeadjust'));
        if ($this->emailstatus != 'disable' && (empty($this->validfrom) || $this->validfrom < $now) &&  (empty($this->validuntil) || $this->validuntil >= $now) ) {
            return 'Y';
        } else {
            return '';
        }
    }

    /**
     * @return string
     */
    public function getIsActive()
    {
        if($this->getActive()) {
            return "<i class='fa fa-check-circle' aria-hidden='true' title='" . gT("Yes") . "'></i><span class='sr-only'>".gT("Yes")."</span>";
        } else {
            return "";
        }
    }

    /**
     * get default sort
     * @return array
     */
    public function getSort() {
        $sort = new \CSort;
        $sort->defaultOrder = Yii::app()->db->quoteColumnName($this->tableAlias.'.tid').' ASC';
        $sort->multiSort = true;
        $sort->attributes = array();
        /* specific sort oart */

        $sort->attributes = array_merge($sort->attributes,array("*"));
        return $sort;
    }

    /**
     * Disable access
     * @return self
     */
    public function setDisable() {
        $dateNow = dateShift(date("Y-m-d H:i:s"), "Y-m-d H:i:s", App()->getConfig('timeadjust'));
        if (empty($this->validuntil) || $this->validuntil >= $dateNow) {
            $this->validuntil = $dateNow;
            $this->save(false);// Force save : no validation
        }
        return $this;
    }

    /**
     * Disable access
     * @param str
     * @return self
     */
    public function setDeleted() {
        $dateNow = dateShift(date("Y-m-d H:i:s"), "Y-m-d H:i:s", App()->getConfig('timeadjust'));
        if (empty($this->validuntil) || $this->validuntil >= $dateNow) {
            $this->validuntil = $dateNow;
        }
        $this->emailstatus = "disable";
        $this->firstname = "";
        $this->lastname = "";
        $this->email = "";
        $aAttributes = $this->getTokenAttributes();
        unset($aAttributes[self::$attributeGroup]);
        foreach ($aAttributes as $attribute => $attributeInformations) {
            if(!self::$keepMandatoryAttributeWhenDelete || $attributeInformations['mandatory'] != "Y") {
                $this->setAttribute($attribute, '');
            }
        }
        $this->save(false);// Force save : no validation
        return $this;
    }

    /**
     * Get the absolute starturl
     * @return string
     */
    public function getAbsoluteStartUrl()
    {
        return App()->getController()->createAbsoluteUrl(
                'survey/index',
                array(
                    'sid' => self::$sid,
                    'token' => $this->token,
                    'newtest' => 'Y'
                )
            );
    }

    /**
     * Get a stranslated string
     * @param string
     * @return string
     */
    private static function translate($string) {
        return \TokenUsersListAndManagePlugin\Utilities::translate($string);
    }

    private function getTokenAttributes()
    {
        $tokensAttributes = self::$survey->getTokenAttributes();
        $LanguageSettings = SurveyLanguageSetting::model()->findByAttributes(
            array('surveyls_survey_id' => self::$sid, 'surveyls_language' => App()->getLanguage())
        );
        if ($LanguageSettings) {
            $attributeCaptions = array_filter((array) $LanguageSettings->attributeCaptions);
            foreach ($attributeCaptions as $attribute => $caption) {
                $tokensAttributes[$attribute]['caption'] = $caption;
            }
        }
        return $tokensAttributes;
    }
}
