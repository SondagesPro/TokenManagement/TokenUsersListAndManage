<?php

/**
 * Some Utilities
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2020-2025 Denis Chenu <http://www.sondages.pro>
 * @license AGPL v3
 * @version 0.36.1
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

namespace TokenUsersListAndManagePlugin;

use App;
use Yii;
use Plugin;
use PluginSetting;
use Survey;
use Token;
use CDbCriteria;
use CHtml;

class Utilities
{
    /* @var string $API version */
    const API = '0.35.0';

    /* @var string[] GlobalSettings : have a global relates setting */
    const GlobalSettings = array(
        'managementByParent',
        'useAttributeRestricted',
        'allowAllAttributeRestricted',
        'listSurveyRestrictedByParent'
    );

    /**
     * Translate a string
     * @param string $string The message that are being translated
     * @param string $language
     * @return string
     */
    public static function translate($string, $language = null)
    {
        /* Don't use gT currently : do quickest way */
        return \Yii::t('', $string, array(), 'TokenUsersListAndManageMessages', $language);
    }

    /**
     * Get a DB setting from a plugin
     * @param integer survey id
     * @param string setting name
     * @return mixed
     */
    public static function getSetting($surveyId, $sSetting)
    {
        if (!Yii::getPathOfAlias('TokenUsersListAndManagePlugin')) {
            return null;
        }
        $settings = Settings::getInstance();
        return $settings->getSetting($surveyId, $sSetting);
    }

    /**
     * Get all survey setting from this plugin and return sureyid
     * @param string $setting setting name
     * @param string $value setting value
     * @return array
     */
    public static function getSurveyIdWithValue($setting, $value)
    {
        if (!Yii::getPathOfAlias('TokenUsersListAndManagePlugin')) {
            return null;
        }
        $settings = Settings::getInstance();
        return $settings->getSurveyIdWithValue($setting, $value);
    }

    /**
     * get the token attribute list
     * @param integer $surveyId
     * @param string $prefix
     * @param boolean $default add default one
     * @return array
     */
    public static function getTokensAttributeList($surveyId, $prefix = "", $default = false)
    {
        $oSurvey = Survey::model()->findByPk($surveyId);

        $aTokens = array();
        if ($default) {
            $aTokens = array(
                $prefix . 'firstname' => gT("First name"),
                $prefix . 'lastname' => gT("Last name"),
                $prefix . 'token' => gT("Token"),
                $prefix . 'email' => gT("Email"),
            );
        }
        foreach ($oSurvey->getTokenAttributes() as $attribute => $information) {
            $aTokens[$prefix . $attribute] = $attribute;
            if (!empty($information['description'])) {
                $aTokens[$prefix . $attribute] = $information['description'];
            }
        }
        return $aTokens;
    }

    /**
     * Return the list of token related : didn't check reloadAnyResponse right
     * Right is done via reloadAnyResponse settings
     * @param integer $surveyId
     * @param string $token
     * @param boolean $checkRight on reloadAnyResponse settings for this survey
     * @return integer[]
     */
    public static function getTokensList($surveyId, $token, $checkRight = false)
    {
        if ($checkRight && !\reloadAnyResponse\Utilities::getReloadAnyResponseSetting($surveyId, 'allowTokenUser')) {
            return array();
        }
        $tokensList = array(
            $token => $token
        );
        if ($checkRight && !\reloadAnyResponse\Utilities::getReloadAnyResponseSetting($surveyId, 'allowTokenGroupUser')) {
            return $tokensList;
        }
        $tokenAttributeGroup = self::getTokenAttributeGroup($surveyId);
        if (empty($tokenAttributeGroup)) {
            return $tokensList;
        }
        $oToken = Token::model($surveyId)->find("token = :token", array(":token" => $token));
        $tokenGroup = (isset($oToken->$tokenAttributeGroup) && trim($oToken->$tokenAttributeGroup) != '') ? $oToken->$tokenAttributeGroup : null;
        if (empty($tokenGroup)) {
            return $tokensList;
        }
        $criteria = new CDbCriteria();
        $criteria->addCondition("token <> '' AND token IS NOT NULL");
        $criteria->compare($tokenAttributeGroup, $tokenGroup);
        $oTokenGroup = Token::model($surveyId)->findAll($criteria);
        return \CHtml::listData($oTokenGroup, 'token', 'token');
    }

    /**
     * Return an email template
     * @param integer sureyId
     * @param string $mailType
     * @param boolean $replace
     * @return string
     */
    public static function getMailTemplate($surveyId, $mailtype, $replace = true)
    {
        $sLanguage = App()->language;
        $aSurveyInfo = getSurveyInfo($surveyId, $sLanguage);
        $mailTemplate = $aSurveyInfo["email_{$mailtype}"];
        preg_match("/<body[^>]*>(.*?)<\/body>/is", $mailTemplate, $aMailTemplate);
        if (isset($aMailTemplate[1])) {
            $mailTemplate = $aMailTemplate[1];
        }
        $mailTemplate = preg_replace("/<img[^>]+\>/i", "", $mailTemplate);
        if ($replace) {
            $aReplacement = self::getDefaultMailReplace($surveyId);
            $mailTemplate = str_replace(array_keys($aReplacement), $aReplacement, $mailTemplate);
        }
        $filter = new \CHtmlPurifier();
        $mailTemplate = $filter->purify($mailTemplate);
        return $mailTemplate;
    }

    /**
     * Return the default mail templace replacement
     * @param integer sureyId
     * @param string $mailType
     * @param boolean $replace
     * @return string
     */
    public static function getDefaultMailReplace($surveyId)
    {
        $sLanguage = App()->language;
        $aSurveyInfo = getSurveyInfo($surveyId, $sLanguage);
        return array(
            '{FIRSTNAME}' => sprintf(self::translate("%s(First name of user)%s"), "<em class='text-primary'>", "</em>"),
            '{LASTNAME}' => sprintf(self::translate("%s(Last name of user)%s"), "<em class='text-primary'>", "</em>"),
            '{SURVEYURL}' => sprintf(self::translate("%s(link automatically generated to the current survey)%s"), "<em class='text-primary'>", "</em>"),
            '{PARENTURL}' => sprintf(self::translate("%s(link automatically generated to the parent survey)%s"), "<em class='text-primary'>", "</em>"),
            '{OPTOUTURL}' => sprintf(self::translate("%s(link to opt-out of receiving new messages)%s"), "<em class='text-primary'>", "</em>"),
            '{SURVEYDESCRIPTION}' => sprintf(self::translate("%s(your specific instructions)%s"), "<em class='text-primary'>", "</em>"),
            '{MESSAGE}' => sprintf(self::translate("%s(your specific instructions)%s"), "<em class='text-primary'>", "</em>"),
            '{ADMINNAME}' => sprintf(self::translate("%s(your contact information)%s"), "<em class='text-primary'>", "</em>"),
            '{ADMINEMAIL}' => sprintf(self::translate("%s(your email)%s"), "<em class='text-primary'>", "</em>"),
            /* Extra part */
            '{SURVEYNAME}' => $aSurveyInfo['surveyls_title'],
        );
    }

    /**
     * Send an email as token user
     * @param integer $surveyId
     * @param \Token|null $oManagerToken
     * @param \Token $oUserToken
     * @param string $sMessage
     * @param string $emailType
     * @param integer|null $srid
     * @return boolean
     */
    public static function sendMail($surveyId, $oUserToken, $oManagerToken = null, $sMessage = "", $emailType = 'register', $srid = null)
    {
        $sLanguage = App()->language;
        if (intval(App()->getConfig('versionnumber')) < 4) {
            return self::sendMailLS3($surveyId, $oUserToken, $oManagerToken, $sMessage, $emailType, $srid);
        }
        $mailer = new \LimeMailer();
        $mailer->emailType = 'tokenuserslistandmanageplugin';
        $mailer->setSurvey($surveyId);
        $mailer->replaceTokenAttributes = true;
        $mailer->setToken($oUserToken->token);
        $aSurveyInfo = getSurveyInfo($surveyId, $sLanguage);

        /* Replace specific word for rwsubject and rawbody */
        $specificReplace = array(
            '{SURVEYDESCRIPTION}' => '{MESSAGE}',
            '{ADMINNAME}' => '{MANAGERNAME}',
            '{ADMINEMAIL}' => '{MANAGEREMAIL}',
            '{SURVEYURL}' => '{CURRENTURL}',
        );
        $mailer->rawSubject = str_replace(
            array_keys($specificReplace),
            $specificReplace,
            $aSurveyInfo['email_' . $emailType . '_subj']
        );
        $mailer->rawBody = str_replace(
            array_keys($specificReplace),
            $specificReplace,
            $aSurveyInfo['email_' . $emailType]
        );
        $mailer->aReplacements["PARENTURL"] = $mailer->aReplacements["CURRENTURL"] = $mailer->aReplacements["BASEURL"] = App()->getController()->createAbsoluteUrl(
            "survey/index",
            array(
                'sid' => $surveyId,
                'token' => $oUserToken->token
            )
        );

        if ($srid && App()->getConfig('reloadAnyResponseApi') && \reloadAnyResponse\Utilities::getReloadAnyResponseSetting($surveyId, 'allowTokenGroupUser')) {
            $mailer->aReplacements["CURRENTURL"] = App()->getController()->createAbsoluteUrl(
                "survey/index",
                array(
                    'sid' => $surveyId,
                    'token' => $oUserToken->token,
                    'srid' => $srid
                )
            );
        }
        if (Yii::getPathOfAlias('RelatedSurveyManagement') && version_compare(\RelatedSurveyManagement\Utilities::API, "0.4", ">=")) {
            if (version_compare(\RelatedSurveyManagement\Utilities::API, "0.8", ">=")) {
                $oParentSurvey = \RelatedSurveyManagement\ParentSurveys::getInstance($surveyId);
            } else {
                $oParentSurvey = new \RelatedSurveyManagement\ParentSurveys($surveyId);
            }
            $parentSurvey = $oParentSurvey->getFinalAncestrySurvey();
            if ($parentSurvey) {
                $mailer->aReplacements["PARENTURL"] = App()->getController()->createAbsoluteUrl(
                    "survey/index",
                    array(
                        'sid' => $parentSurvey,
                        'token' => $oUserToken->token
                    )
                );
            }
        }
        $mailer->aReplacements["MESSAGE"] = $sMessage;
        /* urlPlaceHolder are not replaced by LipmeMailer, @todo report issue about url not updated */
        $ownUrlsPlaceholders = array('CURRENT', 'PARENT', 'BASE');
        foreach ($ownUrlsPlaceholders as $urlPlaceholder) {
            if (!empty($mailer->aReplacements["{$urlPlaceholder}URL"])) {
                $url = $mailer->aReplacements["{$urlPlaceholder}URL"];
                $mailer->rawBody = str_replace("@@{$urlPlaceholder}URL@@", $url, $mailer->rawBody);
                if ($mailer->getIsHtml()) {
                    $mailer->aReplacements["{$urlPlaceholder}URL"] = \CHtml::link($url, $url);
                }
            }
        }
        $mailer->aReplacements["MANAGERNAME"] = $aSurveyInfo['admin'];
        $mailer->aReplacements["MANAGERMAIL"] = $aSurveyInfo['adminemail'];
        if ($oManagerToken) {
            $mailer->aReplacements["MANAGERNAME"] = implode(" ", array($oManagerToken->firstname,$oManagerToken->lastname));
            $mailer->aReplacements["MANAGERMAIL"] = $oManagerToken->email;
        }
        $mailer->setFrom(
            $aSurveyInfo['adminemail'],
            $mailer->aReplacements["MANAGERNAME"]
        );
        $setReplyTo  = self::getSetting($surveyId, 'setReplyTo');
        if ($setReplyTo && $oManagerToken && $aSurveyInfo['adminemail'] != $mailer->aReplacements["MANAGERMAIL"]) {
            $mailer->addReplyTo($mailer->aReplacements["MANAGERMAIL"], $mailer->aReplacements["MANAGERNAME"]);
            if ($setReplyTo == 'both') {
                $mailer->addReplyTo($aSurveyInfo['adminemail'], $aSurveyInfo['admin']);
            }
        }
        $success = $mailer->sendMessage();
        if ($success) {
            $today = dateShift(date("Y-m-d H:i:s"), "Y-m-d H:i", App()->getConfig('timeadjust'));
            if ($oUserToken->sent) {
                $oUserToken->remindersent = $today;
                // $oToken->remindercount++; not really a reminder
            } else {
                $oUserToken->sent = $today;
            }
            $oUserToken->save();
            return true;
        }
        return false;
    }

    /**
     * Send an email as token user
     * @param integer $surveyId
     * @param \Token $oUserToken
     * @param \Token|null $oManagerToken
     * @param string $sMessage
     * @param string $emailType
     * @param integer|null $srid
     * @return boolean
     */
    private static function sendMailLS3($surveyId, $oUserToken, $oManagerToken = null, $sMessage = "", $emailType = 'register', $srid = null)
    {
        $sLanguage = App()->language;
        $aSurveyInfo = getSurveyInfo($surveyId, $sLanguage);
        $aMail = array();
        $aMail['subject'] = $aSurveyInfo['email_' . $emailType . '_subj'];
        $aMail['message'] = $aSurveyInfo['email_' . $emailType];
        $aReplacementFields = array();
        $aReplacementFields["{ADMINNAME}"] = $aSurveyInfo['admin'];
        $aReplacementFields["{ADMINEMAIL}"] = $aSurveyInfo['adminemail'];
        if ($oManagerToken) {
            $aReplacementFields["{ADMINNAME}"] = implode(" ", array($oManagerToken->firstname,$oManagerToken->lastname));
            $aReplacementFields["{ADMINEMAIL}"] = $oManagerToken->email;
        }
        $aReplacementFields["{SURVEYNAME}"] = $aSurveyInfo['name'];
        $aReplacementFields["{EXPIRY}"] = $aSurveyInfo["expiry"];
        foreach ($oUserToken->attributes as $attribute => $value) {
            $aReplacementFields["{" . strtoupper($attribute) . "}"] = $value;
        }
        $aReplacementFields["{SURVEYDESCRIPTION}"] = $sMessage;
        $aReplacementFields["{MESSAGE}"] = $sMessage;

        $useHtmlEmail = (getEmailFormat($surveyId) == 'html');
        $aMail['subject'] = preg_replace("/{TOKEN:([A-Z0-9_]+)}/", "{" . "$1" . "}", $aMail['subject']);
        $aMail['message'] = preg_replace("/{TOKEN:([A-Z0-9_]+)}/", "{" . "$1" . "}", $aMail['message']);
        $aReplacementFields["{PARENTURL}"] = $aReplacementFields["{SURVEYURL}"] = $aReplacementFields["{BASEURL}"] = App()->getController()->createAbsoluteUrl(
            "survey/index",
            array(
                'sid' => $surveyId,
                'token' => $oUserToken->token
            )
        );
        if ($srid && App()->getConfig('reloadAnyResponseApi') && \reloadAnyResponse\Utilities::getReloadAnyResponseSetting($surveyId, 'allowTokenGroupUser')) {
            $aReplacementFields["{SURVEYURL}"] = App()->getController()->createAbsoluteUrl(
                "survey/index",
                array(
                    'sid' => $surveyId,
                    'token' => $oUserToken->token,
                    'srid' => $srid
                )
            );
        }
        if (Yii::getPathOfAlias('RelatedSurveyManagement') && version_compare(\RelatedSurveyManagement\Utilities::API, "0.4", ">=")) {
            if (version_compare(\RelatedSurveyManagement\Utilities::API, "0.8", ">=")) {
                $oParentSurvey = \RelatedSurveyManagement\ParentSurveys::getInstance($surveyId);
            } else {
                $oParentSurvey = new \RelatedSurveyManagement\ParentSurveys($surveyId);
            }
            $parentSurvey = $oParentSurvey->getFinalAncestrySurvey();
            if ($parentSurvey) {
                $aReplacementFields["{PARENTURL}"] = App()->getController()->createAbsoluteUrl(
                    "survey/index",
                    array(
                        'sid' => $parentSurvey,
                        'token' => $oUserToken->token
                    )
                );
            }
        }

        $aReplacementFields["{OPTOUTURL}"] = App()->getController()->createAbsoluteUrl("optout/tokens", array('surveyid' => $surveyId, 'token' => $oUserToken->token));
        $aReplacementFields["{OPTINURL}"] = "";
        foreach (array('OPTOUT', 'OPTIN', 'SURVEY', 'PARENT', 'BASE') as $key) {
            $url = $aReplacementFields["{{$key}URL}"];
            if ($useHtmlEmail) {
                $aReplacementFields["{{$key}URL}"] = "<a href='{$url}'>" . htmlspecialchars($url) . '</a>';
            }
            $aMail['subject'] = str_replace("@@{$key}URL@@", $url, $aMail['subject']);
            $aMail['message'] = str_replace("@@{$key}URL@@", $url, $aMail['message']);
        }
        // Replace the fields
        $aMail['subject'] = ReplaceFields($aMail['subject'], $aReplacementFields);
        $aMail['message'] = ReplaceFields($aMail['message'], $aReplacementFields);
        $sFrom = $aSurveyInfo['adminemail'];
        $textFrom = $aReplacementFields["{ADMINNAME}"];
        if (!empty($aSurveyInfo['admin'])) {
            $textFrom = sprintf(self::translate("%s via %s"), $aReplacementFields["{ADMINNAME}"], $aSurveyInfo['admin']);
        }
        $sFrom = $textFrom . " <" . $sFrom . ">";
        $sBounce = getBounceEmail($surveyId);
        $sTo = $oUserToken->email;
        $sitename = App()->getConfig('sitename');
        global $maildebug, $maildebugbody;
        if (!\Permission::model()->hasSurveyPermission($surveyId, 'surveysettings', 'update')) {
            App()->setConfig("emailsmtpdebug", 0);
        }
        $oldModel = $emailType;
        if ($emailType == 'invite') {
            $oldModel = 'invitation';
        } elseif ($emailType == 'remind') {
            $oldModel = 'reminder';
        }
        $event = new \PluginEvent('beforeTokenEmail');
        $event->set('survey', $surveyId);
        $event->set('type', $emailType);
        $event->set('model', $oldModel);
        $event->set('subject', $aMail['subject']);
        $event->set('to', $sTo);
        $event->set('body', $aMail['message']);
        $event->set('from', $sFrom);
        $event->set('bounce', $sBounce);
        $event->set('token', $oUserToken->getAttributes());
        App()->getPluginManager()->dispatchEvent($event);
        $body = strval($event->get('body'));
        $subject = strval($event->get('subject'));
        $sTo = $event->get('to');
        $sFrom = $event->get('from');
        $sBounce = $event->get('bounce');
        if ($event->get('send', true) == false) {
            $maildebug = (string)$event->get('error', $maildebug);
            $success = empty($event->get('error'));
        } else {
            $success = SendEmailMessage($body, $aMail['subject'], $sTo, $sFrom, $sitename, $useHtmlEmail, $sBounce);
        }
        if ($success) {
            $today = dateShift(date("Y-m-d H:i:s"), "Y-m-d H:i", App()->getConfig('timeadjust'));
            $oUserToken->sent = $today;
            $oUserToken->save();
            return true;
        }
        return false;
    }

    /**
     * Return the list of allowed srid related, null if no restriction
     * @param integer $surveyId
     * @param string $token
     * @return boolean
     */
    public static function getHaveAllAccess($surveyId, $token)
    {
        if (empty($token)) {
            return false;
        }
        if (!self::getSetting($surveyId, 'allowAllAttributeRestricted')) {
            return false;
        }
        return models\TokenSridAccess::haveAllAccess($surveyId, $token);
    }

    /**
     * Return the list of allowed srid related, null if no restriction
     * @param integer $surveyId
     * @param string $token
     * @return null|integer[]
     */
    public static function getAllowedSridList($surveyId, $token)
    {
        if (!self::getSetting($surveyId, 'useAttributeRestricted')) {
            return null;
        }
        $haveRelatedSurveyManagement = Yii::getPathOfAlias('RelatedSurveyManagement') && version_compare(\RelatedSurveyManagement\Utilities::API, "0.4", ">=");
        $finalSurveyId = $surveyId;
        if ($haveRelatedSurveyManagement) {
            if (self::getSetting($surveyId, 'managementByParent')) {
                if (version_compare(\RelatedSurveyManagement\Utilities::API, "0.8", ">=")) {
                    $oParentSurvey = \RelatedSurveyManagement\ParentSurveys::getInstance($surveyId);
                } else {
                    $oParentSurvey = new \RelatedSurveyManagement\ParentSurveys($surveyId);
                }
                $parentSurveyId = $oParentSurvey->getFinalAncestrySurvey();
                if ($parentSurveyId) {
                    $finalSurveyId = $parentSurveyId;
                }
            }
        }
        /* 1st check if available here */
        $tokenAttributeRestricted = self::getSetting($finalSurveyId, 'tokenAttributeRestricted');
        if (empty($tokenAttributeRestricted)) {
            return null;
        }
        $oToken = \Token::model($finalSurveyId)->findByToken($token);
        if (empty($oToken) || empty($oToken->$tokenAttributeRestricted)) {
            return null;
        }
        /* Check if allow All */
        if (self::getSetting($surveyId, 'allowAllAttributeRestricted')) {
            $allowedAll = models\TokenSridAccess::model()->findByPk(array(
                'sid' => $surveyId,
                'token' => $token,
                'srid' => 0,
            ));
            if ($allowedAll) {
                return null;
            }
        }
        /* Token user is restricted : get the srid */
        $oSridList = models\TokenSridAccess::model()->findAll(
            "sid = :sid and token = :token",
            array(
                ':sid' => $surveyId,
                ':token' => $token,
            )
        );
        return \CHtml::listData($oSridList, 'srid', 'srid');
    }

    /**
     * Return the current token attribute group
     * @param integer $surveyId
     * @return null|integer[]
     */
    public static function getTokenAttributeGroup($surveyId)
    {
        $responseListAndManageApi = self::getResponseListAndManageApi();
        if ($responseListAndManageApi && version_compare($responseListAndManageApi, "2.5", "<")) {
            return \responseListAndManage\Utilities::getSetting($surveyId, 'tokenAttributeGroup');
        }
        $TokenAttributeGroup = self::getSetting($surveyId, 'tokenAttributeGroup');
        if (is_null($TokenAttributeGroup) && $responseListAndManageApi) {
            $TokenAttributeGroup = strval(\responseListAndManage\Utilities::getSetting($surveyId, 'tokenAttributeGroup', false));
            self::setSetting($surveyId, 'tokenAttributeGroup', $TokenAttributeGroup);
        }
        return $TokenAttributeGroup;
    }

    /**
     * Return the current token attribute group
     * @param integer $surveyId
     * @return null|integer[]
     */
    public static function getTokenAttributeGroupManager($surveyId)
    {
        $responseListAndManageApi = self::getResponseListAndManageApi();
        if ($responseListAndManageApi && version_compare($responseListAndManageApi, "2.5", "<")) {
            return \responseListAndManage\Utilities::getSetting($surveyId, 'tokenAttributeGroupManager');
        }
        $TokenAttributeGroupManager = self::getSetting($surveyId, 'tokenAttributeGroupManager');
        if (is_null($TokenAttributeGroupManager) && $responseListAndManageApi) {
            $TokenAttributeGroupManager = strval(\responseListAndManage\Utilities::getSetting($surveyId, 'tokenAttributeGroupManager', false));
            self::setSetting($surveyId, 'tokenAttributeGroupManager', $TokenAttributeGroupManager);
        }
        return $TokenAttributeGroupManager;
    }

    /**
     * get the response List and manage API version
     * @return null|string
     */
    public static function getResponseListAndManageApi()
    {
        if (App()->getConfig('responseListAndManageAPI')) {
            return App()->getConfig('responseListAndManageAPI');
        }
        if (Yii::getPathOfAlias('responseListAndManage')) {
            if (!defined('\responseListAndManage\Utilities::API')) {
                return 1;
            }
            return \responseListAndManage\Utilities::API;
        }
        return null;
    }
    /**
     * Set an attribute value
     * @access private
     * @param integer $surveyId
     * @param string $setting
     * @param string $value
     * @return void
     */
    private static function setSetting($surveyId, $setting, $value = '')
    {
        $oPlugin = \Plugin::model()->find(
            "name = :name",
            array(":name" => 'TokenUsersListAndManage')
        );
        /* How can i come here ? */
        if (!$oPlugin) {
            return null;
        }
        $oSetting = \PluginSetting::model()->find(
            'plugin_id = :pluginid AND ' . App()->getDb()->quoteColumnName('key') . ' = :key AND model = :model AND model_id = :surveyid',
            array(
                ':pluginid' => $oPlugin->id,
                ':key' => $setting,
                ':model' => 'Survey',
                ':surveyid' => $surveyId,
            )
        );
        if (empty($oSetting)) {
            $oSetting = new \PluginSetting();
        }
        $oSetting->setAttribute('plugin_id', $oPlugin->id);
        $oSetting->setAttribute('key', $setting);
        $oSetting->setAttribute('model', 'Survey');
        $oSetting->setAttribute('model_id', $surveyId);
        $oSetting->setAttribute('value', json_encode($value));
        $oSetting->save();
    }

    /**
     * Check if survey has a token table, and return integer for survey id to check
     * $param $surveyId
     * @return false|int
     */
    public static function getSurveyHasTokenTable($surveyId)
    {
        $sTableName = Yii::app()->db->tablePrefix . 'tokens_' . $surveyId;
        if (in_array($sTableName, App()->db->schema->getTableNames())) {
            return $surveyId;
        }
        return false;
    }

    /**
     * Check if a token is a manager
     * @param integer $surveyId
     * @param string $token
     * @return boolean
     */
    public static function getIsManager($surveyId, $token)
    {
        $sTableName = Yii::app()->db->tablePrefix . 'tokens_' . $surveyId;
        if (!in_array($sTableName, App()->db->schema->getTableNames())) {
            return false;
        }
        \Yii::import('TokenUsersListAndManagePlugin.models.TokenManaged');
        $TokenManaged = \TokenManaged::model($tokenTableId)->findByToken($this->token);
        return $TokenManaged && $TokenManaged->getIsManager();
    }
}
